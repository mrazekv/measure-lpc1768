/*
 * usb.h
 *
 *  Created on: 8.3.2012
 *      Author: Vojta
 */

#ifndef USB_H_
#define USB_H_

void USB_start();
int VCOM_putchar(int c);
int VCOM_getchar(void);


void VCOM_xput(unsigned char c);
unsigned char  VCOM_xget(void);



#endif /* USB_H_ */
