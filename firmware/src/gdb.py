import sys, os, tempfile, subprocess

f = tempfile.NamedTemporaryFile(delete=False)
SCRIPT_NAME = f.name

GBD = ["arm-none-eabi-gdb", "-x", SCRIPT_NAME]
OCD = "openocd-0.5.exe"
SCRIPT = """
  target remote localhost:3333
  monitor reset
  monitor halt
  delete breakpoints
  file FreeRTOS_USB_Eth_Main.elf
  #break main
  """

f.write(SCRIPT)
f.close()

subprocess.Popen(OCD)

subprocess.call(GBD)

os.unlink(SCRIPT_NAME)




# gdb_lpc17xx_debug.gdb
# # Connect to OpenOCD gdb server
# target remote localhost:3333
#
# # reset and halt the IC with debugger-specific OpenOCD commands
# monitor reset
# monitor halt
#
# # load the firmware's symbol table into gdb
# file Blinky.elf
#
# #configure debugging and then display settings
# set debug arm
# show arm disassembler
# show arm abi
# show debug arm
#
# # set a breakpoint on the first function in main()
# echo -----------Set Breakpoint on SysTick_Handler---\n
# br SysTick_Handler
#
# # after the target starts and breakpoints, resume
# echo -----------Monitor Poll------------------------\n
# monitor poll
# echo -----------Resume, Sleep-----------------------\n
# monitor resume
# monitor sleep 200
# echo -----------Monitor Poll------------------------\n
# monitor poll
# echo -----------print SystemCoreClock---------------\n
# print SystemCoreClock
# echo -----------print msTicks-----------------------\n
# print msTicks
# echo -----------info registers----------------------\n
# info registers
# echo -----------------------------------------------\n
