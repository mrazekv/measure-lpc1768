/*
 * cmdconfig.c
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 */

#include "cmd.h"
#include "monitor.h"
#include "config.h"
#include "string.h"
#include "iptools.h"


static const char weekdates[8][4]={
		"SUN", "MON","TUE","WED","THU", "FRI", "SAT", "???"
};

CMDACTION(configcmd)
{
	if(argc==2 && strcmp(argv[1], "show")==0) /* config show */
	{
		TConfig * conf=config_all();;

		char buff[IP_STRING];
		uip_ipaddr_t ip;
		ip_to_string(buff,conf->ipaddr);
		xprintf("IP: %s\n", buff);
		ip_to_string(buff,conf->netmask);
		xprintf("mask: %s\n", buff);
		ip_to_string(buff,conf->gw);
		xprintf("gw: %s\n", buff);

		xprintf("Time %s %04d-%02d-%02d %d:%02d",
				weekdates[conf->dow & 0x07],  // 0..7
				(int)conf->year,(int) conf->month,(int) conf->day,
				(int)conf->hour,(int) conf->minute
		);
	}
	else /* Help */
	{
		xprintf("Configuration commands help:\n"
				" config show - shows the config file");
	}
	return CMDLINE_OK;
}
