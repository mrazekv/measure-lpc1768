/*
 * mymath.h
 *
 *  Created on: 1.3.2012
 *      Author: Vojta
 */

#ifndef MYMATH_H_
#define MYMATH_H_


#define PRINTFLOAT printfloat
// \
   //sprintf(buff,"%d.%02d",(int)a, ((int)(a*100))%100);

#define myabs(a) ((a>0)?(a):(-a))

/***
 * @brief print float with 2 decimal places
 */
void printfloat(float a, char * buff);

float mysqrt(float x);

#endif /* MYMATH_H_ */
