/*
 * cmdlog.c
 *
 *  Created on: 6.3.2012
 *      Author: Vojta
 */



#include "cmd.h"
#include "monitor.h"
#include "string.h"
#include "stdio.h"
#include "mymath.h"
#include "logger.h"

static inline void printMonth(const char * header, TLogMonth * m)
{
	xprintf("%s %d/%d:\r\n", header, m->month, m->year);
	xprintf(" Active: %d.%02d Wmin\r\n", (int)m->active, ((int)(m->active*100)%100));
	xprintf(" Apparent: %d.%02d VAmin\r\n", (int)m->apparent, ((int)(m->apparent*100)%100));


}

CMDACTION(logcmd)
{
	if(argc==2 && strcmp(argv[1], "profile")==0) /* log profile */
	{
		log_print();
	}
	if(argc==2 && strcmp(argv[1], "con")==0) /* log con */
	{
		TLogMonth * current;
		TLogMonth * last;
		TLogMonth * sum;
		log_get_con(&last, &current, &sum);
		printMonth("Previous month", last);
		printMonth("This month", current);
		printMonth("Summary", sum);

	}
	else if(argc==2 && strcmp(argv[1], "reload")==0) /* ade reload */
	{
		xprintf("Wait for rebuild cache ... \r\n");
		log_histogram(0);
		log_save_hist();
		xprintf("     ... done \r\n");
	}
	else if(argc==2 && strcmp(argv[1], "clean")==0) /* ade clean */
	{
		log_clean();
		xprintf("Wait for rebuild cache ... \r\n");
		log_histogram(0);
		xprintf("     ... done \r\n");
	}
	else
	{
		xprintf("Logger commands help:\n"
				" log profile - shows profile\n"
				" log reload  - reloads histogram cache\n"
				" log con     - consumption\n"
				" log clean   - clean current profile");
	}


	return CMDLINE_OK;

}
