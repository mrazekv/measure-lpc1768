/*
 * cron.c
 *
 *  Created on: 27.2.2012
 *      Author: Vojta
 */

#include "FreeRTOS.h"
#include "task.h"
#include "rtc_cal.h"

#include "monitor.h"

#include "cron.h"
#include "cmdline.h"
#include "logger.h"

// Tasks
#define EVENT_COUNT 2
static TEvent events[]={
   {PER_MINUTE, &log_write, 0},
   {PER_FIVEMINUTE, &time_store, 0},
   {PER_TENMINUTE, &log_save_hist, 0},
   {0,0,0}
};


// Tells that this time is possible to do event
static int IsInTime(RTC_TIME_Type * time, TEvent * ev)
{
	int sec=0, min=0;
	int ret=0;
	switch(ev->per)
	{
	case PER_TWOSECOND:
		sec=2;
		break;
	case PER_TENSECOND:
		sec=10;
		break;
	case PER_THIRTYSECOND:
		sec=30;
		break;
	case PER_MINUTE:
		ret=time->SEC==59;
		break;
	case PER_TWOMINUTE:
		min=2;
		ret=time->SEC==41;
		break;
	case PER_FIVEMINUTE:
		min=5;
		ret=time->SEC==32;
		break;
	case PER_TENMINUTE:
		min=8;
		ret=time->SEC==41;
		break;
	}

	if(sec)
	{
		ret= !(time->SEC % sec);
	}

	if(min)
	{
		ret= ret && !(time->MIN % min);
	}

	return ret;
}

void vCronTask( void *pvParameters )
{

	for(;;)
	{
		vTaskDelay(1000 / portTICK_RATE_MS);
		//xprintf("Cron test");

		RTC_TIME_Type time;

		rtc_cal_gettime(&time);
		ledToggle(LED2);

		for(TEvent * ev=events; ev->func!=0; ev++ )
		{
			/* Test to do this action */
			if(IsInTime(&time, ev))
				ev->func();

		}
	}

}
