/*
 * cmdade.c
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 */

#include "cmd.h"
#include "monitor.h"
#include "ade7753.h"
#include "string.h"
#include "stdio.h"
#include "mymath.h"
#include "logger.h"


CMDACTION(adecmd)
{
	if(argc==2 && strcmp(argv[1], "run")==0) /* ade run */
	{
		xprintf("Run");
		uint8_t data[3];
		for(uint8_t add=0x01; add<=0x08; add++)
		{
			ade_read(add, data, 3);
			xprintf("READ 0x%02X 0x%02x%02x%02x\n", add, data[0], data[1], data[2]);
		}
	}
	else if(argc==4 && strcmp(argv[1], "read")==0) /* ade read <size> <addr_hex> */
	{
		uint8_t data[3];
		uint8_t size;

		uint8_t addr=(uint8_t)(ParseHex(argv[3]) & 0xFF);
		size=(uint8_t)(ParseHex(argv[2]) & 0xFF);

		if(size>=4) size=3; /* max size */
		xprintf("Read from ADE[%02x] %d bytes ... 0x", addr, size);

		ade_read(addr, data, size);
		for(int i=0; i<size; i++)
		{
			xprintf("%02x", data[i]);
		}
	}
	else if(argc==5 && strcmp(argv[1], "write")==0) /* ade write <size> <addr> <data_hex> */
	{
		uint8_t size;

		uint32_t data=ParseHex(argv[4]);
		uint8_t addr=(uint8_t)(ParseHex(argv[3]) & 0xFF);
		size=(uint8_t)(ParseHex(argv[2]) & 0xFF);

		if(size>=4) size=3; /* max size */

		/* moving left */
		//data=data << ((4-size)*8);
		uint8_t data_ar[3];
		int pos=0;
		for(int i=size-1; i>=0; i--, pos++)
		{
			data_ar[pos]=((uint8_t *)&data)[i];
		}
		ade_write(addr,data_ar, size);
	}
	else if(argc==3 && strcmp(argv[1], "spi")==0) /* ade spi <data8>*/
	{
		uint8_t cmd=(uint8_t)(ParseHex(argv[2]) & 0xFF);

		xprintf(" SPI: 0x%02x ... ", cmd);

		xprintf("0x%02x", ade_spi(cmd));
	}
	else if(argc==2 && strcmp(argv[1], "show")==0) /* ade show */
	{
		float active, apparent, reactive, factor, power, current;
		ade_derivation(&active, &apparent, &reactive, &factor, &power, &current);

		char buff[16];
		PRINTFLOAT(active,buff);
		xprintf("Active:   %s W\n", buff);
		PRINTFLOAT(apparent,buff);
		xprintf("Apparent: %s VA\n", buff);
		PRINTFLOAT(reactive,buff);
		xprintf("Reactive: %s VAR\n", buff);
		PRINTFLOAT(factor,buff);
		xprintf("Factor cos(phi): %s", buff);

	}
	else
	{
		xprintf("ADE commands help:\n"
				" ade run                            reads data from 0x00 - 0x08\n"
				" ade read <size> <addr_hex>         reads data from address\n"
				" ade write <size> <addr> <data_hex> writes data to ade\n"
				" ade spi <data8>                    sends BYTE directly\n"
				" ade show                           show current values");
	}


	return CMDLINE_OK;

}


