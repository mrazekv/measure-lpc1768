#include "FreeRTOS.h"
#include "portable.h" // free, malloc etc.
#include "semphr.h"
#include "task.h"
#include <stdio.h>

#include "config.h"
#include "monitor.h"
#include "diskio.h"
#include "ff.h"
#include "leds.h"
#include "ctype.h"
#include "string.h"

FATFS fatfs;
static FIL fileh;
static xSemaphoreHandle xSemCfg = NULL;
static TConfig config;


// 0= none, 1=operations only, 2=all
#define DEBUG_LEVEL 1

#if DEBUG_LEVEL==0
	#define DBG1(...)
	#define DBG2(...)
#elif DEBUG_LEVEL==1
	#define DBG1 xprintf
	#define DBG2(...)
#elif DEBUG_LEVEL==2
	#define DBG1 xprintf
	#define DBG2 xprintf
#endif



/**
 * @brief Initialization of FatFS
 * @return 0 on success
 * */   
int config_init()
{
	WORD res;
	portENTER_CRITICAL();
	{
		xSemCfg=xSemaphoreCreateMutex();
		xSemaphoreTake(xSemCfg, portMAX_DELAY);
	}
	portEXIT_CRITICAL();

	{ /* xSemCfg - 3 lines before */
		if((res=(WORD)disk_initialize(DRIVER)))
			goto init_fail;

		if((res=(WORD)f_mount(DRIVER, &fatfs)))
			goto init_fail;

		if((res=f_open(&fileh, "0:config.bin",FA_OPEN_EXISTING | FA_READ))==0)
		{
		  UINT read;
		  f_read(&fileh, &config, sizeof(config), &read);
		  f_close(&fileh);
		}
		else
		{
		  DBG1("open failed;");
		}

	}
	xSemaphoreGive(xSemCfg);
	config_save();
	return 0;

init_fail:
	xSemaphoreGive(xSemCfg);
	return res;
}

void config_save()
{
	xSemaphoreTake(xSemCfg, portMAX_DELAY);
	{
		FRESULT res;
		if((res=f_open(&fileh, CONFIG_FILE,FA_CREATE_ALWAYS | FA_WRITE))==0)
		{
		  UINT written;
		  f_write(&fileh, &config, sizeof(config), &written);
		  f_close(&fileh);
		}
		else
		{
		  DBG1("open failed;");
		}
	}
	xSemaphoreGive(xSemCfg);
}

TConfig * config_all()
{
	return &config;
}
