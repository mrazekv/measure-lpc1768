/* Blikani s ledkami
Autor: Vojtech Mrazek
Rok:   2012
*/
#include <LPC17xx.h>
#include "leds.h"

#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

/**
 * @brief Nastaven HW na ledky na P2.11 - 13
 * */ 
void ledInit()
{
  // Nastaveni P2.11 P2.12 P2.13 na vystup
  LPC_PINCON->PINSEL4 &= ~(0x3f << 22); // Nastaveno na GPIO
  LPC_GPIO2->FIODIR |= (7 << 11);
  
	GPIO_SetDir(0, (1 << 11), 1);
  
  LPC_GPIO2->FIOPIN |= (7 << 11); // Zhasnuti vsech LED
}

/**
 * @brief Rozsviceni ledky
 * @param led ID ledky
 * */  
void ledOn(uint32_t led)
{
  LPC_GPIO2->FIOPIN &= ~(led);
}


/**
 * @brief Rozsviceni ledky
 * @param led ID ledky
 * */  
void ledOff(uint32_t led)
{
  LPC_GPIO2->FIOPIN |= led;
}

/**
 * @brief Inverze ledky, pokud je nejaka rozla a zbytek zhasly, tak se napred roznou
 * @param led ID ledky
 * */  
void ledToggle(uint32_t led)
{
  if(LPC_GPIO2->FIOPIN & led) // nektera je v 1 => nejaka je zhasla
    ledOn(led);
  else
    ledOff(led);
}