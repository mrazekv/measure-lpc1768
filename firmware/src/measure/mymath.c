/*
 * mymath.c
 *
 *  Created on: 1.3.2012
 *      Author: Vojta
 */
#include "stdlib.h"
#include "stdint.h"
#include "stdio.h"
#include "mymath.h"

#define MATHDEBUG 0

#if MATHDEBUG
#include "monitor.h"
#endif

#define MY_EULER 2.71828182845904523536028747135266249775724709369995



void printfloat(float a, char * buff)
{
	char * pos;
	pos=buff;
	if(a<0)
	{
		pos[0]='-';
		pos++;
	}

	sprintf(pos,"%d.%02d",(int)a, ((int)(a*100))%100);
}

#if MATHDEBUG
static char buff[20];
#endif

float myexp(float x)
{
	if(x>10000000)
		return 0.0;

    float ret=1;
    float aprox=1;
    uint32_t fact=1;
    uint32_t iter=1;
    float diff;

    /* aproximace */
    int full=(int)x;

    x-=full;
    float xval=x;
    if(full>0)
    {
        int i;
        for(i=0; i<full; i++)
        {
            aprox*=MY_EULER;
            if(aprox>100000)
            	return aprox;
        }
    }
    if(full < 0)
    {
        int i;
        for(i=0; i<-full; i++)
        {
            aprox/=MY_EULER;
        }
    }

    do
    {
        diff=xval/fact;
        ret+=diff;
        xval*=x;
        iter++;
        fact*=iter;
    } while(myabs(diff) > 0.000001 && iter<60);

    return ret * aprox;
}


float mylog(float v)
{
    if(v<0) return 0.0/0.0;
    float ret=0;
    while(v>2)
    {
        v/=MY_EULER;
        ret++;
    }

    while(v<1/MY_EULER)
    {
        v*=MY_EULER;
        ret--;
    }


#if MATHDEBUG
	printfloat(ret, buff);
	xprintf("Mylog aprox ret: %s\r\n", buff);


	printfloat(v, buff);
	xprintf("Mylog aprox v: %s\r\n", buff);
#endif


    float x=v-1;
    float xval=x;
    float diff;
    int iter=1;
    int sign=1;

    do
    {
        diff=sign * xval/iter;
        ret+=diff;

        sign *= -1;
        iter++;
        xval*=x;


#if MATHDEBUG
    	printfloat(ret, buff);
    	xprintf("Mylog aprox RET ITER: %s\r\n", buff);
#endif

        //comm_puts("log iter\r\n");

    } while(myabs(diff) > 0.000000001 && iter<60);

    return ret;

}


float mysqrt(float x)
{
	if(x<=0.01f) return 0.0f;
	float logf=mylog(x);
	float expf=myexp(logf / 2);


#if MATHDEBUG
	printfloat(x, buff);
	xprintf("Odmocnina: %s\r\n",buff);
	printfloat(logf, buff);
	xprintf("Log: %s\r\n",buff);
	printfloat(expf, buff);
	xprintf("Vysledek: %s\r\n--------------\r\n",buff);
#endif

	return expf;
}
