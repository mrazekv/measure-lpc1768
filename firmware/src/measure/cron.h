#ifndef _H_CRON
#define _H_CRON



/* Definition of tasks is included in source file */

void vCronTask( void *pvParameters );


typedef enum {
	PER_TWOSECOND, PER_TENSECOND, PER_THIRTYSECOND, PER_MINUTE, PER_TWOMINUTE, PER_FIVEMINUTE, PER_TENMINUTE
} EPeriodicity;

typedef struct {
	EPeriodicity per;  // Periodicity
	void (*func)(); // Current function
	char done; // temporary
} TEvent;


#endif
