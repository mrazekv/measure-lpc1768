/*
 * cmd.h
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 */

#ifndef CMD_H_
#define CMD_H_

#include "cmdline.h"

CMDDECLARE(adecmd);
CMDDECLARE(configcmd);
CMDDECLARE(ipcmd);
CMDDECLARE(timecmd);
CMDDECLARE(logcmd);

#endif /* CMD_H_ */
