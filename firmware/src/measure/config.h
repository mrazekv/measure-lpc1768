#ifndef _H_CONFIG
#define _H_CONFIG
#include <stdlib.h>
#include <stdint.h>
#include "uip.h"

/***
 * @brief Defines driver for FatFS subsystem (0=MMC or SD card on SPI interface)
 * */ 
#define DRIVER 0

/**
 * @brief Path to configuration file
 */
#define CONFIG_FILE "0:config.bin"

/**
 * @brief configuration structure, on change is needed to rebuild all measure files
 */
typedef struct {
	uip_ipaddr_t ipaddr;
	uip_ipaddr_t netmask;
	uip_ipaddr_t gw;
	uint8_t dow;
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
} TConfig;

/**
 * @brief Initialization of FatFS
 * @return 0 on success
 * */   
int config_init();


/**
 * @brief Saves current configuration to CONFIG_FILE
 */
void config_save();

/**
 * @brief Gets pointer to current configuration
 */
TConfig * config_all();

#endif
