/* Accessing to the ADE 7753 device via SSP0 */
#ifndef _H_ADE7753
#define _H_ADE7753

#define ADE_MAX 1200


/**
 * @brief Initializes ADE, configuration must be initialized as first
 * sends 1st word with 7 ticks, 3 words with 8 tics
 */
void ade_init();

/**
 * @brief Writes data to ADE via SPI, ade must be initialized before,
 * locked by semaphore
 */
void ade_write(uint8_t addr, uint8_t * data, uint32_t size);

/**
 * @brief Reads data from ADE via SPI, ADE must be initialized before,
 * locekd by semaphore
 */
void ade_read(uint8_t addr, uint8_t * data, uint32_t size);

/**
 * @brief Sends directly SPI command to ADE, locked by semaphore
 */
uint8_t ade_spi(uint8_t data);

/**
 * @brief Refreshs data from ADE
 */
void vAdeTask(void *pvParameters);


/**
 * @brief gets current derivation values
 * @param active P
 * @param apparent S
 * @param reactive R
 * @param factor cos(phi)
 * @param power Vrms
 * @param current Irms
 */
void ade_derivation(float * active, float * apparent, float * reactive, float * factor, float * power, float * current);


typedef struct {
	int32_t active;
	int32_t apparent;
	//uint32_t power;
	//uint32_t current;
} TEnergy;


/**
 * @brief Gets energy for longer time
 * @return number of second of logging
 */
unsigned int ade_longtime(TEnergy * energy);

#endif
