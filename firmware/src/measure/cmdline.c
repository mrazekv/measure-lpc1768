/*
 * cmdline.c
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 */
#include <ctype.h>
#include <string.h>

#include "cmdline.h"
#include "monitor.h"
#include "cmd.h"

CMDDECLARE(help);

static inline uint8_t char2hex(char c)
{
	if(c>='0' && c<='9') return c-'0';
	if(c>='a' && c<='f') return c-'a' + 10;
	if(c>='A' && c<='F') return c-'A' + 10;
	return 0;
}

uint32_t ParseHex(const char * data)
{
	uint32_t ret=0;
	while(*data)
	{
		ret=(ret<<4) | char2hex(*data);
		data++;
	}
	return ret;
}


static TCMDdata commandDef[] = {
		{"help", &help},
		{"ade", &adecmd},
		{"config", &configcmd},
		{"ip", &ipcmd},
		{"time", &timecmd},
		{"log", &logcmd},
		{"",0}
};


CMDACTION(help)
{
	xprintf("Avaliable commands\n"
			" - help   - shows this help\n"
			" - ade    - settings up ADE7753 IC\n"
			" - config - configuration changing\n"
			" - ip     - network setting\n"
			" - time   - RTC settings\n"
			" - log    - Logs and histogram\n"
			"\n"
			"If you append 'help' after command, you may see help to this command");
	return CMDLINE_OK;
}


CMDACTION(pok)
{
	for(int i=0; i<argc; i++)
	{
		xprintf("Argument %d: %s\r\n", i, argv[i]);
	}

	xprintf("Toto je pokusna fce\r\n");
	return CMDLINE_OK;
}


int cmdline_parse(char * data, uint32_t length)
{
	/* Creates ARGV a ARGC */
	uint32_t argc=0;
	char * argv[16];
	int is_started=0;

	/* goes trough all characters in the buffer */
	for(uint32_t i=0; i<length-1; i++ )
	{
		if(isspace(data[i]) || data[i]==0)
		{
			int was_last=data[i]==0;

			if(is_started) /* any argument in current vector */
			{
				is_started=0;
				data[i]=0;
				argc++;
			}

			if(was_last) /* Last */
				break;
		}
		else if(!is_started)
		{
			argv[argc]=data+i;
			is_started=1;
		}
	}

	if(argc<1)
		return CMDLINE_FAIL;

	for(TCMDdata * cmd = commandDef; cmd->operation!=0; cmd++)
	{
		if(strcmp(argv[0], cmd->command)==0)
		{
			return cmd->operation(argc, argv);
		}
	}
	return CMDLINE_FAIL;
}
