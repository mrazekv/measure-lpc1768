/*
 * iptools.c
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 */
#include "uip.h"
#include "iptools.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include "config.h"


/***
 * @brief Convert IP address to string
 * @param data buffer with min length IP_STRING
 * @param addr IPv4 address
 */
void ip_to_string(char * data, uip_ipaddr_t addr)
{
	sprintf(data, "%d.%d.%d.%d",
			addr[0] & 0xFF,
			addr[0] >> 8,
			addr[1] & 0xFF,
			addr[1] >> 8
			);
}

/***
 * @brief Converts data to ipaddr
 * @brief errors are ignored
 */
void ip_from_string(char * data, uip_ipaddr_t addr)
{
	int len=strlen(data);
	int pos=0;
	char * print=data;

	for(int i=0; i<=len; i++)
	{
		if(isdigit(data[i]))
			continue;

		/* place 0 */
		data[i]=0;

		uint8_t dataint=(uint8_t)(atoi(print));

		switch(pos)
		{
		case 0:
			addr[0]=dataint;
			break;
		case 1:
			addr[0] |= dataint<<8;
			break;
		case 2:
			addr[1]=dataint;
			break;
		case 3:
			addr[1] |= dataint<<8;
			break;
		default:
			break;
		}
		pos++;

		print=data+i+1;

	} /* foreach character in data */
}



/***
 * @brief Initialization of IP stack from config file,
 * config must be initialized
 */
void ip_init()
{
	char buff[IP_STRING+5]; // buffer with reserve
	//uip_ipaddr_t ip;
	TConfig * conf=config_all();
	uip_sethostaddr(conf->ipaddr);
	uip_setnetmask(conf->netmask);
	uip_setdraddr(conf->gw);

/*
	if(config_get("ipaddr", buff, sizeof(buff),"")==0)
	{
		ip_from_string(buff, ip);
		uip_sethostaddr(ip);
	}

	if(config_get("mask", buff, sizeof(buff), "")==0)
	{
		ip_from_string(buff, ip);
		uip_setnetmask(ip);
	}

	if(config_get("gw", buff, sizeof(buff), "") ==0)
	{
		ip_from_string(buff, ip);
		uip_setdraddr(ip);
	}*/

}

