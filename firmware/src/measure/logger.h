/*
 * logger.h
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 */

#ifndef LOGGER_H_
#define LOGGER_H_
/***
 * @brief Defines driver for FatFS subsystem (0=MMC or SD card on SPI interface)
 * */
#define LOG_DRIVER 0

#define LOG_FILE "0:measure.csv"
// Maximum size 80MB
#define LOG_MAX_SIZE 80*1024*1024

typedef struct {
	float active;
	float apparent;
	uint32_t timesum;
} TLogHistogram;

typedef struct {
	float active;
	float apparent;
	uint16_t year;
	uint8_t month;
} TLogMonth;

void log_write();

void log_get_hist(TLogHistogram ** DOW, TLogHistogram ** HOUR);
void log_get_con(TLogMonth ** last, TLogMonth ** current, TLogMonth ** sum);

void log_clean();

void log_enter();

void log_leave();

void log_histogram(int cache);

void log_save_hist();

void log_print();

#endif /* LOGGER_H_ */
