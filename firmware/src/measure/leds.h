#ifndef _H_LEDS
#define _H_LEDS

#define LED1 (1<<11)
#define LED2 (1<<12)
#define LED3 (1<<13)

/**
 * @brief Nastaven HW na ledky na P2.11 - 13
 * */ 
void ledInit();

/**
 * @brief Rozsviceni ledky
 * @param led ID ledky
 * */  
void ledOn(uint32_t led);

/**
 * @brief Rozsviceni ledky
 * @param led ID ledky
 * */  
void ledOff(uint32_t led);

/**
 * @brief Inverze ledky, pokud je nejaka rozla a zbytek zhasly, tak se napred roznou
 * @param led ID ledky
 * */  
void ledToggle(uint32_t led);

#endif