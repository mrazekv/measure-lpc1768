/*
 * cmdip.c
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 */
#include "cmd.h"
#include "uip.h"
#include "monitor.h"
#include "iptools.h"
#include "config.h"
#include "string.h"


static void ipcmd_print_info()
{
	char buff[IP_STRING];
	uip_ipaddr_t ip;

	uip_gethostaddr(ip);
	ip_to_string(buff,ip);
	xprintf("IP: %s\n", buff);

	uip_getnetmask(ip);
	ip_to_string(buff,ip);
	xprintf("Mask: %s\n", buff);


	uip_getdraddr(ip);
	ip_to_string(buff,ip);
	xprintf("Gateway: %s\n", buff);
}

CMDACTION(ipcmd)
{
	if(argc==5 && strcmp(argv[1], "set")==0) /* ip set <addr> <mask> <gw> */
	{
		/* Converts through ipstring */
		char buff[IP_STRING];
		TConfig * conf=config_all();
		uip_ipaddr_t ip;

		/* addr */
		ip_from_string(argv[2], conf->ipaddr);
		/*ip_to_string(buff, conf->ipaddr);
		config_set("ipaddr", buff);*/

		/* mask */
		ip_from_string(argv[3], conf->netmask);
		/*ip_to_string(buff, conf->netmask);
		config_set("mask", buff);*/

		/* gw */
		ip_from_string(argv[4], conf->gw);
		/*ip_to_string(buff, conf->gw);
		config_set("gw", buff);*/

		config_save();

		ip_init();
		ipcmd_print_info();
	}
	else if(argc==2 && strcmp(argv[1], "show")==0)
	{
		ipcmd_print_info();
	}
	else
	{
		xprintf("Network commands help:\n"
				" ip set <addr> <mask> <gw> - sets network layer\n"
				" ip show                   - shows current settings");
	}
	return CMDLINE_OK;
}

