/*
 * cmdtime.c
 *
 *  Created on: 29.2.2012
 *      Author: Vojta
 */
#include "cmd.h"
#include "monitor.h"
#include "config.h"
#include "string.h"
#include "rtc_cal.h"
#include <stdio.h>
#include <stdlib.h>

static const char weekdates[8][4]={
		"SUN", "MON","TUE","WED","THU", "FRI", "SAT", "???"
};

#define TIME_SAVE(a,b) 	sprintf(buff, "%d", (int)(a)); \
						config_set(b, buff);


#define TIME_GET(a,b)   config_get(b, buff, sizeof(buff), ""); \
						a=atoi(buff);


static void time_print()
{
	RTC_TIME_Type time;
	rtc_cal_gettime(&time);

	xprintf("%s %04d-%02d-%02d %d:%02d:%02d",
			weekdates[time.DOW & 0x07],  // 0..7
			(int)time.YEAR, (int)time.MONTH, (int)time.DOM,
			(int)time.HOUR, (int)time.MIN, (int)time.SEC
	);
}

/**
 * @brief saves current time to config file
 */
void time_store()
{
	RTC_TIME_Type time;
	rtc_cal_gettime(&time);

	char buff[16];
	TConfig * conf=config_all();

	conf->dow=time.DOW;
	conf->day=time.DOM;
	conf->year=time.YEAR;
	conf->month=time.MONTH;

	conf->hour=time.HOUR;
	conf->minute=time.MIN;

	config_save();
}

/**
 * @brief saves current time to config file
 */
void time_restore()
{
	RTC_TIME_Type time;

	char buff[16];

	TConfig * conf=config_all();

	time.DOW=conf->dow;
	time.DOM=conf->day;
	time.YEAR=conf->year;
	time.MONTH=conf->month;

	time.HOUR=conf->hour;
	time.MIN=conf->minute;

	rtc_cal_settime(&time);
}

CMDACTION(timecmd)
{
	if(argc==2 && strcmp(argv[1], "show")==0) /* time show */
	{
		time_print();
	}
	else if((argc==8 || argc==9) && strcmp(argv[1], "set")==0) /* time set <DOW> <YEAR> <MONTH> <DAY> <HOUR> <MIN> [<SEC>]*/
	{
		int i;
		strupr(argv[2]);
		for(i=0; i<7; i++)
		{
			if(strcmp(weekdates[i], argv[2])==0)
				break;
		}

		if(i>=7)
		{
			xprintf("DOW is MON, TUE etc.");
			return CMDLINE_FAIL;
		}

		RTC_TIME_Type time;

		time.DOW=i;
		time.YEAR=atoi(argv[3]);
		time.MONTH=atoi(argv[4]);
		time.DOM=atoi(argv[5]);

		time.HOUR=atoi(argv[6]);
		time.MIN=atoi(argv[7]);
		time.SEC=( (argc==9) ? atoi(argv[8]) :  0);

		rtc_cal_settime(&time);

		time_store();
		time_print();
	}
	else
	{
		xprintf("Time commands help:\n"
				" time show\n"
				" time set <DOW> <YEAR> <MONTH> <DAY> <HOUR> <MIN> [<SEC>]");
	}

	return CMDLINE_OK;

}
