/*
 * logger.c
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 */


#include "FreeRTOS.h"
#include "portable.h" // free, malloc etc.
#include "semphr.h"
#include "task.h"
#include "ade7753.h"
#include "mymath.h"
#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "monitor.h"
#include "diskio.h"
#include "ff.h"
#include "leds.h"
#include "ctype.h"
#include "string.h"

#include "rtc_cal.h"

#define UART_OUT
#ifdef UART_OUT
#include "comm.h"
#define OUT comm_puts
#else
#define OUT(...)
#endif

#define MONTHLOG_CLEAN(a) \
	a.active=0.0f; \
	a.apparent=0.0f; \
	a.year=0; \
	a.month=0;

extern FATFS fatfs;
static xSemaphoreHandle xSemLog=NULL;
static FIL file;
static FILINFO fileinfo;

static RTC_TIME_Type time;
static char buff[64];
static TEnergy energy;

static TLogHistogram histDOW[7];
static TLogHistogram histHOUR[12]; // 2 hours element
static TLogMonth conSum;
static TLogMonth currentMonth;
static TLogMonth lastMonth;

static inline int logger_init()
{
	WORD res;
	portENTER_CRITICAL();
	{
		xSemLog=xSemaphoreCreateMutex();
		if((res=(WORD)disk_initialize(LOG_DRIVER)))
			goto init_fail;

		if((res=(WORD)f_mount(LOG_DRIVER, &fatfs)))
			goto init_fail;
	}
	portEXIT_CRITICAL();
	return 0;

init_fail:
	portEXIT_CRITICAL();
	return res;
}

static inline void CreateTimeLine(char * data, uint32_t size)
{
	rtc_cal_gettime(&time);

	sprintf(data, "%u;%u;%u;%u;%u;%u;",
			(UINT)time.DOW, (UINT)time.YEAR, (UINT)time.MONTH, (UINT)time.DOM,
			(UINT)time.HOUR, (UINT)time.MIN);
}

static inline void WriteMonth(float act, float app, uint16_t year, uint8_t month)
{
	conSum.active+=act;
	conSum.apparent+=app;

	if(currentMonth.month==month && currentMonth.year==year)
	{
		currentMonth.active+=act;
		currentMonth.apparent+=app;
	}

	else if(currentMonth.year < year || (currentMonth.year==year && currentMonth.month < month)) // next month
	{
		lastMonth=currentMonth;
		currentMonth.year=year;
		currentMonth.month=month;
		currentMonth.active=act;
		currentMonth.apparent=app;
	}


	else if(lastMonth.month==month && lastMonth.year==year)
	{
		lastMonth.active+=act;
		lastMonth.apparent+=app;
	}



}

void log_enter()
{
	if(xSemLog==NULL)
	{
		if(logger_init()!=0) return;
	}

	xSemaphoreTake(xSemLog, portMAX_DELAY);
}

void log_leave()
{
	xSemaphoreGive(xSemLog);
}

void log_get_hist(TLogHistogram ** DOW, TLogHistogram ** HOUR)
{
	*DOW= histDOW;
	*HOUR=histHOUR;
}


void log_get_con(TLogMonth ** last, TLogMonth ** current, TLogMonth ** sum)
{
	*last=&lastMonth;
	*current=&currentMonth;
	*sum=&conSum;
}

void log_write()
{
	log_enter();
	{
		//xprintf("Log2");
		if(f_open(&file, LOG_FILE, FA_WRITE | FA_OPEN_ALWAYS)==0)
		{
			/* Seek to end */
			if(file.fsize>0)
			{
				f_lseek(&file, file.fsize);
			}

			if(file.fsize<LOG_MAX_SIZE)
			{
				UINT written;
				UINT interval=ade_longtime(&energy);

				if(interval<40)
					goto exit;


				float active=((energy.active - 3300)* 0.000131817472631516f) / ((float)interval);
				float apparent=((energy.apparent - 246000) * 0.000158323258101552f) / ((float)interval);
				float con_active=((energy.active - 3300)* 0.000131817472631516f) / (60.0f); // W/min
				float con_apparent=((energy.apparent - 246000) * 0.000158323258101552f) / (60.0f); // VA/min

				if(active > ADE_MAX || apparent > ADE_MAX)
					goto exit;

				if(active < 0 ) {active=0; con_active=0;}
				if(apparent < 0) {apparent=0; con_apparent=0;}
				if(active > apparent) {apparent=active; con_apparent=con_active;}

				//xprintf("Log: %s", buff);
				CreateTimeLine(buff, sizeof(buff));
				f_write(&file, buff, strlen(buff), &written);

				sprintf(buff, "%d.%02d;%d.%02d;%u;%d.%02d;%d.%02d\r\n",
						(int)active, ((int)(active*100))%100,
						(int)apparent, ((int)(apparent*100))%100,
						interval,
						(int)con_active, ((int)(con_active*100))%100,
						(int)con_apparent, ((int)(con_apparent *100))%100
						);
				f_write(&file, buff, strlen(buff), &written);

				portENTER_CRITICAL();
				{
					rtc_cal_gettime(&time);
					/* Writes data to histogram */
					histDOW[time.DOW].active+=active;
					histDOW[time.DOW].apparent+=apparent;
					histDOW[time.DOW].timesum++;

					histHOUR[time.HOUR >> 1].active+=active;
					histHOUR[time.HOUR >> 1].apparent+=apparent;
					histHOUR[time.HOUR >> 1].timesum++;

					WriteMonth(con_active,con_apparent,time.YEAR,time.MONTH);
				}
				portEXIT_CRITICAL();

				comm_puts("Log written");
				comm_puts(buff);
				comm_puts("\r\n");



			} /* Maximum size limit*/
		exit:
			f_close(&file);

		}/* If open */
		//xprintf("Log3");

	}
	log_leave();
}

static void save_hist_unsafe()
{
	DWORD size=0;
	if(f_stat(LOG_FILE, &fileinfo)==FR_OK)
	{
		size=fileinfo.fsize;
	}

	if(f_open(&file, "hist.bin", FA_WRITE | FA_CREATE_ALWAYS )==FR_OK)
	{
		UINT written;
		f_write(&file, &size, sizeof(size), &written);

		for(int i=0; i<7; i++)
		{
			f_write(&file, &( histDOW[i]), sizeof(TLogHistogram), &written);
		}

		for(int i=0; i<12; i++)
		{
			f_write(&file, &(histHOUR[i]), sizeof(TLogHistogram), &written);
		}


		f_write(&file, &(lastMonth), sizeof(TLogMonth), &written);
		f_write(&file, &(currentMonth), sizeof(TLogMonth), &written);
		f_write(&file, &(conSum), sizeof(TLogMonth), &written);

		f_close(&file);
	}
}

static DWORD open_hist_unsafe()
{
	DWORD size=0;
	if(f_open(&file, "hist.bin", FA_READ | FA_OPEN_EXISTING )==FR_OK)
	{
		UINT read;

		if(f_read(&file, &size, sizeof(size), &read)!=0)
			goto error;

		if(!read) goto error;

		for(int i=0; i<7; i++)
		{
			if(f_read(&file, &( histDOW[i]), sizeof(TLogHistogram), &read) != 0)
				goto error;

#ifdef DBG
			for(BYTE * b=(BYTE *)&( histDOW[i]); b<(BYTE *)&( histDOW[i+1]); b++)
			{
				xprintf("%02x ", *b);
			}
			xprintf("\r\n");
#endif
			if(!read) goto error;

		}

		for(int i=0; i<12; i++)
		{
			if(f_read(&file, &(histHOUR[i]), sizeof(TLogHistogram), &read) != 0)
				goto error;

			if(!read) goto error;

		}


		if(f_read(&file, &(lastMonth), sizeof(TLogMonth), &read) != 0)
			goto error;
		if(!read) goto error;

		if(f_read(&file, &(currentMonth), sizeof(TLogMonth), &read) != 0)
					goto error;
		if(!read) goto error;


		if(f_read(&file, &(conSum), sizeof(TLogMonth), &read) != 0)
					goto error;
		if(!read) goto error;

		f_close(&file);
	}
	else
	{
		xprintf("fopen_error - hist\r\n");
	}

	xprintf("Read %u\r\n", size);
	return size;

error:
	f_close(&file);
	xprintf("Read error\r\n");
	return 0;
}

/* Saves histogram to binary file */
void log_save_hist()
{
	log_enter();
	{
		save_hist_unsafe();
	}
	log_leave();
}

void log_clean()
{
	log_enter();

	f_unlink(LOG_FILE);
	/*if(f_open(&file, LOG_FILE, FA_WRITE | FA_OPEN_EXISTING )==FR_OK)
	{
		f_close(&file);
	}*/

	log_leave();
}

void log_histogram(int cache)
{
	log_enter();

	/* clear all */
	for(int i=0; i<7; i++)
	{
		histDOW[i].apparent=0.0f;
		histDOW[i].active=0.0f;
		histDOW[i].timesum=0;
	}
	for(int i=0; i<12; i++)
	{
		histHOUR[i].apparent=0.0f;
		histHOUR[i].active=0.0f;
		histHOUR[i].timesum=0;
	}

	MONTHLOG_CLEAN(currentMonth);
	MONTHLOG_CLEAN(lastMonth);
	MONTHLOG_CLEAN(conSum);

	UINT reads;
	int column=0;
	float apparent=0;
	float active=0;
	float con_apparent=0;
	float con_active=0;
	int dow=0;
	int hour=0;
	int month=0, year=0;
	int pos=0;
	char data[32]="";
	DWORD seek=0;

	if(cache)
		seek=open_hist_unsafe();

	if(f_open(&file, LOG_FILE, FA_READ | FA_OPEN_EXISTING)==0)
	{
		if(seek)
			f_lseek(&file, seek);

		while(1)
		{
			if(f_read(&file, buff, sizeof(buff), &reads)!=0)
			{
				xprintf("Read fail");
				break;
			}


			if(!reads)
				break;

			for(UINT i=0;i<reads; i++)
			{
				switch(buff[i])
				{
				case ';': /* new column */
					data[pos]=0;

					/* test column */
					if(column==0) /* DOW */
					{
						dow=atoi(data);
					}
					if(column==1) /* year */
					{
						year=atoi(data);
					}
					if(column==2) /* month */
					{
						month=atoi(data);
					}
					else if(column==4) /* hour */
					{
						hour=atoi(data);
					}
					else if(column==6) /* active */
					{
						active=atoff(data);
					}
					else if(column==7) /* apparent */
					{
						apparent=atoff(data);
					}

					else if(column==9) /* active work */
					{
						//comm_puts(data);
						//comm_puts("\r\n");
						con_active=atoff(data);
					}
					pos=0;
					column++;
					break;
				case '\r':
					break;
				case '\n':
					data[pos]=0;
					con_apparent=atoff(data);

					//comm_puts("start");
					WriteMonth(con_active,con_apparent,year, month);

					//comm_puts("end");

					//xprintf("Dow %d", dow);
					//xprintf("Write log %d %d\r\nBuff:%s", dow, hour, buff);
					histDOW[dow].active+=active;
					histDOW[dow].apparent+=apparent;
					histDOW[dow].timesum++;

					histHOUR[hour>>1].active+=active;
					histHOUR[hour>>1].apparent+=apparent;
					histHOUR[hour>>1].timesum++;
					//comm_puts("hist1");
					pos=0;
					column=0;
					break;
				default:
					data[pos++]=buff[i];
					break;

				} /* switch buff[i] */

			} /* foreach symbol */

		} /* while read */

		f_close(&file);
	} /* if open */
	else
	{
		xprintf("Open fails");
	}


	comm_puts("Leaves");
	log_leave();

}

void log_print()
{
	xprintf("Day of week: 0=Sunday\r\n");
	for(int i=0; i<7; i++)
	{
		float data=histDOW[i].active / histDOW[i].timesum;
		printfloat(data,buff);
		xprintf("  %d: Active: %s\r\n",i, buff);
		data=histDOW[i].apparent / histDOW[i].timesum;
		printfloat(data,buff);
		xprintf("      Apparent: %s\r\n",buff);
	}

	xprintf("Hours:\r\n");
	for(int i=0; i<12; i++)
	{
		float data=histHOUR[i].active / histHOUR[i].timesum;
		printfloat(data,buff);
		xprintf("  %d-%d: Active: %s\r\n",i*2, i*2+2, buff);
		data=histDOW[i].apparent / histHOUR[i].timesum;
		printfloat(data,buff);
		xprintf("      Apparent: %s\r\n",buff);
	}

}
