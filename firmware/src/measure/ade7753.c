#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include <math.h>
#include "rtc_cal.h"

#include <stdlib.h>
#include <stdint.h>

#include "ade7753.h"
#include "mymath.h"

#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_rtc.h"
#include "lpc17xx_clkpwr.h"

#include "monitor.h"

/* energy in last second */
static TEnergy energyDerivation;
static TEnergy energySum;
static RTC_TIME_Type startTime;


/* Delay until semaphore is not created */
#define xSemaphoreTakeNotNull(sem, delay)              \
	while(sem == NULL)				               \
		vTaskDelay(200);     					       \
	xSemaphoreTake(sem, delay);

#define GET3B(addr, output) ade_read(addr, getbuff, 3); \
		uint32_t output=(getbuff[1] << 16) | \
		(getbuff[2] << 8) | \
		(getbuff[3]);

// Selection of gain (more in datasheet)
#define ADE_GAIN 0x0b

static xSemaphoreHandle xSemADE = NULL;

static inline uint8_t ade_rw(uint8_t out);

void ade_init() {
	portENTER_CRITICAL();
	{
		xSemADE = xSemaphoreCreateMutex();
		/* Take the semaphore */
		xSemaphoreTake(xSemADE, portMAX_DELAY);

		/* ************************************************** */
		/*                Pin configuration                   */
		/* ************************************************** */
		PINSEL_CFG_Type PinCfg;
		SSP_CFG_Type SSP_ConfigStruct;

		// SSEL1 P0.6 as GPIO, pull-up mounted but driven push/pull here
		PinCfg.Funcnum = PINSEL_FUNC_0;
		PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
		PinCfg.Pinmode = PINSEL_PINMODE_TRISTATE;
		PinCfg.Pinnum = 6;
		PinCfg.Portnum = 0;

		GPIO_SetDir(0, (1 << 6), 1);
		PINSEL_ConfigPin(&PinCfg);

		// SCK P0.7 alternate function 0b10
		PinCfg.Funcnum = PINSEL_FUNC_2;
		PinCfg.Pinnum = 7;
		PINSEL_ConfigPin(&PinCfg);
		// MISO P0.8
		PinCfg.Pinmode = PINSEL_PINMODE_PULLDOWN;
		PinCfg.Pinnum = 8;
		PINSEL_ConfigPin(&PinCfg);
		// MOSI P0.9
		PinCfg.Pinnum = 9;
		PINSEL_ConfigPin(&PinCfg);

		/* ************************************************** */
		/*                SSP configuration                   */
		/*                  with 7 databits                   */
		/* ************************************************** */
		SSP_ConfigStructInit(&SSP_ConfigStruct);

		/* Switch to second phase, hight CLK and 7 databits */
		SSP_ConfigStruct.CPHA = SSP_CPHA_SECOND;
		SSP_ConfigStruct.CPOL = SSP_CPOL_HI;
		SSP_ConfigStruct.Databit = SSP_DATABIT_7;

		SSP_Init(LPC_SSP1, &SSP_ConfigStruct);

		CLKPWR_SetPCLKDiv(CLKPWR_PCLKSEL_SSP1, CLKPWR_PCLKSEL_CCLK_DIV_4);

		SSP_Cmd(LPC_SSP1, ENABLE);

		/* wait for busy gone */
		while (LPC_SSP1->SR & SSP_SR_BSY) {
			;
		}

		SSP_SetClock(LPC_SSP1, 400);

		/* drain SPI RX FIFO */
		while (LPC_SSP1->SR & SSP_SR_RNE) {
			volatile uint32_t dummy = LPC_SSP1->DR;
			(void) dummy;
		}

		/* send data to read 0x01 with 7 bites (1st tick of clock has been done during reset */
		ade_rw(0x01);
		SSP_Cmd(LPC_SSP1, DISABLE);

		/* ************************************************** */
		/*                SSP configuration                   */
		/*                  with 8 databits                   */
		/* ************************************************** */

		/* Uses the configuration from previous block, changes only 8 databits */
		SSP_ConfigStruct.Databit = SSP_DATABIT_8;

		SSP_Init(LPC_SSP1, &SSP_ConfigStruct);
		SSP_Cmd(LPC_SSP1, ENABLE);

		/* wait for busy gone */
		while (LPC_SSP1->SR & SSP_SR_BSY) {
			;
		}

		SSP_SetClock(LPC_SSP1, 400);

		/* drain SPI RX FIFO */
		while (LPC_SSP1->SR & SSP_SR_RNE) {
			volatile uint32_t dummy = LPC_SSP1->DR;
			(void) dummy;
		}

		/* Read data from address 0x01 - 3 uint8_ts */
		ade_rw(0x00);
		ade_rw(0x00);
		ade_rw(0x00);


		rtc_cal_gettime(&startTime);

		/* Allow to communicate via SPI to other processes */
		xSemaphoreGive(xSemADE);

		/* Setting up GAIN on address 0x0f (MSB is 1 to write)*/
		ade_rw(0x8f);
		ade_rw(ADE_GAIN);
	}
	portEXIT_CRITICAL(); /* End critical section */
}

static inline uint8_t ade_rw(uint8_t out) {
	uint8_t in;

	LPC_SSP1->DR = out;
	while (LPC_SSP1->SR & SSP_SR_BSY) {
		;
	}
	in = LPC_SSP1->DR;
#ifdef DEBUG
	xprintf("  * SPI 0x%02x -> 0x%02x\n", out, in);
#endif
	vTaskDelay(20);

	return in;
}

void ade_write(uint8_t addr, uint8_t * data, uint32_t size) {
	/* Take the semaphore */
	xSemaphoreTakeNotNull(xSemADE, portMAX_DELAY);

	ade_rw(addr | 0x80); /* prvni bit (MSB) nastaveny, za nim v 0 */
	for (uint32_t i = 0; i < size; i++, data++)
		ade_rw(*data);

	xSemaphoreGive(xSemADE);
}

void ade_read(uint8_t addr, uint8_t * data, uint32_t size) {
	/* Take the semaphore */
	xSemaphoreTakeNotNull(xSemADE, portMAX_DELAY)

	ade_rw(addr);
	for (uint32_t i = 0; i < size; i++, data++)
		*data = ade_rw(0x00);

	xSemaphoreGive(xSemADE);
}

uint8_t ade_spi(uint8_t data) {
	/* Take the semaphore */
	xSemaphoreTakeNotNull(xSemADE, portMAX_DELAY)

	uint8_t ret = ade_rw(data);

	xSemaphoreGive(xSemADE);

	return ret;
}

/**
 * @brief gets current derivation values
 * @param active P
 * @param apparent S
 * @param reactive R
 * @param factor cos(phi)
 */
void ade_derivation(float * active, float * apparent, float * reactive, float * factor, float * power, float * current)
{
	*active=(energyDerivation.active - 165.0f) * 0.0000987923153172505f;
	*apparent=(energyDerivation.apparent - 5541) * 0.00011860476016884f;
	if(*active<0 || *active>ADE_MAX) *active=0.0f;
	if(*apparent<0 || *apparent>ADE_MAX) *apparent=0.0f;

	if(*active>*apparent) *active=*apparent;



	*reactive=mysqrt((*apparent * *apparent) - (*active * *active));
	if(*reactive>*apparent) *reactive=*apparent;

	*factor=*active / *apparent;
	*power=0.0; //upower;
	*current=0.0; //ucurrent;
}

static unsigned int time_dif(RTC_TIME_Type * a, RTC_TIME_Type * b)
{
	int ret=0;
	ret+=a->SEC - b->SEC;
	ret+=(a->MIN - b->MIN)*60;
	ret+=(a->HOUR - b->HOUR)*3600;
	ret+=((a->DOW - b->DOW) % 7)* (24 * 3600);
	return (unsigned int)ret;
}

/**
 * @brief converts 24 signed number to int32 signed
 */
static inline int32_t getInt(uint32_t number)
{
	if(number & 0x800000) // 24th bit set -> negative
	{
		number |= 0xFF000000;
	}
	return (int32_t) number;
}

/**
 * @brief Gets energy for longer time
 * @return number of second of logging
 */
unsigned int ade_longtime(TEnergy * energy)
{
	RTC_TIME_Type current;
	rtc_cal_gettime(&current);
	/* Count second difference between times */
	unsigned int timesum=time_dif(&current, &startTime);
	portENTER_CRITICAL();
	{
		energy->active=energySum.active;
		energy->apparent=energySum.apparent;

		rtc_cal_gettime(&startTime);
		energySum.active=0;
		energySum.apparent=0;
	}
	portEXIT_CRITICAL();
	return timesum;
}



void vAdeTask(void *pvParameters)
{
	for (;;) {
		/* Wait for initialization */
		while(xSemADE==NULL)
		{
			vTaskDelay(100);
		}

		//comm_puts("tady1");
		ledToggle(LED3);
		vTaskDelay(1000 / portTICK_RATE_MS);

		/* Counts power */
		uint8_t getbuff[3];

		GET3B(0x03, active);
		GET3B(0x06, apparent);

		int32_t iact=getInt(active);
		int32_t iapp=getInt(apparent);

		portENTER_CRITICAL();
		{
			energyDerivation.active=iact;
			energyDerivation.apparent=iapp;

			energySum.active+=iact;
			energySum.apparent+=iapp;
		}
		portEXIT_CRITICAL();
	} /* for infinite loop */
} /* vAdeTask(..) */
