/*
 * iptools.h
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 */

#ifndef IPTOOLS_H_
#define IPTOOLS_H_
#include "uip.h"

/**
 * @brief Minimal size of output buffer to convert IP
 * "xxx.xxx.xxx.xxx0"
 */
#define IP_STRING 17


/***
 * @brief Convert IP address to string
 * @param data buffer with min length IP_STRING
 * @param addr IPv4 address
 */
void ip_to_string(char * data, uip_ipaddr_t addr);

/***
 * @brief Converts data to ipaddr
 * @brief errors are ignored
 */
void ip_from_string(char * data, uip_ipaddr_t addr);


/***
 * @brief Initialization of IP stack from config file,
 * config must be initialized
 */
void ip_init();


#endif /* IPTOOLS_H_ */
