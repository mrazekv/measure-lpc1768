/*
 * cmdline.h
 *
 *  Created on: 28.2.2012
 *      Author: Vojta
 * Description: Accepts data from some stream, makes argc and argv vector
 */

#ifndef CMDLINE_H_
#define CMDLINE_H_
#include <stdlib.h>
#include <stdint.h>

#define CMDLINE_OK 0
#define CMDLINE_FAIL 1

uint32_t ParseHex(const char * data);

/**
 * @brief saves current time to config file (in cmdtime.c)
 */
void time_store();

/**
 * @brief saves current time to config file (in cmdtime.c)
 */
void time_restore();

/**
 * @brief Parses data from command line and do actions defined in cmdline.c
 * @return CMDLINE_OK on success, CMDLINE_FAIL on fail
 */
int cmdline_parse(char * data, uint32_t length);

typedef struct {
	char command[16];
	int (*operation)(int argc, char** argv);
} TCMDdata;

#define CMDDECLARE(name) int name(int,char**)
#define CMDACTION(name) int name(int argc,char** argv)

#endif /* CMDLINE_H_ */
