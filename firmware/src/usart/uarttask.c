#include "FreeRTOS.h"
#include <ctype.h>
#include <stdlib.h>
#include <stdint.h>
#include "task.h"
#include "ade7753.h"
#include "cmdline.h"

#include "lpc17xx_rtc.h"

#include "rtc_cal.h"
#include "config.h"
#include "logger.h"
#include "cron.h"

#include "comm.h"
#include "monitor.h"
#include "diskio.h"
#include "ff.h"

#include "iptools.h"
#include <string.h>

#include <usb.h>

#define LOAD_HIST
#define USE_USB

#ifdef USE_USB
#define CHARPUT VCOM_xput
#define CHARGET VCOM_xget
#else
#define CHARPUT xcomm_put
#define CHARGET xcomm_get
#endif

BYTE Buff[32];

void vUartTask( void *pvParameters )
{
#ifdef USE_USB
	USB_start();
#endif
	comm_init();
	comm_puts("Ethernet electricity measurement\r\n");

#ifdef DEBUG
	comm_puts("DEBUG build\r\n");
#endif

	xfunc_out = xcomm_put;
	xfunc_in  = xcomm_get;
	xprintf("xprintf is working\n");
	
	/* Config & SD card */
	if(config_init()!=0)
	{
		xprintf("Config init failed");
		ledOn(LED1 | LED2 | LED3);
		for(;;) ;
	}

	time_restore();
	comm_puts("Time initialized\r\n");

	ade_init();
	comm_puts("ADE initialized\r\n");

	ip_init();
	comm_puts("IP stack initialized\r\n");


	//log_write();

#ifdef LOAD_HIST
	comm_puts("Wait for loading histogram ...\r\n");
	log_histogram(1);
	comm_puts("        ... done\r\n");
#endif

	char buff[64];

	xfunc_out = CHARPUT;
	xfunc_in  = CHARGET;
  
	uint32_t pos=0;
	xprintf("> ");
	for(;;)
	{
		char c= (char) CHARGET(); //comm_get();

		if(c=='\r' || c=='\n' || pos==sizeof(buff)-1) /* ending command */
		{
			buff[pos]=0;
			xprintf("\n");

			uint32_t i;
			char command[32];

			if(cmdline_parse(buff, sizeof(buff)) != CMDLINE_OK)
			{
				xprintf("Operation hasn't been succesfull");
			}

			/* Restart */
			pos=0; 
			xprintf("\n> ");
		}
		else /* not ending command */
		{
			if(c==0x7f) /* backspace */
			{
				if(pos>0)
				{
					pos--;
					CHARPUT(c);
				}
				continue;
			}
			else if(c == 0x1b) /* control keys */
			{
				continue;
			}
			else if(!isprint(c))
			{
				c='?';
			}

			if(pos>=sizeof(buff))
				continue;

			buff[pos++]=c;
			CHARPUT(c);
		}
	}
  
exit:
  vTaskDelete(NULL);
}
