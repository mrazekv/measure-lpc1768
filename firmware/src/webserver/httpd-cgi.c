/**
 * \addtogroup httpd
 * @{
 */

/**
 * \file
 *         Web server script interface
 * \author
 *         Adam Dunkels <adam@sics.se>
 *
 */

/*
 * Copyright (c) 2001-2006, Adam Dunkels.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the uIP TCP/IP stack.
 *
 * $Id: httpd-cgi.c,v 1.2 2006/06/11 21:46:37 adam Exp $
 *
 */

#include "uip.h"
#include "psock.h"
#include "httpd.h"
#include "httpd-cgi.h"
#include "httpd-fs.h"
#include "mymath.h"
#include "ade7753.h"
#include "diskio.h"
#include "ff.h"
#include "logger.h"

#include <stdio.h>
#include <string.h>

HTTPD_CGI_CALL(block, "current_block", current_block );
HTTPD_CGI_CALL(xml, "data_xml", data_xml );
HTTPD_CGI_CALL(histogram, "histogram", histogram_funct );
HTTPD_CGI_CALL(consumption, "consumption_block", consumption_block );
HTTPD_CGI_CALL(cxml, "consumption_xml", consumption_xml );


static const struct httpd_cgi_call *calls[] = {&block, &consumption, &histogram, &xml, &cxml, NULL };

/*---------------------------------------------------------------------------*/
static
PT_THREAD(nullfunction(struct httpd_state *s, char *ptr))
{
  PSOCK_BEGIN(&s->sout);
  ( void ) ptr;
  PSOCK_END(&s->sout);
}

/*---------------------------------------------------------------------------*/
httpd_cgifunction
httpd_cgi(char *name)
{
  const struct httpd_cgi_call **f;

  /* Find the matching name in the table, return the function. */
  for(f = calls; *f != NULL; ++f) {
    if(strncmp((*f)->name, name, strlen((*f)->name)) == 0) {
      return (*f)->function;
    }
  }
  return nullfunction;
}


char *pcStatus;
unsigned long ulString;


static PT_THREAD(generate_current_row(const char * title, const char * units, float data))
{
	sprintf(uip_appdata, "<tr><td>%s</td><td class=\"data\">", title);
	PRINTFLOAT(data,uip_appdata + strlen(uip_appdata));
	sprintf(uip_appdata + strlen(uip_appdata), " %s</td></tr>",units);

	return strlen( uip_appdata);
}
static PT_THREAD(generate_xml_row(const char * title, const char * units, float data))
{
	sprintf(uip_appdata, "<%s>\n\t<value>", title);
	PRINTFLOAT(data,uip_appdata + strlen(uip_appdata));
	sprintf(uip_appdata + strlen(uip_appdata), "</value>\n\t<unit>%s</unit>\n</%s>",units, title);

	return strlen( uip_appdata);
}


static PT_THREAD(current_block(struct httpd_state *s, char *ptr))
{
	float active, apparent, reactive, factor, power, current;
	ade_derivation(&active, &apparent, &reactive, &factor, &power, &current);

	PSOCK_BEGIN(&s->sout);
	( void ) ptr;
	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Active (P)", "W", active));
	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Reactive (Q)", "VAr", reactive));
	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Apparent (S)", "VA", apparent));
	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Factor (cos(phi))", "", factor));
	//PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("I<sub>RMS</sub>", "A", current));
	//PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("U<sub>RMS</sub>", "V", power));
	PSOCK_END(&s->sout);
}


static PT_THREAD(consumption_block(struct httpd_state *s, char *ptr))
{
	TLogMonth * current;
	TLogMonth * last;
	TLogMonth * sum;
	log_get_con(&last, &current, &sum);

	PSOCK_BEGIN(&s->sout);
	( void ) ptr;

	sprintf(uip_appdata, "<tr><td colspan=2 class=\"title\">Previous month %d/%04d</td></tr>", last->month, last->year);
	PSOCK_SEND_STR(&s->sout, uip_appdata);

	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Active work", "Wh", last->active / 60));
	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Apparent work", "VAh", last->apparent / 60));

	sprintf(uip_appdata, "<tr><td colspan=2 class=\"title\">This month %d/%04d</td></tr>", current->month, current->year);
	PSOCK_SEND_STR(&s->sout, uip_appdata);

	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Active work", "Wh", current->active / 60));
	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Apparent work", "VAh", current->apparent / 60));


	sprintf(uip_appdata, "<tr><td colspan=2 class=\"title\">Summary</td></tr>");
	PSOCK_SEND_STR(&s->sout, uip_appdata);

	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Active work", "Wh", sum->active / 60));
	PSOCK_SEND(&s->sout, uip_appdata, generate_current_row("Apparent work", "VAh", sum->apparent / 60));

	PSOCK_END(&s->sout);
}

static PT_THREAD(data_xml(struct httpd_state *s, char *ptr))
{
	float active, apparent, reactive, factor, power, current;
	ade_derivation(&active, &apparent, &reactive, &factor, &power, &current);

  PSOCK_BEGIN(&s->sout);
  ( void ) ptr;
	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("active", "W", active));
	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("reactive", "VAr", reactive));
	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("apparent", "VA", apparent));
	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("factor", "", factor));
	//PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("irms", "A", current));
	//PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("urms", "V", power));
  PSOCK_END(&s->sout);
}


static PT_THREAD(consumption_xml(struct httpd_state *s, char *ptr))
{
	TLogMonth * current;
	TLogMonth * last;
	TLogMonth * sum;
	log_get_con(&last, &current, &sum);

  PSOCK_BEGIN(&s->sout);
  ( void ) ptr;
	sprintf(uip_appdata, "<last>\n<month>%d</month>\n<year>%04d</year>", last->month, last->year);
	PSOCK_SEND_STR(&s->sout, uip_appdata);

	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("active", "Wh", last->active));
	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("apparent", "VAh", last->apparent));


	sprintf(uip_appdata, "</last><current>\n<month>%d</month>\n<year>%04d</year>", current->month, current->year);
	PSOCK_SEND_STR(&s->sout, uip_appdata);

	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("active", "Wh", current->active));
	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("apparent", "VAh", current->apparent));


	sprintf(uip_appdata, "</current><summary>");
	PSOCK_SEND_STR(&s->sout, uip_appdata);

	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("active", "Wh", sum->active));
	PSOCK_SEND(&s->sout, uip_appdata, generate_xml_row("apparent", "VAh", sum->apparent));

	sprintf(uip_appdata, "</summary>");
	PSOCK_SEND_STR(&s->sout, uip_appdata);
  PSOCK_END(&s->sout);
}


#define PSOCK_SEND_FLOAT(out, nmr)  \
   PRINTFLOAT(nmr,uip_appdata); \
   PSOCK_SEND_STR(out, uip_appdata)


static PT_THREAD(histogram_funct(struct httpd_state *s, char *ptr))
{
	TLogHistogram * dow;
	TLogHistogram * hour;
	float data;
	log_get_hist(&dow, &hour);


	PSOCK_BEGIN(&s->sout);

    /* DOW */
	sprintf(uip_appdata, " =GEN= */\ndow=[");
	for(int i=0; i<7; i++)
	{
		sprintf(uip_appdata+strlen(uip_appdata), "[");
		data= dow[i].active / dow[i].timesum;
		PRINTFLOAT(data, uip_appdata+strlen(uip_appdata));
		sprintf(uip_appdata+strlen(uip_appdata), ",");
		data= dow[i].apparent / dow[i].timesum;
		PRINTFLOAT(data, uip_appdata+strlen(uip_appdata));
		sprintf(uip_appdata+strlen(uip_appdata), "],");

	}
	sprintf(uip_appdata+strlen(uip_appdata), "0];\n");
	PSOCK_SEND_STR(&s->sout, uip_appdata);

	/* HOUR */
	sprintf(uip_appdata, "hour=[");
	for(int i=0; i<12; i++)
	{
		sprintf(uip_appdata+strlen(uip_appdata), "[");
		data= hour[i].active / hour[i].timesum;
		PRINTFLOAT(data, uip_appdata+strlen(uip_appdata));
		sprintf(uip_appdata+strlen(uip_appdata), ",");
		data= hour[i].apparent / hour[i].timesum;
		PRINTFLOAT(data, uip_appdata+strlen(uip_appdata));
		sprintf(uip_appdata+strlen(uip_appdata), "],");

	}
	sprintf(uip_appdata+strlen(uip_appdata), "0];\n/*");
	PSOCK_SEND_STR(&s->sout, uip_appdata);


  PSOCK_END(&s->sout);
}
/** @} */






