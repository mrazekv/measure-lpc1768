\chapter{Měření elektrického výkonu a energie}\label{chap:mereni}
Elektrický výkon má odlišné parametry ve střídavém a stejnosměrném obvodu.
Při další analýze se budeme zaměřovat na měření výkonu a energie v~jednofázových obvodech se střídavými veličinami.

% 
% V~poslední době došlo na trhu k~masivnímu nárustu různých integrovaných obvodů, které umožňují měření elektrické energie.
% Obvody měří nejen aktuální množství odebírané energie, ale také celkovou spotřebu.
% 
% Jednotivé obvody se liší v měřených veličinách a ve způsobu komunikace.
% Základním způsobem měření je jedoduchý obvod, který má pouze pulzní výstup.
% Každý pulz znamená konstatní přírustek elektrické práce.
% Dalším způsobem je přímé čtení přes sběrnici (I$^2$C, SPI, UART). 
% Při využití této komunikace je možné číst i další veličiny.

\section{Výkon}
V~obvodu se stejnosměrnými veličinami lze definovat elektrický výkon pomocí proudu a napětí nebo elektrickou prací (\ref{eq:PW}).
Jeho základní jednotkou je 1 W.
\begin{equation} \label{eq:PW}
P= U~\cdot I~= {dW \over dt}
\end{equation}

V~obvodech, kde se vyskytují střídavé veličiny, se však situace komplikuje, neboť v~síti může docházet k~posunu mezi proudem a napětím.
Je-li zdroj zatěžován obvodem mající rezistivní charakter, k~posunu nedochází.
Ve většině případů má však zátěž induktivní charakter, což způsobuje posun mezi proudem a napětím.
Tento posun označujeme jako fázový posun $\varphi$.
Parametrem odvozeným z~fázového posunu je účiník $\cos{\varphi}$, který určuje charakter zátěže.
Je-li jeho hodnota 1, zátěž má rezistivní charakter.
Kladný účiník menší než 1 (na intervalu $<0,1)$) vyjadřuje induktivní zátěž a záporný (na intervalu $(-1,0)$) kapacitní.

Pro zjednodušení se zaměříme na jednofázovou soustavu s~harmonickým průběhem proudu a napětí.
Okamžité hodnoty proudu a napětí ($i$ a $u$) i efektivní hodnoty ($I$ a $U$) jsou vektorovými veličinami.
Pokud proud $I=I_S$ vektorově rozložíme podle obrázku \ref{fig:proudy}, vzniknou nám další složky $I_Q$ a $I_P$.
\begin{figure}[ht!]
  \centering
  \subfloat[Rozložení proudů]{\label{fig:proudy}\includegraphics[height=5cm]{fig/proudy}}
  \subfloat[Rozložení výkonů]{\label{fig:vykony}\includegraphics[height=5cm]{fig/vykony}}
  \caption{Rozložení veličin jednofázové soustavy}
  \label{fig:veliciny}
\end{figure}
Proud $I_S$ nazýváme zdánlivým, $I_Q$ tvoří jeho jalovou složku a $I_P$ jeho činnou složku.
Většinou je pro nás však důležitější výkon, jehož rozložení odpovídá rozložení proudů, jak vidíme na obrázku \ref{fig:vykony}.

\begin{eqnarray}
P & = & U~\cdot I~\cdot \cos{\varphi} ~ \mathrm{[W]}\label{eq:PUI}\\
Q & = & U~\cdot I~\cdot \sin{\varphi} ~ \mathrm{[VAr]} \label{eq:QUI}\\
S & = & U~\cdot I ~ \mathrm{[VA]} \label{eq:SUI}
\end{eqnarray}

Při znalosti efektivních hodnot napětí a proudu a jejich vzájemného posunu můžeme jednotlivé výkony dopočítat pomocí vzorců \ref{eq:PUI} až \ref{eq:SUI}.
Velmi často se využívá i toho, že trojúhelník $ S P Q$ je pravoúhlý a že v~něm platí Pythagorova věta $S^2=P^2+Q^2$.

Jednotlivé výkony je užitečné sledovat z~toho důvodu, že každý z~nich jinak zatěžuje rozvodnou síť.
Na vybuzení magnetického pole točivých strojů odebírá spotřebitel jalový proud $I_Q$, který je příčinou zvýšených výrobních nákladů energie.
Jalová energie $W_Q$, dodávaná z~elektrárny, nepředstavuje sama o~sobě žádnou spotřebu paliva v~elektrárně.
Při odběru jalové energie však rozvodnou soustavou prochází proud $I_S$, který způsobí větší ztráty na vodičích než proud $I_P$, který by soustavou protékal, kdyby jalová složka $I_Q$ byla nulová.
Výkonem elektrárny však musí být dodávána i energie odpovídající  ztrátám na vedení.
Abychom přenosem jalové energie neomezovali využití generátorů k~výrobě činné energie, snažíme se kompenzovat jalovou energii až u~spotřebitele (nebo na jiném nejbližším místě) synchronními kompenzátory a nebo statickými kondenzátory \cite{elm}.

\section{Klasické analogové metody měření výkonu a energie}
Pro analogové měření velmi často využíváme wattmetru.
Jedná se o~magnetodynamický systém, který obsahuje dvě cívky.
Jedna cívka funguje jako proudový snímač a druhá jako napěťový snímač.
Díky vzájemnému postavení dochází k~vychylování ručky přístroje a jsme schopni odečítat aktuální příkon, přesněji činný výkon $P$.

Pro měření zdánlivého výkonu $S$ většinou využíváme dodatečné zapojení ampérmetru a voltmetru.
Vlastní hodnotu spočítáme pomocí vzorce \ref{eq:SUI}.

Pro zjištění jalového výkonu $Q$ můžeme použít dvě metody.
První metodou je použití wattmetru s~fázovacím článkem (obrázek \ref{fig:fazclan}).
Na wattmetru uměle posuneme jednu z~veličin o~$90 ^\circ$.
Nejjednodušší je posunout napětí fázovacím článkem RC.

\begin{figure}[ht!]
  \begin{center}
    \includegraphics[height=5cm,keepaspectratio]{fig/rc}
    \caption{Fázovací článek}
    \label{fig:fazclan}
  \end{center}
\end{figure}

Pro kondenzátor $C$ musí platit to, že se blíží ideálnímu kondenzátoru --- nemá rezistivní složku, ale má pouze reaktanci (vzorec \ref{eq:reaktance}).
Aby byl správný fázový posuv na wattmetru, musí platit to, že reaktance kondenzátoru odpovídá rezistivitě rezistoru $R=X_C$.

\begin{equation}
X_C={1 \over \omega C} = {1 \over 2 \pi C}
\label{eq:reaktance}
\end{equation}
Díky tomu, že reaktance kondenzátoru je frekvenčně závislá, může být tento fázovací článek použit pouze pro jednu frekvenci, pro kterou byl rezistor R zvolen.
Druhou možností je to, že vypočítáme jalovou složku z~$P$ a $S$ ze znalosti jejich pravoúhlého rozložení.

Měření elektrické energie analogovými metodami je použito například v~klasickém elektroměru.
Tento elektroměr obsahuje dvě cívky podobně jako wattmetr.
Na rozdíl od wattmetru však tyto dvě cívky působí na hliníkový kotouč, který otáčí s~hřídelí vedoucí do mechanického čítače, který zaznamenává počet otáček.
Tento čítač je kalibrován tak, aby jeho hodnota odpovídala přímo spotřebované energii.
Poslední dobou se od tohoto způsobu řešení ustupuje, mimo jiné i proto, že tento elektroměr díky mechanickému odporu otočného mechanizmu není většinou schopen zaznamenávat hodnoty pod 5 \% maximálního příkonu.

\section{Moderní digitální metody měření výkonu a energie}
V~dnešní době se od klasických měřicích způsobů upouští a v~mnohem větší míře se využívá integrovaných obvodů. 
Dříve byla preferovaná metoda využívající dvou A/D převodníků, jejichž výstupy bylo nutné dále zpracovávat.
Tato metoda je postupně nahrazována integrovaným řešením, které není tolik nezatěžuje mikrokontroler, protože většina výpočtů je provedena uvnitř integrovaného obvodu.
Integrované obvody mají většinou dva vstupy, proudový a napěťový.

\subsection{Snímání proudu}
Většina obvodů obsahuje proudový vstup, který ve skutečnosti snímá napětí.
Proto musíme převést proud na napětí, což je nejjednodušší provést pomocí úbytku napětí na rezistoru.
Tento převod je lineární, tudíž jediné, co je nutné, je mít kvalitní rezistor $R$ pro převod.
Největší podíl na stabilitě má teplotní závislost rezistivity.
I~proto se snažíme, aby nedocházelo k~velkému úbytku výkonu na rezistoru, což by zapříčinilo nadměrné zahřívání a způsobilo to nelinearitu měření.

Pro snímání proudu v~obvodu máme více možností.
Základní možností je snímání pomocí bočníku (obrázek \ref{fig:bocnik}).
Zde musíme mít velmi přesný odpor malé hodnoty, aby na něm docházelo k~velmi malému úbytku napětí.
Tento úbytek napětí měříme.
Bočníkem můžeme měřit jak střídavé, tak i stejnosměrné proudy.

\begin{figure}[ht!]
  \centering
  \subfloat[Bočník]{\label{fig:bocnik}\includegraphics[height=5cm]{fig/bocnik}}
  \subfloat[Proudový transformátor \cite{currentT}]{\label{fig:ct}\includegraphics[height=5cm]{fig/ct}}
  \subfloat[Rogowského cívka včetně integračního prvku ]{\label{fig:rogowski}\includegraphics[height=5cm, width=6cm, keepaspectratio, clip, trim=0mm -10mm 0mm 0mm]{fig/rogowski}}
  \caption{Snímání proudu v~obvodu}
  \label{fig:snimani}
\end{figure}

%http://www.freepatentsonline.com/6954060.html
%http://www.magnelab.com/whitepapers.php?name=7-1-Flexible-Rogowski-Coils-A-Technical-Review

Dalším ze způsobů měření proudu je snímání pomocí proudového transformátoru (obrázek \ref{fig:ct}), jehož výhodou je galvanické oddělení od měřeného obvodu.
Proudový transformátor musí být zatížen odporem, na kterém měříme napětí.
Zde je velmi důležitá volba materiálu, protože pokud bychom použili materiál s~velkou plochu hysterezní smyčky, tak by docházelo ke ztrátám v~důsledku hystereze, což by způsobovalo chybu měření.
Tyto ztráty totiž nejsou lineární.
Jádro transformátoru může být jednolité nebo i rozebíratelné, jak známe například z~klešťových ampérmetrů.
Tento způsob měření můžeme použít pouze u~střídavých veličin, protože využíváme magnetické indukce.


Měření můžeme provádět také pomocí Rogowského cívky (obrázek \ref{fig:rogowski}).
Od proudového transformátoru se liší hlavně v~závislosti okamžitého výstupního napětí na okamžitém vstupním proudu
\begin{equation}\label{eq:rogowski}
u~= M \cdot { di \over dt }
\end{equation}
která obsahuje derivaci, díky které musí být na vstupu měřicího prvku použit integrační člen \cite{rogowski}.
Konstanta $M$ určuje magnetické vlastnosti cívky.
Výhodou tohoto řešení je flexibilita samotné cívky, díky čemu získáme větší možnosti v~mechanickém zapojení měřicího obvodu.


\subsection{Snímání napětí}
Měření napětí je oproti proudu mnohem jednodušší, neboť všechny dostupné obvody jsou na vstupu vybaveny A/D převodníkem, který snímá napětí.
Nicméně A/D převodník má omezený rozsah a při připojení přímo na síť by došlo k~poškození obvodu.
Proto musíme napětí lineárně rozdělit, což uděláme napěťovým děličem (obrázek \ref{fig:delic}).
\begin{figure}[ht!]
  \centering
  \includegraphics[height=3.5cm]{fig/delic}
  \caption{Dělič napětí}
  \label{fig:delic}
\end{figure}
Při volbě odporů však musíme počítat s~tím, že na dělič připojujeme poměrně vysoké napětí.
Proto je potřeba kontrolovat výkon spotřebovaný na jednotlivých rezistorech.
Abychom předešli nadměrnému zahřívání a tím i poškození rezistorů, volíme rezistory určené pro vyšší výkon -- například drátové nebo MELF.

%na zvládnutí pøepìtí => dane prislusnou normou. Zejmena z pohledu predpeti, ktere se muze na siti prechodne objevit napr. vlivem blesku apod. V teto situaci se na vstupech muze objevit az xkV.
Protože se jedná o~vstup, který je přímo připojen k~síti, je nutné mimo jiné splnit požadavky dané normou \cite{csnPodminky}.
Zejména z~pohledu přepětí, které se může na síti přechodně objevit např. vlivem blesku a podobně.
V~této situaci se může na vstupech objevit vysoké napětí, podle normy předpokládáme až 4~kV.
Výrobci integrovaných obvodů tedy uvádí pro tyto vstupy nejen měřicí rozsah, ale také i maximální napětí, které můžeme ke vstupu připojit bez poškození integrovaného obvodu.


\subsection{Výstup z~měření}
Obvody se často liší v~tom, jakým způsobem předávají výsledky pro další zpracování.
Starší obvody byly vybaveny pouze impulzním výstupem, kdy obvod má pouze jeden výstupní pin a pomocí impulzů signalizuje množství spotřebované energie. 
Každý impulz znamená konstantní přírůstek spotřebované energie.
Velikost tohoto přírůstku však musíme změřit předem, při kalibraci, jelikož na vstupech každého obvodu se vyskytují prvky s~mírně odlišnými parametry (převodový poměr proudového transformátoru, dělící poměr napěťového děliče a podobně).
Velmi často chceme znát nejen množství spotřebované energie, ale i aktuální příkon.
Tento příkon je definovaný podle vzorce
\begin{equation}
P = {{\Delta W}\over{ \Delta t}}
\label{eq:energietovykon}
\end{equation}
Při vyhodnocování příkonu můžeme jednoduše odstranit derivaci pomocí metody nazývané {\em hradlování}.
Princip této metody spočívá v~tom, že impulzy po určitou konstantní dobu budeme přičítat do registru.
Po uplynutí této doby vynásobíme registr kalibrační konstantou odpovídající dělení tímto časem, čímž získáme velikost okamžitého příkonu.
Tento způsob získávání dat z~měřicího obvodu se využívá i v~dnešní době, kdy nepotřebujeme měřit jiné hodnoty, než činnou elektrickou práci (nebo příkon).
Výhodou tohoto přenosu je to, že k~oddělení síťové měřicí části od nízkonapěťové, kde se nejčastěji vyskytuje procesor, stačí pouze jeden optočlen.

Pokročilejší měřicí obvody obsahují digitální rozhraní, nejčastěji SPI nebo \iTc.
Tato rozhraní si jsou velmi podobná, obě dvě jsou synchronní a fungují na principu dotaz--odpověď. 
Rozdíl je však v~tom, že \iTc sběrnice umožňuje polovičně duplexní přenosy a SPI plně duplexní přenosy. 
Z~toho plyne i to, že SPI vyžaduje tři datové vodiče (\textit{MOSI}, \textit{MISO} a \textit{CLK}) a \iTc pouze dva (\textit{DATA} a \textit{CLK}). 
Největší rozdíl je však ve způsobu adresace jednotlivých zařízení na sběrnici.

Někdy také můžeme u~měřicích obvodů najít komunikaci pomocí UART portu.
Toto už není sběrnice, takže pro každý prvek potřebujeme jeden výstup.
Komunikace je asynchronní, tudíž neexistuje žádný vodič s~časovacím signálem.
Obsahuje pouze dva vodiče -- RxD pro příjem a TxD pro odesílání dat.
Problémem je nutnost stejné přenosové rychlosti.


Někdy se využívá i toho, že měřicí obvod má přímo integrované jádro ARM nebo 8052.
Měřicí část je potom jednou z~periferií procesoru a čtení naměřených dat probíhá pomocí periferních operací.
Tyto procesory umožňují měřit velké množství veličin, podobně jak měřicí obvody s~digitálním výstupem.
Jsou uzpůsobeny pro nasazení v~elektronických elektroměrech a dalších podobných zařízeních a tomu odpovídají i periferie vyskytující se přímo v~procesoru (řadič LCD, UART, SPI \dots).

