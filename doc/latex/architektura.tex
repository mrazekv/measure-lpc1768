\chapter{Návrh architektury}\label{chap:architektura}
Na trhu lze nalézt dva způsoby řešení předávání výsledků měření.
Jedním z~možných způsobů implementace je jednoduchý měřicí systém, který bude posílat na externí server.
Toto řešení nabízela například společnost Google a její produkt PowerMeter.
Další způsob, který se často vyskytuje v~komerčních řešeních, je jednoduchý klient pro měření, který poskytuje data aplikaci na osobním počítači, která naměřené údaje dále zpracovává.
Komunikace s~počítačem je zajištěna buď pomocí průmyslové sběrnice, jakou je například RS485, CAN a podobně.
Toto řešení nabízí uživateli velké možnosti pohledů na zpracovaná data, ale problémem je to, že pro zpracování dat musí mít vždy zapnutý počítač.

Já jsem se rozhodl pro kombinované řešení.
Cílem bude vytvořit autonomní zařízení, které je možné připojit do internetové sítě, na níž bude poskytovat webový server, na kterém bude možné přečíst aktuální údaje o~spotřebě a také zobrazit jednoduchou analýzu -- například průměrnou spotřebu v~jednotlivých dnech v~týdnu nebo v~hodinách.
Pokud však uživatel bude potřebovat získat podrobnější statistiky, bude moci k~tomu využít výstup v~běžně rozšířeném formátu CSV nebo XML a ty následně zpracovat.

\section{Architektura}
Blokové schéma tohoto zapojení je vidět na obrázku \ref{fig:schema}.
Obvod je rozdělen na dvě základní části -- silovou a nízkonapěťovou.

V~silové části se nachází zdroj, dále jsou zde umístěny měřicí prvky jako je proudový transformátor pro snímání proudu spotřebičem a napěťový dělič pro snímání napětí.
Tyto dvě měřené veličiny jsou zapojené do měřicího obvodu ADE7753, který přímo komunikuje s~procesorem.
Komunikační sběrnice je galvanicky oddělena obvodem ADuM2402.

V~nízkonapěťové části je hlavním prvkem procesor LPC1768.
Procesor poskytuje sběrnice SPI a SSP pro komunikaci s~MicroSD kartou a s~měřicím obvodem.
Dále poskytuje konfiguračního rozhraní USB.
Poskytuje i rozhraní RMII\footnote{Reduced Media Independent Interface} pro komunikaci s~fyzickou vrstvou internetového rozhraní LAN8720a.

\begin{figure}[bht]
\begin{center}
  \includegraphics[width=15cm,keepaspectratio]{fig/schema2}
  \caption{Blokové schéma měřicí platformy}
  \label{fig:schema}
\end{center}
\end{figure}


Podrobné schéma celého systému je uvedeno v~obrazové příloze \ref{app:schema}.
Při návrhu nízkonapěťové části jsem vycházel z~referenční vývojové desky \cite{codered}.



\section{Napájení}
Protože procesor vyžaduje napájení 3,3 V~a analogová měřicí část vyžaduje napájení 5 V, je napájení rozděleno na dvě větve.
Pro transformaci síťového napětí jsem pro jednoduchost zvolil \textit{AC/DC konvertor} firmy Traco.
Tento obvod je dimenzovaný na výkon 4 W, což by mělo být podle dokumentace jednotlivých prvků dostatečné.

Nízkonapěťová část vyžaduje napětí 3,3 V, které jsou získané z~5 V~zdroje pomocí stabilizátoru.
Silová část však vyžaduje napájení 5 V, kde však musí být jeden z~vodičů při měření napětí spojen se zemí.
Vzhledem k~tomu, že v~zásuvce však nemusí být fáze v~levé zdířce, neboť je to pouze doporučení \cite{csnFaze}, nejsme schopni zaručit, že vodič spojený se zemí na desce bude střední vodič.
Mohla by se tam připojit i fáze, a proto musíme silovou část galvanicky oddělit od nízkonapěťové.

Obě části jsou spojeny ve třech místech.
Prvním místem spoje je napájecí zdroj, který transformuje síťové střídavé napětí 230 V~na 5 V~stejnosměrných.
Při této transformaci dochází k~oddělení síťového napětí od nízkonapěťové části.
Druhým místem spojení je napájení silové měřicí části obvodu, pro jejíž oddělení použijeme \textit{DC/DC konvertor}.
Tento prvek galvanicky odděluje napájení s~izolačním napětím 3 kV.
Velikost normou požadovaného izolačního napětí liší podle druhu uzemnění a napájení, které by však bylo nutné řešit až při nasazování do výroby.

Posledním místem je spojení sběrnice SPI při komunikaci převodníku s~procesorem.
Pro oddělení se velmi často používají optočleny, ale vzhledem k~jejich ceně bylo výhodnější použít integrovaný obvod ADuM 2402.
Tento obvod odděluje 4 kanály při vysoké přenosové rychlosti (rychlosti jsou závislé na výrobní řadě obvodu -- pohybuje se až do 90 Mbps) při izolačním napětí 5 kV.
Princip činnosti je vidět na obrázku \ref{fig:adum}.
Vstupní signál projde klopným obvodem s~hysterezí, čímž jsou redukovány zákmity.
Protože je oddělení galvanické, není možné přenášet stejnosměrný signál, a proto se musí logická hodnota na vstupním klopném obvodu převést na signál střídavý.
Po přenesení přes transformátor se přenášený signál opět transformuje na logické úrovně.
Tento obvod je využívaný nejen k~oddělení napájení, ale i k~převodu logických úrovní signálu.
Díky němu není nutné signál 3,3 V~zesilovat na 5 V~úroveň, kterou vyžaduje měřicí obvod ADE7753.

\begin{figure}[ht!]
  \centering
  \includegraphics[height=5cm]{fig/adum}
  \caption{Obvod ADuM 2402 \cite{adumDataSheet}}
  \label{fig:adum}
\end{figure}

Obvod je chráněn také proti zvýšenému napětí v~důsledku přepětí v~síti nebo úderu blesku a podobně.
Tyto ochrany jsou celkem tři.
Prvním typem ochrany je klasická pojistka {\it 1.25~A SLOW BLOW}, která je přímo doporučena ke zdroji Traco.
Tato pojistka chrání i napěťový vstup měřicího obvodu ADE7753, protože na ní na rozdíl od polovodičových ochranných prvků nedochází ke zkreslení napětí.
\begin{figure}[ht!]
  \centering
  \subfloat[Zablokování podle proudu u~PTC termistoru \cite{ptc}]{\label{fig:ptc}\includegraphics[height=7cm]{fig/ptc}} %http://www.bourns.com/data/global/pdfs/mfr.pdf
  \subfloat[Voltampérová charakteristika trisilu \cite{trisil}]{\label{fig:trisil}\includegraphics[height=7cm]{fig/trisil}} %cite http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/CD00002051.pdf
  \caption{Charakteristiky ochranných prvků}
\end{figure}
Dalším typem ochrany je resetovatelná pojistka ve formě PTC termistoru.
Tento termistor se při nárůstu proudu ohřeje a díky pozitivní vazbě rezistivity na teplotu zvýší svůj odpor, proto je termistor připojen sériově ke vstupu napájecího zdroje.
Na obrázku \ref{fig:ptc} je znázorněna vazba vstupního proudu a času zvýšení rezistivity na takovou úroveň, že připojeným obvodem nebude procházet žádný proud.
Je vidět, že při vyšším proudu je reakce rychlejší při vyšším proudu.
Ovšem tato reakce je poměrně pomalá a při pulzním přepětí, které vzniká například při rušení napájecích vodičů jiným signálem, by mohlo dojít k~poškození obvodu.
Proto zařazujeme paralelně k~napájecímu zdroji další ochranu, trisil.
Tento prvek pracuje tak, jak je vidět na voltampérové charakteristice na obrázku \ref{fig:trisil}.
Při zvýšení napětí na součástce nad hodnotu $V_{BO}$ dojde k~nedestruktivnímu průrazu polovodiče a tím ke snížení napětí na minimum.
Díky tomu dojde na vstupu zdroje také ke snížení napětí a nedojde k~poškození -- veškerý proud potom prochází trisilem.
Procházející proud je však následně snížen PTC termistorem.
Trisil je na rozdíl od PTC termistoru velmi rychlý, takže se používá pro ošetření pulzů na vstupech, což je součástí zákonného testování přístroje před uvedením na trh.


\section{Měření} \label{sec:mereni}
Pro měření jsem se rozhodl použít obvod ADE7753 představený v~kapitole \ref{sec:ade7753}.
Mimo již zmíněných vlastností je výhodou dostupnost obvodu díky existenci přímého zastoupení firmy Analog Devices v~Brně firmou Amtek.

Pro snadnější testování bude zařízení dimenzováno na výkon do 1000 W a vybaveno standardní zásuvkou.
Zařízení by mohlo zpracovávat i vyšší výkon, ale díky použití nižšího rozsahu bude možné provádět zkoušky na spotřebičích s~menším příkonem.

Pro tento obvod je lepší měřit spíše v~prostřední části měřicího rozsahu, při maximu může docházet k~chybám.
Proto jsou hodnoty rezistorů pro měření vždy menší, než je vypočítané.

\subsection{Proudové měření}
Prvním ze vstupů je proudový vstup.
Z~jmenovitého výkonu 1000 W vychází, že obvodem bude protékat maximální (amplitudový) proud
\begin{equation}\label{eq:proudamp}
I_{AMP}={{P  \over U} \cdot \sqrt{2}}=  {1000 \over 230} \cdot \sqrt{2}=6,15{\rm~A}.
\end{equation}
Od firmy Rawet jsem měl k~dispozici proudové transformátory.
Transformátor je uložený v~kruhovém plastovém pouzdru o~průměru 23 mm, které izoluje i průvlek transformátorem a zbývající díra má vnitřní průměr 4 mm.
Použijeme-li silový vodič s~izolací 1 mm z~každé strany, tak proudová hustota vychází
\begin{equation}\label{eq:proudhust}
J={I \over S}={I \over  {1 \over 4} \pi \cdot d^2}={6,15 \over {1 \over 4} \pi 2^2}=1,96 {\rm~A} \cdot {\rm mm}^{-2} ,
\end{equation}
což je v~pořádku a nebude docházet k~přehřívání vodiče.
Použitý transformátor má transformační poměr $p$ 10000:1.
To znamená, že na výstupní proud bude při maximálním příkonu dosahovat ve špičce hodnoty
\begin{equation}\label{eq:proud}
I_O={I_{AMP} \over p}={6,15 \over 10000}=6,15 {\rm~mA} .
\end{equation}
Pro transformaci proudu na napětí musíme na svorky transformátoru připojit rezistor.
Pokud použijeme rezistor $R=10 {\rm~\Omega}$, bude úbytek napětí na tomto rezistoru
\begin{equation}
U_R={I_O \cdot R}=6,15 \cdot 10^{-3} \cdot 10 = 61,5 {\rm~mV}.
\end{equation}
Abychom se při měření pohybovali v~rozsahu vstupního kanálu, nastavíme zesílení \textit{PGAIN1} na 8.
Maximální úroveň měřeného signálu potom bude $492 {\rm~mV}$.

\subsection{Napěťové měření}
Pro měření napětí použijeme přímo napájení systému.
Napěťová část bude chráněna pouze pojistkou, protože další ochrany by vnesly poměrně velkou nelinearitu měření.
Vstup musí být dimenzován tak, aby ani při přepětí 4 kV nedošlo k~trvalému poškození převodníku ADE7753.
%Převodník je dimenzovaný na maximální napětí $V_{MAX}=\pm 3 {\rm~V}$.

Protože na měřicí vstup nemůžeme připojit přímo síťové napětí, musíme je rozdělit napěťovým děličem (obrázek \ref{fig:delic}), jehož dělicí poměr je dán poměrem rezistorů $R_1$ a $R_2$.
Víme, že jmenovité vstupní napětí je amplitudou síťového
\begin{equation}\label{eq:napamp}
U_{AMP}=U_{MAX} \cdot \sqrt{2}=230 \cdot \sqrt{2}=325,27 {\rm~V}.
\end{equation}
Toto napětí musíme snížit tak, aby se pohybovalo v~rozsahu $\pm$ 0,5~V.
Dělicí poměr tedy bude minimálně $p=650 : 1$.
Protože maximální možné napětí na vstupu bez poškození obvodu je $\pm$ 3 V~a na vstupu se může objevit přepětí 4~kV, měl by být poměr $p=4000 : 3$.
My proto zvolíme poměr rezistorů $p=1000 : 1$.

Odpor $R_2$, na kterém se provádí měření, se většinou volí zhruba $1 {\rm~k\Omega}$ \cite{adeDataSheet}.
Proto nám vychází rezistor $R_1$ o~jmenovité hodnotě  $1 {\rm~M\Omega}$.
Jelikož na rezistoru $R_1$ dochází k~velkému úbytku napětí, je nutné spočítat jeho výkonové zatížení.
Toto zatížení musíme spočítat pro špičkové napětí $U_{PEEK}=4 {\rm~kV}$
%Určíme si špičkové napětí a provozní napětí $U_{NORM}=U_{AMP} \cdot {998 \over 1000} = 325 {\rm~V}$.
%Z těchto napětí jsme schopni určit špičkový výkon (\ref{eq:uPpeek}) a provozní výkon (\ref{eq:uPnorm}).
\begin{equation}\label{eq:uPpeek}
P_{PEEK}= {U^2 \over R}={(4 \cdot 10^3)^2 \over 1 \cdot 10^6}=16 {\rm~W},
\end{equation}
i pro provozní napětí $U_{AMP}$
\begin{equation}\label{eq:uPnorm}
P_{NORM}= {U^2 \over R}={325^2 \over 1 \cdot 10^6}=105 {\rm~mW}.
\end{equation}
Podle velikosti výkonů $P_{PEEK}$ a $P_{NORM}$ je zřejmé, že budeme muset výkon rozdělit mezi více rezistorů.
Proto zvolíme tři výkonové rezistory o~velikosti $R_2=330 {\rm~k\Omega}$ v~klasickém drátovém provedení, čímž nebude docházet k~přehřívání rezistorů a splní se i požadavek na odolnost proti přepětí.

Maximální provozní napětí na měřicím vstupu bude
\begin{equation}\label{eq:uin2}
U_{IN2}=U_{AMP} \cdot {R_2 \over {3 \cdot R_2 + R_1}}=325 \cdot {1 \cdot 10^3 \over {3 \cdot (330 \cdot 10^3) + 1 \cdot 10^3}}=33 {\rm~mV}.
\end{equation}
Proto nastavíme zesílení druhého, napěťového, vstupu \textit{PGAIN2} na hodnotu 8.
Jelikož je omezení na 3 V~při přepětí vztaženo na vstup měřicího obvodu, neprochází potom tento signál programovatelným zesilovačem a není nutné s~tímto zesílením počítat.
Při přepětí 4~kV dojde na vstupu k~nárůstu napětí na
\begin{equation}\label{eq:uin2max}
U_{IN2}=U_{MAX} \cdot {R_2 \over {3 \cdot R_2 + R_1}}=4 000 \cdot {2 \cdot 10^3 \over {3 \cdot (330 \cdot 10^3) + 2 \cdot 10^3}}=4 {\rm~V},
\end{equation}
což sice neodpovídá maximálnímu trvalému napětí na vstupu měřicího obvodu, ale díky ochraně trisilem by nedošlo k~poškození.
Pokud bychom zvolili rezistory $R_1$ a $R_2$ tak, aby při přepětí 4 kV nedošlo k~žádnému překročení maximálního napětí, byla by při jmenovitém napětí na vstupu tak malá vstupní úroveň napětí, že by docházelo ke zkreslení výsledků měření.

\section{Procesor}
V~dnešní době mnoho výrobců vyvíjí procesory postavené na architektuře ARM.
Tato architektura se však dělí do více řad podle výkonosti.
Při výběru vhodného řídicího prvku celého systému hrála hlavní roli dostupnost programového vybavení, rychlost, spotřeba a v~neposlední řadě podpora rozhraní Ethernet.
Vzhledem k~tomu, že na procesoru je nutné kromě měření provádět i analýzu výsledků, musí být použit výkonnější obvod.
Rozhraní Ethernet se vyskytuje až v~řadách Cortex M4 a vzácněji u~řady Cortex M3.
Vyšší řady, jako je ARM 7 nebo ARM 9, jsou pro tuto aplikaci zbytečně složité.

Procesorů v~těchto dvou řadách Cortex je mnoho.
Ovšem velmi často se naráží na problematickou dostupnost, kdy někteří výrobci nabízí celou řadu procesorů, ale odběr je možný jen při velkém množství.
Vzhledem k~dostupnosti jsem vybíral z~procesorů firem STM a NXP.
Procesory od STM však podporovaly internetové rozhraní až od řady Cortex M4, která je dražší.
Proto jsem vybral procesor NXP LPC 1768 \cite{lpc1768}, který podporuje základních komunikační sběrnice, jako je SPI, \iTc nebo SSP.
Dále nabízí řadu UART portů a jeden USB port.
Obsahuje paměť 512 kB ROM a 64 kB RAM.
U~paměti RAM je však nutné pamatovat na to, že 16 kB je vyhrazeno pro Ethernetovou vrstvu a 16 kB pro USB řadič.
Další důležitou součástí pro naši aplikaci je obvod pro reálný čas.

Procesor se nachází v~pouzdře určeném pro SMT LQPF 100.
Při návrhu desky bylo nutné zohlednit i fakt, že ne všechny periferie mohou být současně aktivní.
Taktování procesoru je řešeno 20 MHz krystalem, kde vnitřní takt je upraven na frekvenci 100 MHz.



\section{Ethernet}
Použitý procesor obsahuje řadič Ethernetového rozhraní.
Neimplementuje však jeho nejnižší, fyzickou, vrstvu, kterou je tím pádem nutné řešit externě skrze RMII rozhraní.
Zde jsem použil integrovaný obvod LAN8720A, který je umístěn v~24 vývodovém QFN čtvercovém pouzdru určeném pro SMT.
Problematickou částí tohoto pouzdra je vyvedení země.
Ta je vyvedena pouze na spodní chladící plošce a není na žádném jiném výstupním pinu.
Piny jsou však tak blízko, že mezi nimi nebylo možné provést další vodivý spoj.
Proto jsem se rozhodl pro vrtaný spoj pod touto součástkou, který má výhodu lepšího odvodu přebytečného tepla.
Obecně se toto řešení moc nedoporučuje z~důvodu možnosti chybného osazení při strojovém osazování.
Proto by při průmyslové výrobě takovéto desky bylo vhodné tento prokov konzultovat s~technologem.

Jako vstupní konektor je použitý standardní konektor určený pro internetové sítě RJ--45.
Pro připojení je použito transformátorů a sady odporů 49,9 $\Omega$ podle dokumentace obvodu \cite{lan8720}.
Transformátory jsou přímo integrované v~konektoru, takže není nutné je zapojovat externě.
Je však nutné zohlednit to, že toto zapojení mírně zatěžuje napájecí zdroj a je nutné s~touto zátěží počítat při návrhu napájecího zdroje.

\section{SD karta}
Jako paměťové úložiště měřených dat i nastavení byla použita MicroSD karta.
Zvolil jsem toto řešení z~důvodu nižší ceny než FLASH paměti ve formě čipu.

MicroSD karta, stejně jako SD karta, může pracovat ve dvou režimech --- SD módu a SPI módu \cite{sdcard}.
SD mód je proprietární a vyžaduje více vodičů.
Má však výhodu vyšší přenosové rychlosti.
My však velkou přenosovou rychlost nevyžadujeme a použijeme proto SPI mód.

Vývody karty odpovídají obrázku \ref{fig:microsd}.
Pro kartu použijeme patici, kterou musíme umístit na kraji desky plošného spoje z~důvodu snadného vyjímání karty.
Je nutné pamatovat, že tato patice obsahuje několik míst, které nesmí přijít do kontaktu s~vodivou vrstvou.

\begin{figure}[ht!]
  \centering
  \includegraphics[height=4cm]{fig/microsd}
  \caption{Vývody MicroSD karty v~módu SPI \cite{sdcard}}
  \label{fig:microsd}
\end{figure}

\section{Rozmístění součástek}
Při návrhu rozložení byla snaha co nejvíce minimalizovat rozměry desky.
Do návrhu však byly zaneseny požadavky na fyzické oddělení síťové a nízkonapěťové části obvodu.
Velikost desky byla omezena počtem paralelních spojů a prokovů, kdy nebylo možné více zmenšovat velikost při zachování stanovených rozestupů.

Pro menší rušení obvodů byl ke každému napájecímu vstupu přiřazen kondenzátor 100~nF / X7R.
Tento vysokofrekvenční kondenzátor filtruje nežádané zákmity na lince.
Je však nutné to, aby tyto kondenzátory byly fyzicky umístěné co nejblíž
Dále proti rušení byla rozlita zem po celé ploše desky plošného spoje.
To vytváří stínění proti nepříznivým elektromagnetickým vlivům.
Rozmístění součástek a maska měděné vrstvy jsou uvedeny v~obrazové příloze \ref{app:schema}.




\section{Technologie výroby}
Deska plošného spoje byla vyráběna ve firmě specializující se na malovýrobu DPS.
Byla provedena ve dvouvrstvé technologii s~nanesenou maskou.

Pro letování SMD součástek byly použité dvě metody.
Zadní strana byla pájena za pomocí pasty na školním ručním osazovacím stroji a následně zapájena v~peci.
Horní strana byla pájena ručně při použití olovnaté technologie pájení.
Tato technologie se sice téměř nepoužívá, ale pro měřicí přístroje je stále povolena a doporučována z~důvodu menší oxidace.

Při oživování byla oživena nejdříve slaboproudá část díky možnosti jejího kompletního odpojení pomocí propojky.
Na tuto propojku bylo přivedeno napájení z~proudově omezeného zdroje a díky tomu byl obvod chráněný proti zkratu.
Po otestování příkonu této části bylo přivedeno síťové napájecí napětí na hlavní svorky přístroje.

Pro vyšší bezpečnost při práci s~prototypem přístroje byla DPS připevněna na polyuretanovou desku a byl vytvořen vývojový prvek, který můžete vidět v~obrazové příloze \ref{app:schema}.
Na desku byla připevněna standardní jednofázová zásuvka pro připojení měřeného spotřebiče.