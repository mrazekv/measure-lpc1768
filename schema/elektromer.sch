<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="SILOVA" color="3" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="lpc1xxx-v6">
<packages>
<package name="LQFP100-14X14">
<description>&lt;b&gt;LQFP100 &lt;/b&gt;&lt;p&gt;Low profile Quad Flat Package 14x14x1.4 mm&lt;p&gt;</description>
<wire x1="-6.873" y1="6.873" x2="6.873" y2="6.873" width="0.1524" layer="21"/>
<wire x1="6.873" y1="6.873" x2="6.873" y2="-6.873" width="0.1524" layer="21"/>
<wire x1="6.873" y1="-6.873" x2="-6.123" y2="-6.873" width="0.1524" layer="21"/>
<wire x1="-6.123" y1="-6.873" x2="-6.873" y2="-6.123" width="0.1524" layer="21"/>
<wire x1="-6.873" y1="-6.123" x2="-6.873" y2="6.873" width="0.1524" layer="21"/>
<wire x1="-9" y1="9" x2="9" y2="9" width="0.4064" layer="39"/>
<wire x1="9" y1="9" x2="9" y2="-9" width="0.4064" layer="39"/>
<wire x1="9" y1="-9" x2="-9" y2="-9" width="0.4064" layer="39"/>
<wire x1="-9" y1="-9" x2="-9" y2="9" width="0.4064" layer="39"/>
<circle x="-4.5" y="-4.5" radius="1" width="0.1524" layer="21"/>
<smd name="75" x="-6" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="74" x="-5.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="73" x="-5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="72" x="-4.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="71" x="-4" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="70" x="-3.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="69" x="-3" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="68" x="-2.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="67" x="-2" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="66" x="-1.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="65" x="-1" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="64" x="-0.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="63" x="0" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="62" x="0.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="61" x="1" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="60" x="1.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="59" x="2" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="58" x="2.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="57" x="3" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="56" x="3.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="55" x="4" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="54" x="4.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="53" x="5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="52" x="5.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="51" x="6" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="1" x="-6" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="2" x="-5.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="3" x="-5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="4" x="-4.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="5" x="-4" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="6" x="-3.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="7" x="-3" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="8" x="-2.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="9" x="-2" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="10" x="-1.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="11" x="-1" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="12" x="-0.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="13" x="0" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="14" x="0.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="15" x="1" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="16" x="1.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="17" x="2" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="18" x="2.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="19" x="3" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="20" x="3.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="21" x="4" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="22" x="4.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="23" x="5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="24" x="5.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="25" x="6" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="26" x="7.75" y="-6" dx="1.5" dy="0.27" layer="1"/>
<smd name="27" x="7.75" y="-5.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="28" x="7.75" y="-5" dx="1.5" dy="0.27" layer="1"/>
<smd name="29" x="7.75" y="-4.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="30" x="7.75" y="-4" dx="1.5" dy="0.27" layer="1"/>
<smd name="31" x="7.75" y="-3.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="32" x="7.75" y="-3" dx="1.5" dy="0.27" layer="1"/>
<smd name="33" x="7.75" y="-2.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="34" x="7.75" y="-2" dx="1.5" dy="0.27" layer="1"/>
<smd name="35" x="7.75" y="-1.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="36" x="7.75" y="-1" dx="1.5" dy="0.27" layer="1"/>
<smd name="37" x="7.75" y="-0.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="38" x="7.75" y="0" dx="1.5" dy="0.27" layer="1"/>
<smd name="39" x="7.75" y="0.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="40" x="7.75" y="1" dx="1.5" dy="0.27" layer="1"/>
<smd name="41" x="7.75" y="1.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="42" x="7.75" y="2" dx="1.5" dy="0.27" layer="1"/>
<smd name="43" x="7.75" y="2.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="44" x="7.75" y="3" dx="1.5" dy="0.27" layer="1"/>
<smd name="45" x="7.75" y="3.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="46" x="7.75" y="4" dx="1.5" dy="0.27" layer="1"/>
<smd name="47" x="7.75" y="4.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="48" x="7.75" y="5" dx="1.5" dy="0.27" layer="1"/>
<smd name="49" x="7.75" y="5.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="50" x="7.75" y="6" dx="1.5" dy="0.27" layer="1"/>
<smd name="76" x="-7.75" y="6" dx="1.5" dy="0.27" layer="1"/>
<smd name="77" x="-7.75" y="5.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="78" x="-7.75" y="5" dx="1.5" dy="0.27" layer="1"/>
<smd name="79" x="-7.75" y="4.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="80" x="-7.75" y="4" dx="1.5" dy="0.27" layer="1"/>
<smd name="81" x="-7.75" y="3.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="82" x="-7.75" y="3" dx="1.5" dy="0.27" layer="1"/>
<smd name="83" x="-7.75" y="2.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="84" x="-7.75" y="2" dx="1.5" dy="0.27" layer="1"/>
<smd name="85" x="-7.75" y="1.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="86" x="-7.75" y="1" dx="1.5" dy="0.27" layer="1"/>
<smd name="87" x="-7.75" y="0.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="88" x="-7.75" y="0" dx="1.5" dy="0.27" layer="1"/>
<smd name="89" x="-7.75" y="-0.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="90" x="-7.75" y="-1" dx="1.5" dy="0.27" layer="1"/>
<smd name="91" x="-7.75" y="-1.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="92" x="-7.75" y="-2" dx="1.5" dy="0.27" layer="1"/>
<smd name="93" x="-7.75" y="-2.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="94" x="-7.75" y="-3" dx="1.5" dy="0.27" layer="1"/>
<smd name="95" x="-7.75" y="-3.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="96" x="-7.75" y="-4" dx="1.5" dy="0.27" layer="1"/>
<smd name="97" x="-7.75" y="-4.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="98" x="-7.75" y="-5" dx="1.5" dy="0.27" layer="1"/>
<smd name="99" x="-7.75" y="-5.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="100" x="-7.75" y="-6" dx="1.5" dy="0.27" layer="1"/>
<text x="-10.033" y="-7.747" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="11.43" y="-7.62" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.135" y1="6.868" x2="-5.865" y2="7.873" layer="51"/>
<rectangle x1="-5.635" y1="6.868" x2="-5.365" y2="7.873" layer="51"/>
<rectangle x1="-5.135" y1="6.868" x2="-4.865" y2="7.873" layer="51"/>
<rectangle x1="-4.635" y1="6.868" x2="-4.365" y2="7.873" layer="51"/>
<rectangle x1="-4.135" y1="6.868" x2="-3.865" y2="7.873" layer="51"/>
<rectangle x1="-3.635" y1="6.868" x2="-3.365" y2="7.873" layer="51"/>
<rectangle x1="-3.135" y1="6.868" x2="-2.865" y2="7.873" layer="51"/>
<rectangle x1="-2.635" y1="6.868" x2="-2.365" y2="7.873" layer="51"/>
<rectangle x1="-2.135" y1="6.868" x2="-1.865" y2="7.873" layer="51"/>
<rectangle x1="-1.635" y1="6.868" x2="-1.365" y2="7.873" layer="51"/>
<rectangle x1="-1.135" y1="6.868" x2="-0.865" y2="7.873" layer="51"/>
<rectangle x1="-0.635" y1="6.868" x2="-0.365" y2="7.873" layer="51"/>
<rectangle x1="-0.135" y1="6.868" x2="0.135" y2="7.873" layer="51"/>
<rectangle x1="0.365" y1="6.868" x2="0.635" y2="7.873" layer="51"/>
<rectangle x1="0.865" y1="6.868" x2="1.135" y2="7.873" layer="51"/>
<rectangle x1="1.365" y1="6.868" x2="1.635" y2="7.873" layer="51"/>
<rectangle x1="1.865" y1="6.868" x2="2.135" y2="7.873" layer="51"/>
<rectangle x1="2.365" y1="6.868" x2="2.635" y2="7.873" layer="51"/>
<rectangle x1="2.865" y1="6.868" x2="3.135" y2="7.873" layer="51"/>
<rectangle x1="3.365" y1="6.868" x2="3.635" y2="7.873" layer="51"/>
<rectangle x1="3.865" y1="6.868" x2="4.135" y2="7.873" layer="51"/>
<rectangle x1="4.365" y1="6.868" x2="4.635" y2="7.873" layer="51"/>
<rectangle x1="4.865" y1="6.868" x2="5.135" y2="7.873" layer="51"/>
<rectangle x1="5.365" y1="6.868" x2="5.635" y2="7.873" layer="51"/>
<rectangle x1="5.865" y1="6.868" x2="6.135" y2="7.873" layer="51"/>
<rectangle x1="6.873" y1="5.865" x2="7.878" y2="6.135" layer="51"/>
<rectangle x1="6.873" y1="5.365" x2="7.878" y2="5.635" layer="51"/>
<rectangle x1="6.873" y1="4.865" x2="7.878" y2="5.135" layer="51"/>
<rectangle x1="6.873" y1="4.365" x2="7.878" y2="4.635" layer="51"/>
<rectangle x1="6.873" y1="3.865" x2="7.878" y2="4.135" layer="51"/>
<rectangle x1="6.873" y1="3.365" x2="7.878" y2="3.635" layer="51"/>
<rectangle x1="6.873" y1="2.865" x2="7.878" y2="3.135" layer="51"/>
<rectangle x1="6.873" y1="2.365" x2="7.878" y2="2.635" layer="51"/>
<rectangle x1="6.873" y1="1.865" x2="7.878" y2="2.135" layer="51"/>
<rectangle x1="6.873" y1="1.365" x2="7.878" y2="1.635" layer="51"/>
<rectangle x1="6.873" y1="0.865" x2="7.878" y2="1.135" layer="51"/>
<rectangle x1="6.873" y1="0.365" x2="7.878" y2="0.635" layer="51"/>
<rectangle x1="6.873" y1="-0.135" x2="7.878" y2="0.135" layer="51"/>
<rectangle x1="6.873" y1="-0.635" x2="7.878" y2="-0.365" layer="51"/>
<rectangle x1="6.873" y1="-1.135" x2="7.878" y2="-0.865" layer="51"/>
<rectangle x1="6.873" y1="-1.635" x2="7.878" y2="-1.365" layer="51"/>
<rectangle x1="6.873" y1="-2.135" x2="7.878" y2="-1.865" layer="51"/>
<rectangle x1="6.873" y1="-2.635" x2="7.878" y2="-2.365" layer="51"/>
<rectangle x1="6.873" y1="-3.135" x2="7.878" y2="-2.865" layer="51"/>
<rectangle x1="6.873" y1="-3.635" x2="7.878" y2="-3.365" layer="51"/>
<rectangle x1="6.873" y1="-4.135" x2="7.878" y2="-3.865" layer="51"/>
<rectangle x1="6.873" y1="-4.635" x2="7.878" y2="-4.365" layer="51"/>
<rectangle x1="6.873" y1="-5.135" x2="7.878" y2="-4.865" layer="51"/>
<rectangle x1="6.873" y1="-5.635" x2="7.878" y2="-5.365" layer="51"/>
<rectangle x1="6.873" y1="-6.135" x2="7.878" y2="-5.865" layer="51"/>
<rectangle x1="5.865" y1="-7.873" x2="6.135" y2="-6.868" layer="51"/>
<rectangle x1="5.365" y1="-7.873" x2="5.635" y2="-6.868" layer="51"/>
<rectangle x1="4.865" y1="-7.873" x2="5.135" y2="-6.868" layer="51"/>
<rectangle x1="4.365" y1="-7.873" x2="4.635" y2="-6.868" layer="51"/>
<rectangle x1="3.865" y1="-7.873" x2="4.135" y2="-6.868" layer="51"/>
<rectangle x1="3.365" y1="-7.873" x2="3.635" y2="-6.868" layer="51"/>
<rectangle x1="2.865" y1="-7.873" x2="3.135" y2="-6.868" layer="51"/>
<rectangle x1="2.365" y1="-7.873" x2="2.635" y2="-6.868" layer="51"/>
<rectangle x1="1.865" y1="-7.873" x2="2.135" y2="-6.868" layer="51"/>
<rectangle x1="1.365" y1="-7.873" x2="1.635" y2="-6.868" layer="51"/>
<rectangle x1="0.865" y1="-7.873" x2="1.135" y2="-6.868" layer="51"/>
<rectangle x1="0.365" y1="-7.873" x2="0.635" y2="-6.868" layer="51"/>
<rectangle x1="-0.135" y1="-7.873" x2="0.135" y2="-6.868" layer="51"/>
<rectangle x1="-0.635" y1="-7.873" x2="-0.365" y2="-6.868" layer="51"/>
<rectangle x1="-1.135" y1="-7.873" x2="-0.865" y2="-6.868" layer="51"/>
<rectangle x1="-1.635" y1="-7.873" x2="-1.365" y2="-6.868" layer="51"/>
<rectangle x1="-2.135" y1="-7.873" x2="-1.865" y2="-6.868" layer="51"/>
<rectangle x1="-2.635" y1="-7.873" x2="-2.365" y2="-6.868" layer="51"/>
<rectangle x1="-3.135" y1="-7.873" x2="-2.865" y2="-6.868" layer="51"/>
<rectangle x1="-3.635" y1="-7.873" x2="-3.365" y2="-6.868" layer="51"/>
<rectangle x1="-4.135" y1="-7.873" x2="-3.865" y2="-6.868" layer="51"/>
<rectangle x1="-4.635" y1="-7.873" x2="-4.365" y2="-6.868" layer="51"/>
<rectangle x1="-5.135" y1="-7.873" x2="-4.865" y2="-6.868" layer="51"/>
<rectangle x1="-5.635" y1="-7.873" x2="-5.365" y2="-6.868" layer="51"/>
<rectangle x1="-6.135" y1="-7.873" x2="-5.865" y2="-6.868" layer="51"/>
<rectangle x1="-7.878" y1="-6.135" x2="-6.873" y2="-5.865" layer="51"/>
<rectangle x1="-7.878" y1="-5.635" x2="-6.873" y2="-5.365" layer="51"/>
<rectangle x1="-7.878" y1="-5.135" x2="-6.873" y2="-4.865" layer="51"/>
<rectangle x1="-7.878" y1="-4.635" x2="-6.873" y2="-4.365" layer="51"/>
<rectangle x1="-7.878" y1="-4.135" x2="-6.873" y2="-3.865" layer="51"/>
<rectangle x1="-7.878" y1="-3.635" x2="-6.873" y2="-3.365" layer="51"/>
<rectangle x1="-7.878" y1="-3.135" x2="-6.873" y2="-2.865" layer="51"/>
<rectangle x1="-7.878" y1="-2.635" x2="-6.873" y2="-2.365" layer="51"/>
<rectangle x1="-7.878" y1="-2.135" x2="-6.873" y2="-1.865" layer="51"/>
<rectangle x1="-7.878" y1="-1.635" x2="-6.873" y2="-1.365" layer="51"/>
<rectangle x1="-7.878" y1="-1.135" x2="-6.873" y2="-0.865" layer="51"/>
<rectangle x1="-7.878" y1="-0.635" x2="-6.873" y2="-0.365" layer="51"/>
<rectangle x1="-7.878" y1="-0.135" x2="-6.873" y2="0.135" layer="51"/>
<rectangle x1="-7.878" y1="0.365" x2="-6.873" y2="0.635" layer="51"/>
<rectangle x1="-7.878" y1="0.865" x2="-6.873" y2="1.135" layer="51"/>
<rectangle x1="-7.878" y1="1.365" x2="-6.873" y2="1.635" layer="51"/>
<rectangle x1="-7.878" y1="1.865" x2="-6.873" y2="2.135" layer="51"/>
<rectangle x1="-7.878" y1="2.365" x2="-6.873" y2="2.635" layer="51"/>
<rectangle x1="-7.878" y1="2.865" x2="-6.873" y2="3.135" layer="51"/>
<rectangle x1="-7.878" y1="3.365" x2="-6.873" y2="3.635" layer="51"/>
<rectangle x1="-7.878" y1="3.865" x2="-6.873" y2="4.135" layer="51"/>
<rectangle x1="-7.878" y1="4.365" x2="-6.873" y2="4.635" layer="51"/>
<rectangle x1="-7.878" y1="4.865" x2="-6.873" y2="5.135" layer="51"/>
<rectangle x1="-7.878" y1="5.365" x2="-6.873" y2="5.635" layer="51"/>
<rectangle x1="-7.878" y1="5.865" x2="-6.873" y2="6.135" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LPC176X">
<wire x1="-60.96" y1="45.72" x2="22.86" y2="45.72" width="0.254" layer="94"/>
<wire x1="22.86" y1="-109.22" x2="-60.96" y2="-109.22" width="0.254" layer="94"/>
<wire x1="-60.96" y1="-109.22" x2="-60.96" y2="45.72" width="0.254" layer="94"/>
<wire x1="22.86" y1="45.72" x2="22.86" y2="-109.22" width="0.254" layer="94"/>
<text x="-58.42" y="46.99" size="1.778" layer="96">&gt;VALUE</text>
<text x="-23.114" y="46.99" size="1.778" layer="96">&gt;NAME</text>
<pin name="TDO/SWO" x="-63.5" y="-93.98" length="short"/>
<pin name="TDI" x="-63.5" y="-96.52" length="short"/>
<pin name="TMS/SWDIO" x="-63.5" y="-99.06" length="short"/>
<pin name="TRST" x="-63.5" y="-101.6" length="short"/>
<pin name="TCK/SWDCLK" x="-63.5" y="-104.14" length="short"/>
<pin name="P0[26]/AD0[3]/AOUT/RXD3" x="-63.5" y="-15.24" length="short"/>
<pin name="P0[25]/AD0[2]/I2SRX_SDA/TXD3" x="-63.5" y="-12.7" length="short"/>
<pin name="P0[24]/AD0[1]/I2SRX_WS/CAP3[1]" x="-63.5" y="-10.16" length="short"/>
<pin name="P0[23]/AD0[0]/I2SRX_CLK/CAP3[0]" x="-63.5" y="-7.62" length="short"/>
<pin name="VDDA" x="-63.5" y="-60.96" length="short"/>
<pin name="VSSA" x="-63.5" y="-63.5" length="short"/>
<pin name="VREFP" x="-63.5" y="-68.58" length="short"/>
<pin name="NC" x="25.4" y="-106.68" length="short" rot="R180"/>
<pin name="!RSTOUT" x="-63.5" y="-33.02" length="short" function="dot"/>
<pin name="VREFN" x="-63.5" y="-71.12" length="short"/>
<pin name="RTCX1" x="25.4" y="-73.66" length="short" rot="R180"/>
<pin name="!RESET" x="-63.5" y="-30.48" length="short" function="dot"/>
<pin name="RTCX2" x="25.4" y="-76.2" length="short" rot="R180"/>
<pin name="VBAT" x="-63.5" y="-55.88" length="short"/>
<pin name="P1[31]/SCK1/AD0[5]" x="25.4" y="-15.24" length="short" rot="R180"/>
<pin name="P1[30]/VBUS/AD0[4]" x="25.4" y="-12.7" length="short" rot="R180"/>
<pin name="XTAL1" x="25.4" y="-81.28" length="short" rot="R180"/>
<pin name="XTAL2" x="25.4" y="-83.82" length="short" rot="R180"/>
<pin name="P0[28]/SCL0/USB_SCL" x="-63.5" y="-20.32" length="short"/>
<pin name="P0[27]/SDA0/USB_SDA" x="-63.5" y="-17.78" length="short"/>
<pin name="P3[26]/STCLK/MAT0[1]/PWM1[3]" x="25.4" y="-60.96" length="short" rot="R180"/>
<pin name="P3[25]/MAT0[0]/PWM1[2]" x="25.4" y="-58.42" length="short" rot="R180"/>
<pin name="VDD(3V3)1" x="-63.5" y="-38.1" length="short"/>
<pin name="P0[29]/USB_DPLUS" x="-63.5" y="-22.86" length="short"/>
<pin name="P0[30]/USB_DMINUS" x="-63.5" y="-25.4" length="short"/>
<pin name="VSS1" x="-63.5" y="-76.2" length="short"/>
<pin name="P1[18]/USB_UP_LED/PWM1[1]/CAP1[0]" x="25.4" y="17.78" length="short" rot="R180"/>
<pin name="P1[19]/MCOA0/USB_PPWR/CAP1[1]" x="25.4" y="15.24" length="short" rot="R180"/>
<pin name="P1[20]/MCI0/PWM1[2]/SCK0" x="25.4" y="12.7" length="short" rot="R180"/>
<pin name="P1[21]/MCABORT/PWM1[3]/SSEL0" x="25.4" y="10.16" length="short" rot="R180"/>
<pin name="P1[22]/MCOB0/USB_PWRD/MAT1[0]" x="25.4" y="7.62" length="short" rot="R180"/>
<pin name="P1[23]/MCI1/PWM1[4]/MISO0" x="25.4" y="5.08" length="short" rot="R180"/>
<pin name="P1[24]/MCI2/PWM1[5]/MOSI0" x="25.4" y="2.54" length="short" rot="R180"/>
<pin name="P1[25]/MCOA1/MAT1[1]" x="25.4" y="0" length="short" rot="R180"/>
<pin name="P1[26]/MCOB1/PWM1[6]/CAP0[0]" x="25.4" y="-2.54" length="short" rot="R180"/>
<pin name="VSS2" x="-63.5" y="-78.74" length="short"/>
<pin name="VDD(REG)(3V3)1" x="-63.5" y="-48.26" length="short"/>
<pin name="P1[27]/CLKOUT/USB_OVRCR/CAP0[1]" x="25.4" y="-5.08" length="short" rot="R180"/>
<pin name="P1[28]/MCOA2/PCAP1[0]/MAT0[0]" x="25.4" y="-7.62" length="short" rot="R180"/>
<pin name="P1[29]/MCOB2/PCAP1[1]/MAT0[1]" x="25.4" y="-10.16" length="short" rot="R180"/>
<pin name="P0[0]/RD1/TXD3/SDA1" x="-63.5" y="43.18" length="short"/>
<pin name="P0[1]/TD1/RXD3/SCL1" x="-63.5" y="40.64" length="short"/>
<pin name="P0[10]/TXD2/SDA2/MAT3[0]" x="-63.5" y="17.78" length="short"/>
<pin name="P0[11]/RXD2/SCL2/MAT3[1]" x="-63.5" y="15.24" length="short"/>
<pin name="P2[13]/EINT3/I2STX_SDA" x="25.4" y="-53.34" length="short" rot="R180"/>
<pin name="P2[12]/EINT2/I2STX_WS" x="25.4" y="-50.8" length="short" rot="R180"/>
<pin name="P2[11]/EINT1/I2STX_CLK" x="25.4" y="-48.26" length="short" rot="R180"/>
<pin name="P2[10]/EINT0/NMI" x="25.4" y="-45.72" length="short" rot="R180"/>
<pin name="VDD(3V3)2" x="-63.5" y="-40.64" length="short"/>
<pin name="VSS3" x="-63.5" y="-81.28" length="short"/>
<pin name="P0[22]/RTS1/TD1" x="-63.5" y="-5.08" length="short"/>
<pin name="P0[21]/RI1/RD1" x="-63.5" y="-2.54" length="short"/>
<pin name="P0[20]/DTR1/SCL1" x="-63.5" y="0" length="short"/>
<pin name="P0[19]/DSR1/SDA1" x="-63.5" y="2.54" length="short"/>
<pin name="P0[18]/DCD1/MOSI0/MOSI" x="-63.5" y="5.08" length="short"/>
<pin name="P0[17]/CTS1/MISO0/MISO" x="-63.5" y="7.62" length="short"/>
<pin name="P0[15]/TXD1/SCK0/SCK" x="-63.5" y="12.7" length="short"/>
<pin name="P0[16]/RXD1/SSEL0/SSEL" x="-63.5" y="10.16" length="short"/>
<pin name="P2[9]/USB_CONNECT/RXD2/ENET_MDIO" x="25.4" y="-43.18" length="short" rot="R180"/>
<pin name="P2[8]/TD2/TXD2/ENET_MDC" x="25.4" y="-40.64" length="short" rot="R180"/>
<pin name="P2[7]/RD2/RTS1" x="25.4" y="-38.1" length="short" rot="R180"/>
<pin name="P2[6]/PCAP1[0]/RI1/TRACECLK" x="25.4" y="-35.56" length="short" rot="R180"/>
<pin name="P2[5]/PWM1[6]/DTR1/TRACEDATA[0]" x="25.4" y="-33.02" length="short" rot="R180"/>
<pin name="P2[4]/PWM1[5]/DSR1/TRACEDATA[1]" x="25.4" y="-30.48" length="short" rot="R180"/>
<pin name="P2[3]/PWM1[4]/DCD1/TRACEDATA[2]" x="25.4" y="-27.94" length="short" rot="R180"/>
<pin name="VDD(3V3)3" x="-63.5" y="-43.18" length="short"/>
<pin name="VSS4" x="-63.5" y="-83.82" length="short"/>
<pin name="P2[2]/PWM1[3]/CTS1/TRACEDATA[3]" x="25.4" y="-25.4" length="short" rot="R180"/>
<pin name="P2[1]/PWM1[2]/RXD1" x="25.4" y="-22.86" length="short" rot="R180"/>
<pin name="P2[0]/PWM1[1]/TXD1" x="25.4" y="-20.32" length="short" rot="R180"/>
<pin name="P0[9]/I2STX_SDA/MOSI1/MAT2[3]" x="-63.5" y="20.32" length="short"/>
<pin name="P0[8]/I2STX_WS/MISO1/MAT2[2]" x="-63.5" y="22.86" length="short"/>
<pin name="P0[7]/I2STX_CLK/SCK1/MAT2[1]" x="-63.5" y="25.4" length="short"/>
<pin name="P0[6]/I2SRX_SDA/SSEL1/MAT2[0]" x="-63.5" y="27.94" length="short"/>
<pin name="P0[5]/I2SRX_WS/TD2/CAP2[1]" x="-63.5" y="30.48" length="short"/>
<pin name="P0[4]/I2SRX_CLK/RD2/CAP2[0]" x="-63.5" y="33.02" length="short"/>
<pin name="P4[28]/RX_MCLK/MAT2[0]/TXD3" x="25.4" y="-66.04" length="short" rot="R180"/>
<pin name="VSS6" x="-63.5" y="-88.9" length="short"/>
<pin name="VDD(REG)(3V3)2" x="-63.5" y="-50.8" length="short"/>
<pin name="P4[29]/TX_MCLK/MAT2[1]/RXD3" x="25.4" y="-68.58" length="short" rot="R180"/>
<pin name="P1[17]/ENET_MDIO" x="25.4" y="20.32" length="short" rot="R180"/>
<pin name="P1[16]/ENET_MDC" x="25.4" y="22.86" length="short" rot="R180"/>
<pin name="P1[15]/ENET_REF_CLK" x="25.4" y="25.4" length="short" rot="R180"/>
<pin name="P1[14]/ENET_RX_ER" x="25.4" y="27.94" length="short" rot="R180"/>
<pin name="P1[10]/ENET_RXD1" x="25.4" y="30.48" length="short" rot="R180"/>
<pin name="P1[9]/ENET_RXD0" x="25.4" y="33.02" length="short" rot="R180"/>
<pin name="P1[8]/ENET_CRS" x="25.4" y="35.56" length="short" rot="R180"/>
<pin name="P1[4]/ENET_TX_EN" x="25.4" y="38.1" length="short" rot="R180"/>
<pin name="P1[1]/ENET_TXD1" x="25.4" y="40.64" length="short" rot="R180"/>
<pin name="P1[0]/ENET_TXD0" x="25.4" y="43.18" length="short" rot="R180"/>
<pin name="VDD(3V3)4" x="-63.5" y="-45.72" length="short"/>
<pin name="VSS5" x="-63.5" y="-86.36" length="short"/>
<pin name="P0[2]/TXD0/AD0[7]" x="-63.5" y="38.1" length="short"/>
<pin name="P0[3]/RXD0/AD0[6]" x="-63.5" y="35.56" length="short"/>
<pin name="RTCK" x="-63.5" y="-106.68" length="short"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LPC1768" prefix="IC">
<description>&lt;b&gt;LPC1768 &lt;/b&gt;&lt;p&gt;100 MHz 32-bit ARM Cortex-M3 based microcontroller featuring a high level of integration and low power consumption. 512 kB Flash, 64 kB SRAM, USB Device/Host/OTG, 2 CAN, DAC, Ethernet &lt;/p&gt;&lt;author&gt;MarcVl - Datasheet available on www.nxp.com&lt;/author&gt;</description>
<gates>
<gate name="G$1" symbol="LPC176X" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LQFP100-14X14">
<connects>
<connect gate="G$1" pin="!RESET" pad="17"/>
<connect gate="G$1" pin="!RSTOUT" pad="14"/>
<connect gate="G$1" pin="NC" pad="13"/>
<connect gate="G$1" pin="P0[0]/RD1/TXD3/SDA1" pad="46"/>
<connect gate="G$1" pin="P0[10]/TXD2/SDA2/MAT3[0]" pad="48"/>
<connect gate="G$1" pin="P0[11]/RXD2/SCL2/MAT3[1]" pad="49"/>
<connect gate="G$1" pin="P0[15]/TXD1/SCK0/SCK" pad="62"/>
<connect gate="G$1" pin="P0[16]/RXD1/SSEL0/SSEL" pad="63"/>
<connect gate="G$1" pin="P0[17]/CTS1/MISO0/MISO" pad="61"/>
<connect gate="G$1" pin="P0[18]/DCD1/MOSI0/MOSI" pad="60"/>
<connect gate="G$1" pin="P0[19]/DSR1/SDA1" pad="59"/>
<connect gate="G$1" pin="P0[1]/TD1/RXD3/SCL1" pad="47"/>
<connect gate="G$1" pin="P0[20]/DTR1/SCL1" pad="58"/>
<connect gate="G$1" pin="P0[21]/RI1/RD1" pad="57"/>
<connect gate="G$1" pin="P0[22]/RTS1/TD1" pad="56"/>
<connect gate="G$1" pin="P0[23]/AD0[0]/I2SRX_CLK/CAP3[0]" pad="9"/>
<connect gate="G$1" pin="P0[24]/AD0[1]/I2SRX_WS/CAP3[1]" pad="8"/>
<connect gate="G$1" pin="P0[25]/AD0[2]/I2SRX_SDA/TXD3" pad="7"/>
<connect gate="G$1" pin="P0[26]/AD0[3]/AOUT/RXD3" pad="6"/>
<connect gate="G$1" pin="P0[27]/SDA0/USB_SDA" pad="25"/>
<connect gate="G$1" pin="P0[28]/SCL0/USB_SCL" pad="24"/>
<connect gate="G$1" pin="P0[29]/USB_DPLUS" pad="29"/>
<connect gate="G$1" pin="P0[2]/TXD0/AD0[7]" pad="98"/>
<connect gate="G$1" pin="P0[30]/USB_DMINUS" pad="30"/>
<connect gate="G$1" pin="P0[3]/RXD0/AD0[6]" pad="99"/>
<connect gate="G$1" pin="P0[4]/I2SRX_CLK/RD2/CAP2[0]" pad="81"/>
<connect gate="G$1" pin="P0[5]/I2SRX_WS/TD2/CAP2[1]" pad="80"/>
<connect gate="G$1" pin="P0[6]/I2SRX_SDA/SSEL1/MAT2[0]" pad="79"/>
<connect gate="G$1" pin="P0[7]/I2STX_CLK/SCK1/MAT2[1]" pad="78"/>
<connect gate="G$1" pin="P0[8]/I2STX_WS/MISO1/MAT2[2]" pad="77"/>
<connect gate="G$1" pin="P0[9]/I2STX_SDA/MOSI1/MAT2[3]" pad="76"/>
<connect gate="G$1" pin="P1[0]/ENET_TXD0" pad="95"/>
<connect gate="G$1" pin="P1[10]/ENET_RXD1" pad="90"/>
<connect gate="G$1" pin="P1[14]/ENET_RX_ER" pad="89"/>
<connect gate="G$1" pin="P1[15]/ENET_REF_CLK" pad="88"/>
<connect gate="G$1" pin="P1[16]/ENET_MDC" pad="87"/>
<connect gate="G$1" pin="P1[17]/ENET_MDIO" pad="86"/>
<connect gate="G$1" pin="P1[18]/USB_UP_LED/PWM1[1]/CAP1[0]" pad="32"/>
<connect gate="G$1" pin="P1[19]/MCOA0/USB_PPWR/CAP1[1]" pad="33"/>
<connect gate="G$1" pin="P1[1]/ENET_TXD1" pad="94"/>
<connect gate="G$1" pin="P1[20]/MCI0/PWM1[2]/SCK0" pad="34"/>
<connect gate="G$1" pin="P1[21]/MCABORT/PWM1[3]/SSEL0" pad="35"/>
<connect gate="G$1" pin="P1[22]/MCOB0/USB_PWRD/MAT1[0]" pad="36"/>
<connect gate="G$1" pin="P1[23]/MCI1/PWM1[4]/MISO0" pad="37"/>
<connect gate="G$1" pin="P1[24]/MCI2/PWM1[5]/MOSI0" pad="38"/>
<connect gate="G$1" pin="P1[25]/MCOA1/MAT1[1]" pad="39"/>
<connect gate="G$1" pin="P1[26]/MCOB1/PWM1[6]/CAP0[0]" pad="40"/>
<connect gate="G$1" pin="P1[27]/CLKOUT/USB_OVRCR/CAP0[1]" pad="43"/>
<connect gate="G$1" pin="P1[28]/MCOA2/PCAP1[0]/MAT0[0]" pad="44"/>
<connect gate="G$1" pin="P1[29]/MCOB2/PCAP1[1]/MAT0[1]" pad="45"/>
<connect gate="G$1" pin="P1[30]/VBUS/AD0[4]" pad="21"/>
<connect gate="G$1" pin="P1[31]/SCK1/AD0[5]" pad="20"/>
<connect gate="G$1" pin="P1[4]/ENET_TX_EN" pad="93"/>
<connect gate="G$1" pin="P1[8]/ENET_CRS" pad="92"/>
<connect gate="G$1" pin="P1[9]/ENET_RXD0" pad="91"/>
<connect gate="G$1" pin="P2[0]/PWM1[1]/TXD1" pad="75"/>
<connect gate="G$1" pin="P2[10]/EINT0/NMI" pad="53"/>
<connect gate="G$1" pin="P2[11]/EINT1/I2STX_CLK" pad="52"/>
<connect gate="G$1" pin="P2[12]/EINT2/I2STX_WS" pad="51"/>
<connect gate="G$1" pin="P2[13]/EINT3/I2STX_SDA" pad="50"/>
<connect gate="G$1" pin="P2[1]/PWM1[2]/RXD1" pad="74"/>
<connect gate="G$1" pin="P2[2]/PWM1[3]/CTS1/TRACEDATA[3]" pad="73"/>
<connect gate="G$1" pin="P2[3]/PWM1[4]/DCD1/TRACEDATA[2]" pad="70"/>
<connect gate="G$1" pin="P2[4]/PWM1[5]/DSR1/TRACEDATA[1]" pad="69"/>
<connect gate="G$1" pin="P2[5]/PWM1[6]/DTR1/TRACEDATA[0]" pad="68"/>
<connect gate="G$1" pin="P2[6]/PCAP1[0]/RI1/TRACECLK" pad="67"/>
<connect gate="G$1" pin="P2[7]/RD2/RTS1" pad="66"/>
<connect gate="G$1" pin="P2[8]/TD2/TXD2/ENET_MDC" pad="65"/>
<connect gate="G$1" pin="P2[9]/USB_CONNECT/RXD2/ENET_MDIO" pad="64"/>
<connect gate="G$1" pin="P3[25]/MAT0[0]/PWM1[2]" pad="27"/>
<connect gate="G$1" pin="P3[26]/STCLK/MAT0[1]/PWM1[3]" pad="26"/>
<connect gate="G$1" pin="P4[28]/RX_MCLK/MAT2[0]/TXD3" pad="82"/>
<connect gate="G$1" pin="P4[29]/TX_MCLK/MAT2[1]/RXD3" pad="85"/>
<connect gate="G$1" pin="RTCK" pad="100"/>
<connect gate="G$1" pin="RTCX1" pad="16"/>
<connect gate="G$1" pin="RTCX2" pad="18"/>
<connect gate="G$1" pin="TCK/SWDCLK" pad="5"/>
<connect gate="G$1" pin="TDI" pad="2"/>
<connect gate="G$1" pin="TDO/SWO" pad="1"/>
<connect gate="G$1" pin="TMS/SWDIO" pad="3"/>
<connect gate="G$1" pin="TRST" pad="4"/>
<connect gate="G$1" pin="VBAT" pad="19"/>
<connect gate="G$1" pin="VDD(3V3)1" pad="28"/>
<connect gate="G$1" pin="VDD(3V3)2" pad="54"/>
<connect gate="G$1" pin="VDD(3V3)3" pad="71"/>
<connect gate="G$1" pin="VDD(3V3)4" pad="96"/>
<connect gate="G$1" pin="VDD(REG)(3V3)1" pad="42"/>
<connect gate="G$1" pin="VDD(REG)(3V3)2" pad="84"/>
<connect gate="G$1" pin="VDDA" pad="10"/>
<connect gate="G$1" pin="VREFN" pad="15"/>
<connect gate="G$1" pin="VREFP" pad="12"/>
<connect gate="G$1" pin="VSS1" pad="31"/>
<connect gate="G$1" pin="VSS2" pad="41"/>
<connect gate="G$1" pin="VSS3" pad="55"/>
<connect gate="G$1" pin="VSS4" pad="72"/>
<connect gate="G$1" pin="VSS5" pad="97"/>
<connect gate="G$1" pin="VSS6" pad="83"/>
<connect gate="G$1" pin="VSSA" pad="11"/>
<connect gate="G$1" pin="XTAL1" pad="22"/>
<connect gate="G$1" pin="XTAL2" pad="23"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="elektromer">
<packages>
<package name="20SSOP">
<wire x1="-0.635" y1="0" x2="6.565" y2="0" width="0.254" layer="39"/>
<wire x1="6.565" y1="0" x2="6.565" y2="1.2" width="0.254" layer="39"/>
<wire x1="6.565" y1="1.2" x2="6.565" y2="6" width="0.254" layer="21"/>
<wire x1="6.565" y1="6" x2="6.565" y2="7.2" width="0.254" layer="39"/>
<wire x1="6.565" y1="7.2" x2="-0.635" y2="7.2" width="0.254" layer="39"/>
<wire x1="-0.635" y1="7.2" x2="-0.635" y2="6" width="0.254" layer="39"/>
<wire x1="-0.635" y1="6" x2="-0.635" y2="1.2" width="0.254" layer="21"/>
<wire x1="-0.635" y1="1.2" x2="-0.635" y2="0" width="0.254" layer="39"/>
<wire x1="-0.635" y1="1.2" x2="6.565" y2="1.2" width="0.254" layer="21"/>
<wire x1="-0.635" y1="6" x2="6.565" y2="6" width="0.254" layer="21"/>
<circle x="0.635" y="2.54" radius="0.5" width="0.254" layer="21"/>
<smd name="1" x="0" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="2" x="0.635" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="3" x="1.27" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="4" x="1.905" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="5" x="2.54" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="6" x="3.175" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="8" x="4.445" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="7" x="3.81" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="9" x="5.08" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="10" x="5.715" y="0" dx="0.42" dy="1.34" layer="1"/>
<smd name="20" x="0" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<smd name="19" x="0.635" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<smd name="18" x="1.27" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<smd name="17" x="1.905" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<smd name="16" x="2.54" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<smd name="15" x="3.175" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<smd name="14" x="3.81" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<smd name="13" x="4.445" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<smd name="12" x="5.08" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<smd name="11" x="5.715" y="7.2" dx="0.42" dy="1.34" layer="1"/>
<text x="1.905" y="4.445" size="0.8128" layer="25">&gt;NAME</text>
<text x="1.905" y="3.175" size="0.8128" layer="27">&gt;VALUE</text>
</package>
<package name="2PORT">
<pad name="P$1" x="0" y="0" drill="0.8" shape="square"/>
<pad name="P$2" x="0" y="3.81" drill="0.8" shape="square"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="SOP08">
<description>&lt;b&gt;SMALL OUTLINE PACKAGE&lt;/b&gt;</description>
<wire x1="-3.4" y1="3.9" x2="3.4" y2="3.9" width="0.1998" layer="39"/>
<wire x1="3.4" y1="3.9" x2="3.4" y2="-3.9" width="0.1998" layer="39"/>
<wire x1="-3.4" y1="-3.9" x2="-3.4" y2="3.9" width="0.1998" layer="39"/>
<wire x1="3.07" y1="2.26" x2="3.07" y2="-2.26" width="0.2032" layer="51"/>
<wire x1="3.07" y1="-2.26" x2="-3.07" y2="-2.26" width="0.2032" layer="51"/>
<wire x1="-3.07" y1="-2.26" x2="-3.07" y2="2.26" width="0.2032" layer="51"/>
<wire x1="-3.07" y1="2.26" x2="3.07" y2="2.26" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-3.9" x2="-3.4" y2="-3.9" width="0.1998" layer="39"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-3.175" y="-3.175" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.175" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.27" y="-1.27" size="1.27" layer="21" rot="R90">1</text>
<rectangle x1="-2.1549" y1="-3.49" x2="-1.6551" y2="-2.3599" layer="51"/>
<rectangle x1="-0.8849" y1="-3.49" x2="-0.3851" y2="-2.3599" layer="51"/>
<rectangle x1="0.3851" y1="-3.49" x2="0.8849" y2="-2.3599" layer="51"/>
<rectangle x1="1.6551" y1="-3.49" x2="2.1549" y2="-2.3599" layer="51"/>
<rectangle x1="1.6551" y1="2.3599" x2="2.1549" y2="3.49" layer="51"/>
<rectangle x1="0.3851" y1="2.3599" x2="0.8849" y2="3.49" layer="51"/>
<rectangle x1="-0.8849" y1="2.3599" x2="-0.3851" y2="3.49" layer="51"/>
<rectangle x1="-2.1549" y1="2.3599" x2="-1.6551" y2="3.49" layer="51"/>
</package>
<package name="FUSE-LITTLEFUSE">
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<smd name="P$1" x="-4.675" y="0" dx="3.25" dy="3.45" layer="1"/>
<smd name="P$2" x="4.675" y="0" dx="3.25" dy="3.45" layer="1"/>
</package>
<package name="MEV3SV0505SC">
<wire x1="-2.5146" y1="-6.1976" x2="-2.5146" y2="1.4478" width="0.127" layer="21"/>
<wire x1="-2.5146" y1="1.4478" x2="17.145" y2="1.4478" width="0.127" layer="21"/>
<wire x1="17.145" y1="1.4478" x2="17.145" y2="-6.1976" width="0.127" layer="21"/>
<wire x1="17.145" y1="-6.1976" x2="-2.5146" y2="-6.1976" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.143"/>
<pad name="2" x="2.54" y="0" drill="1.143"/>
<pad name="5" x="10.16" y="0" drill="1.143"/>
<pad name="7" x="15.24" y="0" drill="1.143"/>
<text x="-1.27" y="-5.08" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="AKR103/2">
<wire x1="-7.2" y1="-2.5" x2="-2.54" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.5" x2="3.175" y2="-2.5" width="0.127" layer="21"/>
<wire x1="3.175" y1="-2.5" x2="6.4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="6.4" y1="-2.5" x2="6.4" y2="7.5" width="0.127" layer="21"/>
<wire x1="6.4" y1="7.5" x2="3.175" y2="7.5" width="0.127" layer="21"/>
<wire x1="3.175" y1="7.5" x2="-2.54" y2="7.5" width="0.127" layer="21"/>
<wire x1="-2.54" y1="7.5" x2="-7.2" y2="7.5" width="0.127" layer="21"/>
<wire x1="-7.2" y1="7.5" x2="-7.2" y2="2.54" width="0.127" layer="21"/>
<wire x1="-7.2" y1="2.54" x2="-7.2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="3.175" y1="7.5" x2="3.175" y2="2.54" width="0.127" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.175" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.54" y1="7.5" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.5" width="0.127" layer="21"/>
<wire x1="4.445" y1="-0.635" x2="5.715" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.715" y1="4.445" x2="4.445" y2="5.715" width="0.127" layer="21"/>
<wire x1="-6.35" y1="6.35" x2="-6.35" y2="3.81" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.81" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="6.35" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.35" x2="-6.35" y2="6.35" width="0.127" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-6.35" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="2.54" x2="6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-7.2" y2="2.54" width="0.127" layer="21"/>
<circle x="5.08" y="0" radius="1.27" width="0.127" layer="21"/>
<circle x="5.08" y="5.08" radius="1.27" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.1844"/>
<pad name="2" x="0" y="5" drill="1.2" diameter="2.1844"/>
<text x="6.35" y="-3.175" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
<package name="CONECTOR_JTAG">
<wire x1="1.27" y1="6.35" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="6.35" width="0.127" layer="21"/>
<wire x1="5.08" y1="6.35" x2="1.27" y2="6.35" width="0.127" layer="21"/>
<circle x="2.54" y="5.08" radius="0.635" width="0.127" layer="21"/>
<smd name="1" x="1.2" y="0" dx="2.4" dy="0.76" layer="1"/>
<smd name="5" x="1.2" y="3.81" dx="2.4" dy="0.76" layer="1"/>
<smd name="3" x="1.2" y="5.08" dx="2.4" dy="0.76" layer="1"/>
<smd name="7" x="1.2" y="2.54" dx="2.4" dy="0.76" layer="1"/>
<smd name="9" x="1.2" y="1.27" dx="2.4" dy="0.76" layer="1"/>
<smd name="2" x="5.1" y="5.08" dx="2.4" dy="0.76" layer="1"/>
<smd name="4" x="5.1" y="3.81" dx="2.4" dy="0.76" layer="1"/>
<smd name="6" x="5.1" y="2.54" dx="2.4" dy="0.76" layer="1"/>
<smd name="8" x="5.1" y="1.27" dx="2.4" dy="0.76" layer="1"/>
<smd name="10" x="5.1" y="0" dx="2.4" dy="0.76" layer="1"/>
<text x="0" y="6.985" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.905" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="FUSE-BOURNS">
<wire x1="-4.825" y1="1.525" x2="4.825" y2="1.525" width="0.127" layer="21"/>
<wire x1="-4.825" y1="-1.525" x2="4.825" y2="-1.525" width="0.127" layer="21"/>
<wire x1="-4.825" y1="1.525" x2="-4.825" y2="-1.525" width="0.127" layer="21"/>
<wire x1="4.825" y1="1.525" x2="4.825" y2="-1.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="-1.905" width="0.127" layer="39"/>
<wire x1="-6.35" y1="-1.905" x2="6.35" y2="-1.905" width="0.127" layer="39"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="1.905" width="0.127" layer="39"/>
<wire x1="6.35" y1="1.905" x2="-6.35" y2="1.905" width="0.127" layer="39"/>
<smd name="P$1" x="-4.445" y="0" dx="4.06" dy="3.81" layer="1" rot="R90"/>
<smd name="P$2" x="4.445" y="0" dx="4.06" dy="3.81" layer="1" rot="R90"/>
<text x="-3.175" y="-0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="3.81" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="SOIC16">
<wire x1="-4.31" y1="1.9" x2="5.59" y2="1.9" width="0.127" layer="21"/>
<wire x1="5.59" y1="1.9" x2="5.59" y2="-1.9" width="0.127" layer="21"/>
<wire x1="5.59" y1="-1.9" x2="-4.31" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-4.31" y1="-1.9" x2="-4.31" y2="1.9" width="0.127" layer="21"/>
<wire x1="-4.445" y1="3.81" x2="-4.445" y2="-3.81" width="0.127" layer="39"/>
<wire x1="-4.445" y1="-3.81" x2="5.715" y2="-3.81" width="0.127" layer="39"/>
<wire x1="5.715" y1="-3.81" x2="5.715" y2="3.81" width="0.127" layer="39"/>
<wire x1="5.715" y1="3.81" x2="-4.445" y2="3.81" width="0.127" layer="39"/>
<smd name="1" x="-3.84" y="-2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="2" x="-2.56" y="-2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="3" x="-1.28" y="-2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="4" x="0" y="-2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="5" x="1.28" y="-2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="6" x="2.56" y="-2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="7" x="3.84" y="-2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="8" x="5.12" y="-2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="9" x="5.12" y="2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="10" x="3.84" y="2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="11" x="2.56" y="2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="12" x="1.28" y="2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="13" x="0" y="2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="14" x="-1.28" y="2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="15" x="-2.56" y="2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="16" x="-3.84" y="2.75" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<text x="-3.175" y="-0.635" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SOIC16-WIDE">
<wire x1="-5.25" y1="3.7" x2="-5.25" y2="-3.7" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-3.7" x2="5.25" y2="-3.7" width="0.127" layer="21"/>
<wire x1="5.25" y1="-3.7" x2="5.25" y2="3.7" width="0.127" layer="21"/>
<wire x1="5.25" y1="3.7" x2="-5.25" y2="3.7" width="0.127" layer="21"/>
<wire x1="-5.715" y1="5.5" x2="5.715" y2="5.5" width="0.127" layer="39"/>
<wire x1="-5.715" y1="5.5" x2="-5.715" y2="-5.5" width="0.127" layer="39"/>
<wire x1="-5.715" y1="-5.5" x2="5.715" y2="-5.5" width="0.127" layer="39"/>
<wire x1="5.715" y1="-5.5" x2="5.715" y2="5.5" width="0.127" layer="39"/>
<circle x="-3.81" y="-2.54" radius="0.635" width="0.127" layer="21"/>
<smd name="1" x="-4.41" y="-4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="2" x="-3.15" y="-4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="3" x="-1.89" y="-4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="4" x="-0.63" y="-4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="5" x="0.63" y="-4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="6" x="1.89" y="-4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="7" x="3.15" y="-4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="8" x="4.41" y="-4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="9" x="4.41" y="4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="10" x="3.15" y="4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="11" x="1.89" y="4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="12" x="0.63" y="4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="13" x="-0.63" y="4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="14" x="-1.89" y="4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="15" x="-3.15" y="4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<smd name="16" x="-4.41" y="4.81" dx="1.43" dy="0.55" layer="1" rot="R90"/>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-1.27" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="TRAFO23">
<circle x="0" y="0" radius="11.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="10.9249" width="0.127" layer="21"/>
<circle x="0" y="0" radius="11.9811" width="0.127" layer="39"/>
<pad name="P$1" x="9.525" y="-8.89" drill="0.8" diameter="1.4224"/>
<pad name="P$2" x="11.43" y="-6.35" drill="0.8" diameter="1.4224"/>
<text x="-3.81" y="-5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-6.985" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="3.5"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="QFN24_4MM">
<wire x1="1.65" y1="-2" x2="2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.65" x2="2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.65" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2" x2="-2" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="2" y1="1.65" x2="2" y2="2" width="0.2032" layer="21"/>
<wire x1="2" y1="2" x2="1.65" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.65" y1="2" x2="-2" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.127" layer="51"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="51"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="51"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="51"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="39"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="39"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.127" layer="39"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="39"/>
<circle x="-3.048" y="1.27" radius="0.127" width="0.254" layer="21"/>
<pad name="THERM" x="0" y="0" drill="0.6" diameter="1.778" shape="square"/>
<smd name="1" x="-2" y="1.25" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R180"/>
<smd name="2" x="-2" y="0.75" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R180"/>
<smd name="3" x="-2" y="0.25" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R180"/>
<smd name="4" x="-2" y="-0.25" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R180"/>
<smd name="5" x="-2" y="-0.75" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R180"/>
<smd name="6" x="-2" y="-1.25" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R180"/>
<smd name="7" x="-1.25" y="-2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R90"/>
<smd name="8" x="-0.75" y="-2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R90"/>
<smd name="9" x="-0.25" y="-2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R90"/>
<smd name="10" x="0.25" y="-2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R90"/>
<smd name="11" x="0.75" y="-2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R90"/>
<smd name="12" x="1.25" y="-2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R90"/>
<smd name="13" x="2" y="-1.25" dx="0.8" dy="0.28" layer="1" roundness="50"/>
<smd name="14" x="2" y="-0.75" dx="0.8" dy="0.28" layer="1" roundness="50"/>
<smd name="15" x="2" y="-0.25" dx="0.8" dy="0.28" layer="1" roundness="50"/>
<smd name="16" x="2" y="0.25" dx="0.8" dy="0.28" layer="1" roundness="50"/>
<smd name="17" x="2" y="0.75" dx="0.8" dy="0.28" layer="1" roundness="50"/>
<smd name="18" x="2" y="1.25" dx="0.8" dy="0.28" layer="1" roundness="50"/>
<smd name="19" x="1.25" y="2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R270"/>
<smd name="20" x="0.75" y="2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R270"/>
<smd name="21" x="0.25" y="2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R270"/>
<smd name="22" x="-0.25" y="2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R270"/>
<smd name="23" x="-0.75" y="2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R270"/>
<smd name="24" x="-1.25" y="2" dx="0.8" dy="0.28" layer="1" roundness="50" rot="R270"/>
<text x="-2.45" y="2.8" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.5" y="-3.15" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="0.1" y1="0.1" x2="0.75" y2="0.75" layer="31"/>
<rectangle x1="0.1" y1="-0.75" x2="0.75" y2="-0.1" layer="31"/>
<rectangle x1="-0.75" y1="0.1" x2="-0.1" y2="0.75" layer="31"/>
<rectangle x1="-0.75" y1="-0.75" x2="-0.1" y2="-0.1" layer="31"/>
</package>
<package name="MF-R005">
<wire x1="3.81" y1="0" x2="2.81" y2="-1" width="0.127" layer="21" curve="-90"/>
<wire x1="2.81" y1="-1" x2="-2.81" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.81" y1="-1" x2="-3.81" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="-2.81" y2="1" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.81" y1="1" x2="2.81" y2="1" width="0.127" layer="21"/>
<wire x1="2.81" y1="1" x2="3.81" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.127" layer="39"/>
<wire x1="-3.81" y1="-1.27" x2="3.81" y2="-1.27" width="0.127" layer="39"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="39"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.127" layer="39"/>
<pad name="P$1" x="-2.54" y="0" drill="1" diameter="1.6" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="1" diameter="1.6" shape="octagon"/>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="3.81" y="-1.27" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="TRANSIL-SMP100LC">
<wire x1="-2.3" y1="1.975" x2="-2.3" y2="-1.975" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.975" x2="2.3" y2="-1.975" width="0.127" layer="21"/>
<wire x1="2.3" y1="-1.975" x2="2.3" y2="1.975" width="0.127" layer="21"/>
<wire x1="2.3" y1="1.975" x2="-2.3" y2="1.975" width="0.127" layer="21"/>
<wire x1="-0.8" y1="1.905" x2="-0.8" y2="-1.905" width="0.127" layer="21"/>
<wire x1="0.8" y1="1.905" x2="0.8" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="3.175" y2="-2.54" width="0.127" layer="39"/>
<wire x1="3.175" y1="-2.54" x2="3.175" y2="2.54" width="0.127" layer="39"/>
<wire x1="3.175" y1="2.54" x2="-3.175" y2="2.54" width="0.127" layer="39"/>
<wire x1="-3.175" y1="2.54" x2="-3.175" y2="-2.54" width="0.127" layer="39"/>
<smd name="P$1" x="-2.11" y="0" dx="1.62" dy="2.18" layer="1" rot="R180"/>
<smd name="P$2" x="2.11" y="0" dx="1.62" dy="2.18" layer="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="25">&gt;VALUE</text>
<rectangle x1="-2.3" y1="-1.975" x2="-0.8" y2="1.975" layer="21"/>
<rectangle x1="0.875" y1="-1.975" x2="2.375" y2="1.975" layer="21"/>
</package>
<package name="TRACO-TMTL04">
<wire x1="24" y1="32.75" x2="-3" y2="32.75" width="0.127" layer="21"/>
<wire x1="-3" y1="32.75" x2="-3" y2="-3.75" width="0.127" layer="21"/>
<wire x1="-3" y1="-3.75" x2="24" y2="-3.75" width="0.127" layer="21"/>
<wire x1="24" y1="-3.75" x2="24" y2="32.75" width="0.127" layer="21"/>
<wire x1="-3.175" y1="33.02" x2="-3.175" y2="-4.445" width="0.127" layer="39"/>
<wire x1="-3.175" y1="-4.445" x2="24.13" y2="-4.445" width="0.127" layer="39"/>
<wire x1="24.13" y1="-4.445" x2="24.13" y2="33.02" width="0.127" layer="39"/>
<wire x1="24.13" y1="33.02" x2="-3.175" y2="33.02" width="0.127" layer="39"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.54"/>
<pad name="7" x="21" y="0" drill="1.2" diameter="2.54"/>
<pad name="3" x="0" y="25.25" drill="1.2" diameter="2.54"/>
<pad name="5" x="21" y="29" drill="1.2" diameter="2.54"/>
<pad name="6" x="21" y="21.5" drill="1.2" diameter="2.54"/>
<pad name="4" x="0" y="29" drill="1.2" diameter="2.54"/>
<pad name="2" x="0" y="21.5" drill="1.2" diameter="2.54"/>
</package>
<package name="XTAL-LEZATY">
<circle x="0" y="0" radius="1.05" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.4199" width="0.127" layer="39"/>
<pad name="P$1" x="-0.8" y="0" drill="0.6"/>
<pad name="P$2" x="0.8" y="0" drill="0.6"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SWITCH-EVQP0">
<wire x1="-3.25" y1="3" x2="3.25" y2="3" width="0.127" layer="21"/>
<wire x1="3.25" y1="3" x2="3.25" y2="-3" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3" x2="-3.25" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.25" y1="-3" x2="-3.25" y2="3" width="0.127" layer="21"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.127" layer="39"/>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.127" layer="39"/>
<wire x1="5.08" y1="-3.175" x2="5.08" y2="3.175" width="0.127" layer="39"/>
<wire x1="5.08" y1="3.175" x2="-5.08" y2="3.175" width="0.127" layer="39"/>
<circle x="0" y="0" radius="1.796" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<smd name="B" x="-3.4" y="2" dx="3.2" dy="1" layer="1"/>
<smd name="A" x="-3.4" y="-2" dx="3.2" dy="1" layer="1"/>
<smd name="B1" x="3.4" y="2" dx="3.2" dy="1" layer="1"/>
<smd name="A1" x="3.4" y="-2" dx="3.2" dy="1" layer="1"/>
<text x="-3.175" y="3.175" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SDCON">
<wire x1="0" y1="0" x2="0" y2="-15.875" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="14.3" y2="0" width="0.127" layer="21"/>
<wire x1="14.3" y1="0" x2="14.3" y2="-15.875" width="0.127" layer="21"/>
<wire x1="0" y1="-15.875" x2="14.3" y2="-15.875" width="0.127" layer="21"/>
<wire x1="0.25" y1="-15.24" x2="0.25" y2="-0.15" width="0.127" layer="21" style="shortdash"/>
<wire x1="0.25" y1="-0.15" x2="2.54" y2="-0.15" width="0.127" layer="21" style="shortdash"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-0.15" width="0.127" layer="21" style="shortdash"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="-0.15" width="0.127" layer="21" style="shortdash"/>
<wire x1="12.7" y1="-0.15" x2="13.97" y2="-0.15" width="0.127" layer="21" style="shortdash"/>
<wire x1="13.97" y1="-0.15" x2="13.97" y2="-15.24" width="0.127" layer="21" style="shortdash"/>
<wire x1="13.97" y1="-15.24" x2="0.25" y2="-15.24" width="0.127" layer="21" style="shortdash"/>
<wire x1="2.54" y1="-1.27" x2="5.715" y2="-1.905" width="0.127" layer="21" style="shortdash"/>
<wire x1="5.715" y1="-1.905" x2="10.16" y2="-1.905" width="0.127" layer="21" style="shortdash"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.27" width="0.127" layer="21" style="shortdash"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="-16.51" width="0.127" layer="39"/>
<wire x1="-0.635" y1="-16.51" x2="14.605" y2="-16.51" width="0.127" layer="39"/>
<wire x1="14.605" y1="-16.51" x2="14.605" y2="0.635" width="0.127" layer="39"/>
<wire x1="14.605" y1="0.635" x2="-0.635" y2="0.635" width="0.127" layer="39"/>
<smd name="PIN1" x="4.45" y="-3.75" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="PIN2" x="5.55" y="-3.75" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="PIN3" x="6.65" y="-3.75" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="PIN4" x="7.75" y="-3.75" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="PIN5" x="8.85" y="-3.75" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="PIN6" x="9.95" y="-3.75" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="PIN7" x="11.05" y="-3.75" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="PIN8" x="12.15" y="-3.75" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="CD_GND" x="13.725" y="-10.6" dx="1.5" dy="1.15" layer="1" rot="R90"/>
<smd name="SHELL3" x="4.25" y="-15.325" dx="1.5" dy="1.15" layer="1"/>
<smd name="CD" x="6.55" y="-15.325" dx="1.5" dy="1.15" layer="1"/>
<smd name="SHELL4" x="13.15" y="-15.325" dx="1.5" dy="1.15" layer="1"/>
<smd name="SHELL1" x="0.7" y="-0.85" dx="1.7" dy="1.4" layer="1" rot="R90"/>
<smd name="SHELL2" x="13.5" y="-0.85" dx="1.7" dy="1.4" layer="1" rot="R90"/>
<text x="3.81" y="-7.62" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="11.15" y1="-14.55" x2="14.05" y2="-13.15" layer="41"/>
<rectangle x1="7.5" y1="-15.45" x2="8.5" y2="-14.95" layer="41"/>
<rectangle x1="13.35" y1="-9.65" x2="13.85" y2="-8.65" layer="41"/>
</package>
<package name="SOD-123">
<description>&lt;b&gt;SOD-123&lt;/b&gt;
&lt;p&gt;Source: http://www.diodes.com/datasheets/ds30139.pdf&lt;/p&gt;</description>
<wire x1="-0.5" y1="0.746" x2="-0.5" y2="0" width="0.254" layer="21"/>
<wire x1="-0.5" y1="0" x2="-0.5" y2="-0.746" width="0.254" layer="21"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0.746" width="0.254" layer="21"/>
<wire x1="0.5" y1="0.746" x2="0.5" y2="-0.746" width="0.254" layer="21"/>
<wire x1="0.5" y1="-0.746" x2="-0.5" y2="0" width="0.254" layer="21"/>
<wire x1="-1.35" y1="0.775" x2="1.35" y2="0.775" width="0.127" layer="51"/>
<wire x1="1.35" y1="0.775" x2="1.35" y2="-0.775" width="0.127" layer="51"/>
<wire x1="1.35" y1="-0.775" x2="-1.35" y2="-0.775" width="0.127" layer="51"/>
<wire x1="-1.35" y1="-0.775" x2="-1.35" y2="0.775" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.6" x2="-1.4" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="-0.9" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.4" y1="0.6" x2="1.4" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.4" y1="0.8" x2="0.9" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.6" x2="-1.4" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="-0.9" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.6" x2="1.4" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="0.9" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.127" layer="39"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="39"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.127" layer="39"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="39"/>
<smd name="C" x="-1.85" y="0" dx="0.7" dy="1.2" layer="1" rot="R90"/>
<smd name="A" x="1.85" y="0" dx="0.7" dy="1.2" layer="1" rot="R90"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="-1.524" size="0.4064" layer="25">&gt;VALUE</text>
</package>
<package name="D0805">
<description>&lt;b&gt;LED diode&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<wire x1="0.254" y1="0" x2="-0.254" y2="0.381" width="0.0634" layer="21"/>
<wire x1="-0.254" y1="0.381" x2="-0.254" y2="0" width="0.0634" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.381" width="0.0634" layer="21"/>
<wire x1="-0.254" y1="-0.381" x2="0.254" y2="0" width="0.0634" layer="21"/>
<wire x1="0.254" y1="0.381" x2="0.254" y2="0" width="0.0634" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.381" width="0.0634" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="-1.27" width="0.0634" layer="39"/>
<wire x1="-1.905" y1="-1.27" x2="1.905" y2="-1.27" width="0.0634" layer="39"/>
<wire x1="1.905" y1="-1.27" x2="1.905" y2="1.27" width="0.0634" layer="39"/>
<wire x1="1.905" y1="1.27" x2="-1.905" y2="1.27" width="0.0634" layer="39"/>
<wire x1="0.254" y1="0" x2="0.381" y2="0" width="0.0634" layer="21"/>
<wire x1="0.254" y1="0" x2="0.381" y2="0" width="0.0634" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.254" y2="0" width="0.0634" layer="21"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<text x="-1.524" y="-0.635" size="1.27" layer="21">+</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="1.7018" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="1.7018" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="1.7018" y1="0.787" x2="1.7018" y2="-0.787" width="0.1016" layer="21"/>
<wire x1="0.9517" y1="0.787" x2="0.9517" y2="-0.787" width="0.1016" layer="21"/>
<smd name="1-" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2+" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="1" y="-0.4" size="0.8" layer="21">+</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C2917">
<wire x1="3.683" y1="-2.159" x2="1.905" y2="-2.159" width="0.127" layer="21"/>
<wire x1="1.905" y1="-2.159" x2="-1.905" y2="-2.159" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-2.159" x2="-3.683" y2="-2.159" width="0.127" layer="21"/>
<wire x1="-3.683" y1="-2.159" x2="-3.683" y2="2.159" width="0.127" layer="21"/>
<wire x1="-3.683" y1="2.159" x2="-1.905" y2="2.159" width="0.127" layer="21"/>
<wire x1="-1.905" y1="2.159" x2="1.905" y2="2.159" width="0.127" layer="21"/>
<wire x1="1.905" y1="2.159" x2="3.683" y2="2.159" width="0.127" layer="21"/>
<wire x1="3.683" y1="2.159" x2="3.683" y2="-2.159" width="0.127" layer="21"/>
<wire x1="-1.905" y1="2.159" x2="-1.905" y2="-2.159" width="0.127" layer="21"/>
<wire x1="1.905" y1="2.159" x2="1.905" y2="-2.159" width="0.127" layer="21"/>
<smd name="+" x="-3.175" y="0" dx="3.81" dy="1.9304" layer="1" rot="R90"/>
<smd name="-" x="3.175" y="0" dx="3.81" dy="1.9304" layer="1" rot="R90"/>
<text x="-3.81" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="21">+</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="1.905" y1="-2.159" x2="3.683" y2="2.159" layer="21"/>
</package>
<package name="RJ45-JACK-BEZLED">
<wire x1="-8.43" y1="-10.4" x2="-7.62" y2="-10.4" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-10.4" x2="-5.715" y2="-10.4" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-10.4" x2="-2" y2="-10.4" width="0.127" layer="21"/>
<wire x1="-2" y1="-10.4" x2="2" y2="-10.4" width="0.127" layer="21"/>
<wire x1="2" y1="-10.4" x2="5.715" y2="-10.4" width="0.127" layer="21"/>
<wire x1="5.715" y1="-10.4" x2="7.62" y2="-10.4" width="0.127" layer="21"/>
<wire x1="7.62" y1="-10.4" x2="8.43" y2="-10.4" width="0.127" layer="21"/>
<wire x1="8.43" y1="-10.4" x2="8.43" y2="4.445" width="0.127" layer="21"/>
<wire x1="7.45" y1="10.16" x2="-7.45" y2="10.16" width="0.127" layer="21"/>
<wire x1="-8.43" y1="4.445" x2="-8.43" y2="-10.4" width="0.127" layer="21"/>
<wire x1="-8.43" y1="4.445" x2="-7.45" y2="4.445" width="0.127" layer="21"/>
<wire x1="-7.45" y1="4.445" x2="-7.45" y2="10.16" width="0.127" layer="21"/>
<wire x1="8.43" y1="4.445" x2="7.45" y2="4.445" width="0.127" layer="21"/>
<wire x1="7.45" y1="4.445" x2="7.45" y2="10.16" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-10.4" x2="-7.62" y2="-5.4" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-5.4" x2="-5.715" y2="-5.4" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-5.4" x2="-5.715" y2="-10.4" width="0.127" layer="21"/>
<wire x1="5.715" y1="-10.4" x2="5.715" y2="-5.4" width="0.127" layer="21"/>
<wire x1="5.715" y1="-5.4" x2="7.62" y2="-5.4" width="0.127" layer="21"/>
<wire x1="7.62" y1="-5.4" x2="7.62" y2="-10.4" width="0.127" layer="21"/>
<wire x1="-2" y1="-10.4" x2="-2" y2="-9" width="0.127" layer="21"/>
<wire x1="-2" y1="-9" x2="2" y2="-9" width="0.127" layer="21"/>
<wire x1="2" y1="-9" x2="2" y2="-10.4" width="0.127" layer="21"/>
<pad name="SHIELD2" x="-7.747" y="3.048" drill="1.5748"/>
<pad name="SHIELD1" x="7.747" y="3.048" drill="1.5748"/>
<pad name="3" x="-1.905" y="6.35" drill="0.889"/>
<pad name="4" x="-0.635" y="8.89" drill="0.889"/>
<pad name="5" x="0.635" y="6.35" drill="0.889"/>
<pad name="6" x="1.905" y="8.89" drill="0.889"/>
<pad name="7" x="3.175" y="6.35" drill="0.889"/>
<pad name="2" x="-3.175" y="8.89" drill="0.889"/>
<pad name="1" x="-4.445" y="6.35" drill="0.889"/>
<pad name="8" x="4.445" y="8.89" drill="0.889"/>
<text x="10.16" y="-10.16" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<hole x="-5.715" y="0" drill="3.2004"/>
<hole x="5.715" y="0" drill="3.2004"/>
</package>
<package name="XTAL-12MHZ">
<wire x1="-6.35" y1="2.35" x2="-6.35" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.35" x2="6.35" y2="-2.35" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.35" x2="6.35" y2="2.35" width="0.127" layer="21"/>
<wire x1="6.35" y1="2.35" x2="-6.35" y2="2.35" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.905" x2="3.175" y2="-1.905" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.905" x2="-3.175" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.127" layer="21" curve="180" cap="flat"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.127" layer="21" curve="180" cap="flat"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-2.54" width="0.127" layer="39"/>
<wire x1="-7.62" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="39"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="2.54" width="0.127" layer="39"/>
<wire x1="7.62" y1="2.54" x2="-7.62" y2="2.54" width="0.127" layer="39"/>
<smd name="P$1" x="-4.75" y="0" dx="5.6" dy="2.1" layer="1"/>
<smd name="P$2" x="4.75" y="0" dx="5.6" dy="2.1" layer="1"/>
<text x="-6.35" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-2.54" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="XTAL-SMAL">
<wire x1="-2.5" y1="1.6" x2="2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.5" y1="1.6" x2="2.5" y2="-1.6" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1.6" x2="-2.5" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.6" x2="-2.5" y2="1.6" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="3.175" y2="-2.54" width="0.127" layer="39"/>
<wire x1="3.175" y1="-2.54" x2="3.175" y2="2.54" width="0.127" layer="39"/>
<wire x1="3.175" y1="2.54" x2="-3.175" y2="2.54" width="0.127" layer="39"/>
<wire x1="-3.175" y1="2.54" x2="-3.175" y2="-2.54" width="0.127" layer="39"/>
<smd name="4" x="-1.9" y="1.25" dx="1.6" dy="1.3" layer="1"/>
<smd name="3" x="1.9" y="1.25" dx="1.6" dy="1.3" layer="1"/>
<smd name="1" x="-1.9" y="-1.25" dx="1.6" dy="1.3" layer="1"/>
<smd name="2" x="1.9" y="-1.25" dx="1.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0" size="1.27" layer="21">FT</text>
<text x="-3.175" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="25" rot="R180">&gt;VALUE</text>
</package>
<package name="MINIUSB">
<wire x1="-3.45" y1="0" x2="3.45" y2="0" width="0.127" layer="21"/>
<wire x1="3.45" y1="0" x2="3.45" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3.45" y1="-1.6" x2="3.45" y2="-5.5" width="0.127" layer="21"/>
<wire x1="3.45" y1="-5.5" x2="3.45" y2="-7.1" width="0.127" layer="21"/>
<wire x1="3.45" y1="-7.1" x2="3.45" y2="-9.3" width="0.127" layer="21"/>
<wire x1="3.45" y1="-9.3" x2="-3.45" y2="-9.3" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-9.3" x2="-3.45" y2="-7.1" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-7.1" x2="-3.45" y2="-5.5" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-5.5" x2="-3.45" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-1.6" x2="-3.45" y2="0" width="0.127" layer="21"/>
<wire x1="-4.9" y1="0" x2="4.9" y2="0" width="0.127" layer="21"/>
<wire x1="4.9" y1="0" x2="4.9" y2="-1.6" width="0.127" layer="21"/>
<wire x1="4.9" y1="-1.6" x2="3.45" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-4.9" y1="0" x2="-4.9" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-4.9" y1="-1.6" x2="-3.45" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3.45" y1="-7.1" x2="4.9" y2="-7.1" width="0.127" layer="21"/>
<wire x1="4.9" y1="-7.1" x2="4.9" y2="-5.5" width="0.127" layer="21"/>
<wire x1="4.9" y1="-5.5" x2="3.45" y2="-5.5" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-7.1" x2="-4.9" y2="-7.1" width="0.127" layer="21"/>
<wire x1="-4.9" y1="-7.1" x2="-4.9" y2="-5.5" width="0.127" layer="21"/>
<wire x1="-4.9" y1="-5.5" x2="-3.45" y2="-5.5" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="-4.8" width="0.127" layer="41"/>
<wire x1="-3.81" y1="-4.8" x2="-2.54" y2="-4.8" width="0.127" layer="41"/>
<wire x1="-2.54" y1="-4.8" x2="-2.54" y2="-7.62" width="0.127" layer="41"/>
<wire x1="-2.54" y1="-7.62" x2="-3.81" y2="-7.62" width="0.127" layer="41"/>
<wire x1="-3.81" y1="-7.62" x2="-3.81" y2="-9.525" width="0.127" layer="41"/>
<wire x1="-3.81" y1="-9.525" x2="3.81" y2="-9.525" width="0.127" layer="41"/>
<wire x1="3.81" y1="-9.525" x2="3.81" y2="-7.62" width="0.127" layer="41"/>
<wire x1="3.81" y1="-7.62" x2="2.54" y2="-7.62" width="0.127" layer="41"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-4.8" width="0.127" layer="41"/>
<wire x1="2.54" y1="-4.8" x2="3.81" y2="-4.8" width="0.127" layer="41"/>
<wire x1="3.81" y1="-4.8" x2="3.81" y2="-2.54" width="0.127" layer="41"/>
<wire x1="3.81" y1="-2.54" x2="-3.81" y2="-2.54" width="0.127" layer="41"/>
<smd name="3" x="0" y="-0.8" dx="0.5" dy="1.6" layer="1"/>
<smd name="2" x="-0.8" y="-0.8" dx="0.5" dy="1.6" layer="1"/>
<smd name="1" x="-1.6" y="-0.8" dx="0.5" dy="1.6" layer="1"/>
<smd name="5" x="1.6" y="-0.8" dx="0.5" dy="1.6" layer="1"/>
<smd name="4" x="0.8" y="-0.8" dx="0.5" dy="1.6" layer="1"/>
<smd name="SHIELD1" x="-4.175" y="-0.8" dx="2.5" dy="2.2" layer="1"/>
<smd name="SHIELD4" x="4.175" y="-0.8" dx="2.5" dy="2.2" layer="1"/>
<smd name="SHIELD2" x="-4.175" y="-6.3" dx="2.5" dy="2.2" layer="1"/>
<smd name="SHIELD3" x="4.175" y="-6.3" dx="2.5" dy="2.2" layer="1"/>
<text x="6.35" y="-9.525" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<hole x="-2.2" y="-3.2" drill="1"/>
<hole x="2.2" y="-3.2" drill="1"/>
</package>
<package name="SOD80">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.483" x2="2.973" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.483" x2="-2.973" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.483" x2="-2.973" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="1.483" x2="2.973" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.7874" x2="-1.3208" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.7874" x2="-1.3208" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="0.5" y1="0.6" x2="-0.5" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.5" y1="-0.6" x2="0.5" y2="0.6" width="0.2032" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8636" x2="-1.2954" y2="0.8636" layer="51"/>
<rectangle x1="1.2954" y1="-0.8636" x2="1.8542" y2="0.8636" layer="51"/>
<rectangle x1="-0.9906" y1="-0.7874" x2="-0.381" y2="0.7874" layer="51"/>
<rectangle x1="-0.4001" y1="-0.5999" x2="0.4001" y2="0.5999" layer="35"/>
</package>
<package name="JUMPER">
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-0.635" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="-1.905" y2="0.635" width="0.127" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.8" diameter="1.4224" shape="octagon"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" diameter="1.4224" shape="octagon"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SOT353-1">
<wire x1="-1.125" y1="0.675" x2="-1.125" y2="-0.675" width="0.127" layer="21"/>
<wire x1="-1.125" y1="-0.675" x2="1.125" y2="-0.675" width="0.127" layer="21"/>
<wire x1="1.125" y1="-0.675" x2="1.125" y2="0.675" width="0.127" layer="21"/>
<wire x1="1.125" y1="0.675" x2="-1.125" y2="0.675" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="39"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="39"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.127" layer="39"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="39"/>
<smd name="1" x="-0.65" y="-0.975" dx="0.6" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="0" y="-0.975" dx="0.6" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="0.65" y="-0.975" dx="0.6" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="0.65" y="0.975" dx="0.6" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="-0.65" y="0.975" dx="0.6" dy="0.3" layer="1" rot="R90"/>
<text x="2.54" y="-1.27" size="1.27" layer="21" rot="R90">&gt;NAME</text>
</package>
<package name="CONNECTOR_JTAG_PCB">
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-0.635" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.175" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.905" x2="-3.175" y2="1.905" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-1.27" width="0.127" layer="39"/>
<wire x1="-3.81" y1="-1.27" x2="3.81" y2="-1.27" width="0.127" layer="39"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="2.54" width="0.127" layer="39"/>
<wire x1="3.81" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="39"/>
<circle x="-2.54" y="-1.27" radius="0.3" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.65" diameter="1" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.65" diameter="1" shape="octagon"/>
<pad name="3" x="0" y="0" drill="0.65" diameter="1" shape="octagon"/>
<pad name="4" x="1.27" y="0" drill="0.65" diameter="1" shape="octagon"/>
<pad name="5" x="2.54" y="0" drill="0.65" diameter="1" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="0.65" diameter="1" shape="octagon"/>
<pad name="7" x="1.27" y="1.27" drill="0.65" diameter="1" shape="octagon"/>
<pad name="8" x="0" y="1.27" drill="0.65" diameter="1" shape="octagon"/>
<pad name="9" x="-1.27" y="1.27" drill="0.65" diameter="1" shape="octagon"/>
<pad name="10" x="-2.54" y="1.27" drill="0.65" diameter="1" shape="octagon"/>
<text x="-3.81" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="3.81" y="-1.27" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="I-TRAFO">
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="12.7" x2="2.54" y2="20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="20.32" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="12.7" width="0.254" layer="94" curve="180"/>
<wire x1="12.7" y1="20.32" x2="12.7" y2="15.24" width="0.254" layer="94" curve="180"/>
<wire x1="12.7" y1="15.24" x2="12.7" y2="10.16" width="0.254" layer="94" curve="180"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="5.08" width="0.254" layer="94" curve="180"/>
<wire x1="12.7" y1="5.08" x2="12.7" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="7.62" y1="20.32" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="6.604" y1="20.32" x2="6.604" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="20.32" x2="17.78" y2="20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<pin name="L1" x="20.32" y="20.32" visible="off" length="middle" rot="R180"/>
<pin name="L2" x="20.32" y="0" visible="off" length="middle" rot="R180"/>
</symbol>
<symbol name="ADE7753">
<wire x1="-15.24" y1="30.48" x2="-15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-7.62" x2="20.32" y2="-7.62" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="20.32" y2="30.48" width="0.254" layer="94"/>
<wire x1="20.32" y1="30.48" x2="-15.24" y2="30.48" width="0.254" layer="94"/>
<text x="-15.24" y="31.496" size="1.778" layer="95">&gt;NAME</text>
<text x="20.32" y="-8.382" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="!RESET" x="7.62" y="35.56" length="middle" function="dot" rot="R270"/>
<pin name="DVDD" x="2.54" y="35.56" length="middle" rot="R270"/>
<pin name="AVDD" x="-2.54" y="35.56" length="middle" rot="R270"/>
<pin name="V1P" x="-20.32" y="22.86" length="middle"/>
<pin name="V1N" x="-20.32" y="17.78" length="middle"/>
<pin name="V2N" x="-20.32" y="10.16" length="middle"/>
<pin name="V2P" x="-20.32" y="12.7" length="middle"/>
<pin name="AGND" x="-2.54" y="-12.7" length="middle" rot="R90"/>
<pin name="REF_IN/OUT" x="-20.32" y="5.08" length="middle"/>
<pin name="DGND" x="7.62" y="-12.7" length="middle" rot="R90"/>
<pin name="CF" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="ZX" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="!SAG" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="!IRQ" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="CLKIN" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="CLKOUT" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="!CS" x="25.4" y="20.32" length="middle" rot="R180"/>
<pin name="SCLK" x="25.4" y="22.86" length="middle" rot="R180"/>
<pin name="DOUT" x="25.4" y="25.4" length="middle" rot="R180"/>
<pin name="DIN" x="25.4" y="27.94" length="middle" rot="R180"/>
</symbol>
<symbol name="TRACO-ACDC">
<wire x1="0" y1="0" x2="0" y2="17.78" width="0.254" layer="94"/>
<wire x1="0" y1="17.78" x2="27.94" y2="17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="17.78" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="18.796" size="1.27" layer="95">&gt;NAME</text>
<text x="27.94" y="-0.762" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<pin name="L" x="-5.08" y="12.7" length="middle"/>
<pin name="N" x="-5.08" y="5.08" length="middle"/>
<pin name="+VOUT" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="-VOUT" x="33.02" y="5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="TRACO-DCDC">
<wire x1="0" y1="17.78" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="2.54" x2="25.4" y2="17.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="17.78" x2="0" y2="17.78" width="0.254" layer="94"/>
<text x="0" y="18.542" size="1.27" layer="95">&gt;NAME</text>
<text x="25.4" y="2.032" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<pin name="IN+" x="-5.08" y="15.24" visible="pin" length="middle"/>
<pin name="IN-" x="-5.08" y="5.08" visible="pin" length="middle"/>
<pin name="OUT+" x="30.48" y="15.24" visible="pin" length="middle" rot="R180"/>
<pin name="OUT-" x="30.48" y="5.08" visible="pin" length="middle" rot="R180"/>
</symbol>
<symbol name="MINIUSB">
<wire x1="-5.08" y1="-12.7" x2="-5.08" y2="15.24" width="0.254" layer="94"/>
<wire x1="-5.08" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-5.08" y2="-12.7" width="0.254" layer="94"/>
<text x="-5.08" y="16.002" size="1.27" layer="95">&gt;NAME</text>
<text x="10.16" y="-13.462" size="1.27" layer="95" rot="R180">&gt;VALUE</text>
<pin name="VCC" x="-10.16" y="12.7" length="middle"/>
<pin name="D-" x="-10.16" y="10.16" length="middle"/>
<pin name="D+" x="-10.16" y="7.62" length="middle"/>
<pin name="GND" x="-10.16" y="5.08" length="middle"/>
<pin name="SHIELD1" x="-10.16" y="-2.54" visible="pin" length="middle"/>
<pin name="SHIELD2" x="-10.16" y="-5.08" visible="pin" length="middle"/>
<pin name="SHIELD3" x="-10.16" y="-7.62" visible="pin" length="middle"/>
<pin name="SHIELD4" x="-10.16" y="-10.16" visible="pin" length="middle"/>
</symbol>
<symbol name="XTAL">
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="3.048" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.318" size="1.27" layer="96">&gt;VALUE</text>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="FUSE">
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="1.778" y="0.762" size="1.27" layer="95">&gt;NAME</text>
<text x="1.778" y="-1.27" size="1.27" layer="96">&gt;VALUE</text>
<pin name="P$1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
</symbol>
<symbol name="L">
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="-2.54" x2="0" y2="-5.08" width="0.254" layer="94" curve="-180"/>
<text x="-1.016" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="2.286" y="4.064" size="1.778" layer="95" rot="R270">&gt;VALUE</text>
<pin name="P$1" x="0" y="-7.62" visible="off" length="short" rot="R90"/>
<pin name="P$2" x="0" y="7.62" visible="off" length="short" rot="R270"/>
</symbol>
<symbol name="JTAG">
<wire x1="-2.54" y1="0" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="15.24" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="16.002" size="1.27" layer="95">&gt;NAME</text>
<text x="15.494" y="-0.762" size="1.27" layer="95" rot="R180">&gt;VALUE</text>
<pin name="VDD" x="-7.62" y="12.7" length="middle"/>
<pin name="GND1" x="-7.62" y="10.16" length="middle"/>
<pin name="GND2" x="-7.62" y="7.62" length="middle"/>
<pin name="RTCK" x="-7.62" y="5.08" length="middle"/>
<pin name="GND3" x="-7.62" y="2.54" length="middle"/>
<pin name="TMS" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="TCK" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="TDO" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="TDI" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="!RST" x="20.32" y="2.54" length="middle" function="dot" rot="R180"/>
</symbol>
<symbol name="SDCARD">
<wire x1="17.78" y1="25.4" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="2.54" x2="-10.16" y2="25.4" width="0.254" layer="94"/>
<wire x1="-10.16" y1="25.4" x2="17.78" y2="25.4" width="0.254" layer="94"/>
<text x="-10.16" y="26.416" size="1.778" layer="95">&gt;NAME</text>
<text x="17.78" y="-1.016" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="DAT2" x="-15.24" y="22.86" visible="pin" length="middle"/>
<pin name="DAT3/CD/!CS" x="-15.24" y="20.32" visible="pin" length="middle"/>
<pin name="CMD/DI" x="-15.24" y="17.78" visible="pin" length="middle"/>
<pin name="VDD" x="-15.24" y="15.24" visible="pin" length="middle"/>
<pin name="CLK/SCLK" x="-15.24" y="12.7" visible="pin" length="middle"/>
<pin name="VSS" x="-15.24" y="10.16" visible="pin" length="middle"/>
<pin name="DAT0/DO" x="-15.24" y="7.62" visible="pin" length="middle"/>
<pin name="DAT1" x="-15.24" y="5.08" visible="pin" length="middle"/>
<pin name="GND4" x="22.86" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="CD" x="22.86" y="22.86" visible="pin" length="middle" rot="R180"/>
<pin name="CD_VSS" x="22.86" y="20.32" visible="pin" length="middle" rot="R180"/>
<pin name="GND1" x="22.86" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="GND2" x="22.86" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="GND3" x="22.86" y="7.62" visible="pin" length="middle" rot="R180"/>
</symbol>
<symbol name="STABILIZER">
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-7.62" y="3.302" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.62" y="-12.192" size="1.27" layer="95">&gt;VALUE</text>
<pin name="IN" x="-12.7" y="0" length="middle"/>
<pin name="GND1" x="2.54" y="-15.24" length="middle" rot="R90"/>
<pin name="GND2" x="5.08" y="-15.24" length="middle" rot="R90"/>
<pin name="OUT" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="EN" x="-12.7" y="-5.08" length="middle"/>
</symbol>
<symbol name="ADUM2402">
<wire x1="2.54" y1="1.27" x2="5.08" y2="1.27" width="0.127" layer="94"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="2.54" width="0.127" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="3.81" width="0.127" layer="94"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="5.08" width="0.127" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="6.35" width="0.127" layer="94"/>
<wire x1="5.08" y1="6.35" x2="2.54" y2="6.35" width="0.127" layer="94"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="2.54" width="0.127" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="3.81" width="0.127" layer="94"/>
<wire x1="2.54" y1="3.81" x2="5.08" y2="3.81" width="0.127" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="5.08" y2="-6.35" width="0.127" layer="94"/>
<wire x1="5.08" y1="-6.35" x2="5.08" y2="-5.08" width="0.127" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-3.81" width="0.127" layer="94"/>
<wire x1="5.08" y1="-3.81" x2="5.08" y2="-2.54" width="0.127" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-1.27" width="0.127" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="5.08" y2="-3.81" width="0.127" layer="94"/>
<wire x1="-3.556" y1="6.096" x2="-3.556" y2="5.08" width="0.127" layer="94"/>
<wire x1="-3.556" y1="5.08" x2="-3.556" y2="4.064" width="0.127" layer="94"/>
<wire x1="-3.556" y1="4.064" x2="-1.016" y2="5.08" width="0.127" layer="94"/>
<wire x1="-1.016" y1="5.08" x2="-3.556" y2="6.096" width="0.127" layer="94"/>
<wire x1="-3.556" y1="3.556" x2="-3.556" y2="2.54" width="0.127" layer="94"/>
<wire x1="-3.556" y1="2.54" x2="-3.556" y2="1.524" width="0.127" layer="94"/>
<wire x1="-3.556" y1="1.524" x2="-1.016" y2="2.54" width="0.127" layer="94"/>
<wire x1="-1.016" y1="2.54" x2="-3.556" y2="3.556" width="0.127" layer="94"/>
<wire x1="-1.016" y1="-6.096" x2="-1.016" y2="-5.08" width="0.127" layer="94"/>
<wire x1="-1.016" y1="-5.08" x2="-1.016" y2="-4.064" width="0.127" layer="94"/>
<wire x1="-1.016" y1="-4.064" x2="-3.556" y2="-5.08" width="0.127" layer="94"/>
<wire x1="-3.556" y1="-5.08" x2="-1.016" y2="-6.096" width="0.127" layer="94"/>
<wire x1="-1.016" y1="-3.556" x2="-1.016" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="-1.016" y2="-1.524" width="0.127" layer="94"/>
<wire x1="-1.016" y1="-1.524" x2="-3.556" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-3.556" y1="-2.54" x2="-1.016" y2="-3.556" width="0.127" layer="94"/>
<wire x1="9.398" y1="6.096" x2="9.398" y2="5.08" width="0.127" layer="94"/>
<wire x1="9.398" y1="5.08" x2="9.398" y2="4.064" width="0.127" layer="94"/>
<wire x1="9.398" y1="4.064" x2="11.938" y2="5.08" width="0.127" layer="94"/>
<wire x1="11.938" y1="5.08" x2="9.398" y2="6.096" width="0.127" layer="94"/>
<wire x1="9.398" y1="3.556" x2="9.398" y2="2.54" width="0.127" layer="94"/>
<wire x1="9.398" y1="2.54" x2="9.398" y2="1.524" width="0.127" layer="94"/>
<wire x1="9.398" y1="1.524" x2="11.938" y2="2.54" width="0.127" layer="94"/>
<wire x1="11.938" y1="2.54" x2="9.398" y2="3.556" width="0.127" layer="94"/>
<wire x1="11.938" y1="-1.524" x2="11.938" y2="-2.54" width="0.127" layer="94"/>
<wire x1="11.938" y1="-2.54" x2="11.938" y2="-3.556" width="0.127" layer="94"/>
<wire x1="11.938" y1="-3.556" x2="9.398" y2="-2.54" width="0.127" layer="94"/>
<wire x1="9.398" y1="-2.54" x2="11.938" y2="-1.524" width="0.127" layer="94"/>
<wire x1="11.938" y1="-4.064" x2="11.938" y2="-5.08" width="0.127" layer="94"/>
<wire x1="11.938" y1="-5.08" x2="11.938" y2="-6.096" width="0.127" layer="94"/>
<wire x1="11.938" y1="-6.096" x2="9.398" y2="-5.08" width="0.127" layer="94"/>
<wire x1="9.398" y1="-5.08" x2="11.938" y2="-4.064" width="0.127" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-3.556" y2="5.08" width="0.127" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-3.556" y2="2.54" width="0.127" layer="94"/>
<wire x1="-1.016" y1="5.08" x2="2.54" y2="5.08" width="0.127" layer="94"/>
<wire x1="-1.016" y1="2.54" x2="2.54" y2="2.54" width="0.127" layer="94"/>
<wire x1="5.08" y1="5.08" x2="9.398" y2="5.08" width="0.127" layer="94"/>
<wire x1="5.08" y1="2.54" x2="9.398" y2="2.54" width="0.127" layer="94"/>
<wire x1="11.938" y1="2.54" x2="15.24" y2="2.54" width="0.127" layer="94"/>
<wire x1="15.24" y1="5.08" x2="11.938" y2="5.08" width="0.127" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="9.398" y2="-2.54" width="0.127" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="9.652" y2="-5.08" width="0.127" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="11.938" y2="-5.08" width="0.127" layer="94"/>
<wire x1="15.24" y1="-2.54" x2="11.938" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-3.556" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-3.556" y2="-5.08" width="0.127" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-1.016" y2="-5.08" width="0.127" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-1.016" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-2.54" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="-5.588" width="0.127" layer="94"/>
<wire x1="-2.54" y1="-4.572" x2="-2.54" y2="-3.048" width="0.127" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="12.7" y2="-10.16" width="0.127" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="12.7" y2="0" width="0.127" layer="94"/>
<wire x1="12.7" y1="0" x2="11.176" y2="0" width="0.127" layer="94"/>
<wire x1="11.176" y1="0" x2="11.176" y2="2.286" width="0.127" layer="94"/>
<wire x1="11.176" y1="2.794" x2="11.176" y2="4.826" width="0.127" layer="94"/>
<wire x1="3.048" y1="5.842" x2="3.302" y2="5.842" width="0.127" layer="94"/>
<wire x1="3.302" y1="4.318" x2="3.048" y2="4.318" width="0.127" layer="94"/>
<wire x1="3.302" y1="5.842" x2="3.302" y2="5.334" width="0.127" layer="94" curve="-180"/>
<wire x1="3.302" y1="5.334" x2="3.302" y2="4.826" width="0.127" layer="94" curve="-180"/>
<wire x1="3.302" y1="4.826" x2="3.302" y2="4.318" width="0.127" layer="94" curve="-180"/>
<wire x1="4.572" y1="4.318" x2="4.318" y2="4.318" width="0.127" layer="94"/>
<wire x1="4.318" y1="5.842" x2="4.572" y2="5.842" width="0.127" layer="94"/>
<wire x1="4.318" y1="4.318" x2="4.318" y2="4.826" width="0.127" layer="94" curve="-180"/>
<wire x1="4.318" y1="4.826" x2="4.318" y2="5.334" width="0.127" layer="94" curve="-180"/>
<wire x1="4.318" y1="5.334" x2="4.318" y2="5.842" width="0.127" layer="94" curve="-180"/>
<wire x1="3.048" y1="3.302" x2="3.302" y2="3.302" width="0.127" layer="94"/>
<wire x1="3.302" y1="1.778" x2="3.048" y2="1.778" width="0.127" layer="94"/>
<wire x1="3.302" y1="3.302" x2="3.302" y2="2.794" width="0.127" layer="94" curve="-180"/>
<wire x1="3.302" y1="2.794" x2="3.302" y2="2.286" width="0.127" layer="94" curve="-180"/>
<wire x1="3.302" y1="2.286" x2="3.302" y2="1.778" width="0.127" layer="94" curve="-180"/>
<wire x1="4.572" y1="1.778" x2="4.318" y2="1.778" width="0.127" layer="94"/>
<wire x1="4.318" y1="3.302" x2="4.572" y2="3.302" width="0.127" layer="94"/>
<wire x1="4.318" y1="1.778" x2="4.318" y2="2.286" width="0.127" layer="94" curve="-180"/>
<wire x1="4.318" y1="2.286" x2="4.318" y2="2.794" width="0.127" layer="94" curve="-180"/>
<wire x1="4.318" y1="2.794" x2="4.318" y2="3.302" width="0.127" layer="94" curve="-180"/>
<wire x1="3.048" y1="-1.778" x2="3.302" y2="-1.778" width="0.127" layer="94"/>
<wire x1="3.302" y1="-3.302" x2="3.048" y2="-3.302" width="0.127" layer="94"/>
<wire x1="3.302" y1="-1.778" x2="3.302" y2="-2.286" width="0.127" layer="94" curve="-180"/>
<wire x1="3.302" y1="-2.286" x2="3.302" y2="-2.794" width="0.127" layer="94" curve="-180"/>
<wire x1="3.302" y1="-2.794" x2="3.302" y2="-3.302" width="0.127" layer="94" curve="-180"/>
<wire x1="4.572" y1="-3.302" x2="4.318" y2="-3.302" width="0.127" layer="94"/>
<wire x1="4.318" y1="-1.778" x2="4.572" y2="-1.778" width="0.127" layer="94"/>
<wire x1="4.318" y1="-3.302" x2="4.318" y2="-2.794" width="0.127" layer="94" curve="-180"/>
<wire x1="4.318" y1="-2.794" x2="4.318" y2="-2.286" width="0.127" layer="94" curve="-180"/>
<wire x1="4.318" y1="-2.286" x2="4.318" y2="-1.778" width="0.127" layer="94" curve="-180"/>
<wire x1="3.048" y1="-4.318" x2="3.302" y2="-4.318" width="0.127" layer="94"/>
<wire x1="3.302" y1="-5.842" x2="3.048" y2="-5.842" width="0.127" layer="94"/>
<wire x1="3.302" y1="-4.318" x2="3.302" y2="-4.826" width="0.127" layer="94" curve="-180"/>
<wire x1="3.302" y1="-4.826" x2="3.302" y2="-5.334" width="0.127" layer="94" curve="-180"/>
<wire x1="3.302" y1="-5.334" x2="3.302" y2="-5.842" width="0.127" layer="94" curve="-180"/>
<wire x1="4.572" y1="-5.842" x2="4.318" y2="-5.842" width="0.127" layer="94"/>
<wire x1="4.318" y1="-4.318" x2="4.572" y2="-4.318" width="0.127" layer="94"/>
<wire x1="4.318" y1="-5.842" x2="4.318" y2="-5.334" width="0.127" layer="94" curve="-180"/>
<wire x1="4.318" y1="-5.334" x2="4.318" y2="-4.826" width="0.127" layer="94" curve="-180"/>
<wire x1="4.318" y1="-4.826" x2="4.318" y2="-4.318" width="0.127" layer="94" curve="-180"/>
<wire x1="-3.302" y1="4.572" x2="-3.048" y2="4.572" width="0.127" layer="94"/>
<wire x1="-3.048" y1="4.572" x2="-2.794" y2="4.572" width="0.127" layer="94"/>
<wire x1="-2.794" y1="4.572" x2="-2.794" y2="5.334" width="0.127" layer="94"/>
<wire x1="-2.794" y1="5.334" x2="-3.048" y2="5.334" width="0.127" layer="94"/>
<wire x1="-3.048" y1="4.572" x2="-3.048" y2="5.334" width="0.127" layer="94"/>
<wire x1="-2.794" y1="5.334" x2="-2.54" y2="5.334" width="0.127" layer="94"/>
<wire x1="-3.302" y1="2.032" x2="-3.048" y2="2.032" width="0.127" layer="94"/>
<wire x1="-3.048" y1="2.032" x2="-2.794" y2="2.032" width="0.127" layer="94"/>
<wire x1="-2.794" y1="2.032" x2="-2.794" y2="2.794" width="0.127" layer="94"/>
<wire x1="-2.794" y1="2.794" x2="-3.048" y2="2.794" width="0.127" layer="94"/>
<wire x1="-3.048" y1="2.032" x2="-3.048" y2="2.794" width="0.127" layer="94"/>
<wire x1="-2.794" y1="2.794" x2="-2.54" y2="2.794" width="0.127" layer="94"/>
<wire x1="11.176" y1="-2.032" x2="11.43" y2="-2.032" width="0.127" layer="94"/>
<wire x1="11.684" y1="-2.032" x2="11.43" y2="-2.032" width="0.127" layer="94"/>
<wire x1="11.43" y1="-2.032" x2="11.43" y2="-2.794" width="0.127" layer="94"/>
<wire x1="11.43" y1="-2.794" x2="11.176" y2="-2.794" width="0.127" layer="94"/>
<wire x1="10.922" y1="-2.794" x2="11.176" y2="-2.794" width="0.127" layer="94"/>
<wire x1="11.176" y1="-2.794" x2="11.176" y2="-2.032" width="0.127" layer="94"/>
<wire x1="11.176" y1="-4.572" x2="11.176" y2="-5.334" width="0.127" layer="94"/>
<wire x1="11.176" y1="-5.334" x2="11.43" y2="-5.334" width="0.127" layer="94"/>
<wire x1="10.922" y1="-5.334" x2="11.176" y2="-5.334" width="0.127" layer="94"/>
<wire x1="11.43" y1="-5.334" x2="11.43" y2="-4.572" width="0.127" layer="94"/>
<wire x1="11.43" y1="-4.572" x2="11.176" y2="-4.572" width="0.127" layer="94"/>
<wire x1="11.43" y1="-4.572" x2="11.684" y2="-4.572" width="0.127" layer="94"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-20.32" x2="15.24" y2="-20.32" width="0.254" layer="94"/>
<wire x1="15.24" y1="-20.32" x2="15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="12.7" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<text x="-7.62" y="13.208" size="1.778" layer="95">&gt;NAME</text>
<text x="15.24" y="-21.082" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="VDD1" x="-12.7" y="10.16" length="middle"/>
<pin name="VIA" x="-12.7" y="5.08" visible="pad" length="middle"/>
<pin name="VIB" x="-12.7" y="2.54" visible="pad" length="middle"/>
<pin name="VOC" x="-12.7" y="-2.54" visible="pad" length="middle"/>
<pin name="VOD" x="-12.7" y="-5.08" visible="pad" length="middle"/>
<pin name="GND1_1" x="-12.7" y="-15.24" length="middle"/>
<pin name="GND1_2" x="-12.7" y="-17.78" length="middle"/>
<pin name="GND2_2" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="GND2_1" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="VID" x="20.32" y="-5.08" visible="pad" length="middle" rot="R180"/>
<pin name="VIC" x="20.32" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="VOB" x="20.32" y="2.54" visible="pad" length="middle" rot="R180"/>
<pin name="VOA" x="20.32" y="5.08" visible="pad" length="middle" rot="R180"/>
<pin name="VDD2" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="VE1" x="-12.7" y="-10.16" visible="pad" length="middle"/>
<pin name="VE2" x="20.32" y="-10.16" visible="pad" length="middle" rot="R180"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="CONECTOR/2">
<wire x1="0" y1="-0.508" x2="2.032" y2="-2.54" width="0.254" layer="94" curve="-90"/>
<wire x1="2.032" y1="-2.54" x2="0" y2="-4.572" width="0.254" layer="94" curve="-90"/>
<wire x1="2.54" y1="-2.54" x2="2.032" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="7.112" x2="2.032" y2="5.08" width="0.254" layer="94" curve="-90"/>
<wire x1="2.032" y1="5.08" x2="0" y2="3.048" width="0.254" layer="94" curve="-90"/>
<wire x1="2.54" y1="5.08" x2="2.032" y2="5.08" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="1.481" width="0.254" layer="94"/>
<circle x="0" y="5.08" radius="1.481" width="0.254" layer="94"/>
<text x="-2.54" y="0.762" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="-2.54" visible="off" length="point" rot="R180"/>
<pin name="2" x="2.54" y="5.08" visible="off" length="point" rot="R180"/>
</symbol>
<symbol name="LAN8720">
<wire x1="-20.32" y1="25.4" x2="20.32" y2="25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="25.4" x2="20.32" y2="-27.94" width="0.254" layer="94"/>
<wire x1="20.32" y1="-27.94" x2="-20.32" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-27.94" x2="-20.32" y2="25.4" width="0.254" layer="94"/>
<text x="-20.32" y="27.94" size="1.27" layer="95">&gt;NAME</text>
<text x="-20.32" y="-30.48" size="1.27" layer="96">&gt;VALUE</text>
<pin name="MDIO" x="-22.86" y="17.78" length="short"/>
<pin name="MDC" x="-22.86" y="15.24" length="short"/>
<pin name="NINT/REFCLKO" x="-22.86" y="12.7" length="short"/>
<pin name="TXD0" x="-22.86" y="7.62" length="short"/>
<pin name="TXD1" x="-22.86" y="5.08" length="short"/>
<pin name="TXEN" x="-22.86" y="2.54" length="short"/>
<pin name="RXD0" x="-22.86" y="0" length="short"/>
<pin name="RXD1" x="-22.86" y="-2.54" length="short"/>
<pin name="RXER" x="-22.86" y="-7.62" length="short"/>
<pin name="LED1/REGOFF" x="-22.86" y="-12.7" length="short"/>
<pin name="LED2/NINTSEL" x="-22.86" y="-15.24" length="short"/>
<pin name="NRST" x="-22.86" y="-20.32" length="short"/>
<pin name="TX+" x="22.86" y="17.78" length="short" rot="R180"/>
<pin name="TX-" x="22.86" y="12.7" length="short" rot="R180"/>
<pin name="RX+" x="22.86" y="-10.16" length="short" rot="R180"/>
<pin name="RX-" x="22.86" y="-15.24" length="short" rot="R180"/>
<pin name="XTAL1" x="-5.08" y="-30.48" length="short" rot="R90"/>
<pin name="XTAL2" x="5.08" y="-30.48" length="short" rot="R90"/>
<pin name="CRS_DV" x="-22.86" y="-5.08" length="short"/>
<pin name="RBIAS" x="12.7" y="-30.48" length="short" rot="R90"/>
<pin name="GND" x="-10.16" y="-30.48" length="short" rot="R90"/>
<pin name="VDD1A" x="-7.62" y="27.94" length="short" rot="R270"/>
<pin name="VDD2A" x="-2.54" y="27.94" length="short" rot="R270"/>
<pin name="VDDIO" x="2.54" y="27.94" length="short" rot="R270"/>
<pin name="VDDCR" x="7.62" y="27.94" length="short" rot="R270"/>
</symbol>
<symbol name="SWITCH">
<wire x1="-2.54" y1="1.524" x2="0" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="1.524" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="1.524" x2="0" y2="3.556" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.556" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-3.556" y1="-2.54" x2="-3.556" y2="0" width="0.127" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="3.302" y2="-2.54" width="0.127" layer="94"/>
<wire x1="3.302" y1="-2.54" x2="3.302" y2="0" width="0.127" layer="94"/>
<circle x="2.032" y="0" radius="0.508" width="0.254" layer="94"/>
<circle x="-2.032" y="0" radius="0.5679" width="0.254" layer="94"/>
<text x="-3.302" y="-2.286" size="1.27" layer="95">&gt;NAME</text>
<pin name="A_1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="B_1" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="B_2" x="5.08" y="-2.54" visible="off" length="point" rot="R180"/>
<pin name="A_2" x="-5.08" y="-2.54" visible="off" length="point"/>
</symbol>
<symbol name="TERMISTOR">
<wire x1="-1.016" y1="2.54" x2="-1.016" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="1.016" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.54" x2="-1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.778" x2="2.54" y2="1.778" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.778" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<text x="1.778" y="-0.762" size="1.27" layer="95">&gt;NAME</text>
<text x="1.778" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="P$1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
</symbol>
<symbol name="TRANSIL">
<wire x1="-1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.508" width="0.254" layer="94"/>
<text x="1.778" y="0.254" size="1.27" layer="95">&gt;NAME</text>
<text x="1.778" y="-1.524" size="1.27" layer="95">&gt;VALUE</text>
<pin name="P$1" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
</symbol>
<symbol name="DIODA-ZENER">
<wire x1="-1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="2.032" y="1.778" size="1.27" layer="95">&gt;NAME</text>
<text x="2.032" y="0" size="1.27" layer="95">&gt;VALUE</text>
<pin name="C" x="0" y="5.08" visible="off" length="point"/>
<pin name="A" x="0" y="-2.54" visible="off" length="point"/>
</symbol>
<symbol name="ELKO">
<wire x1="-1.524" y1="-0.889" x2="1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.889" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<text x="-0.5842" y="0.4064" size="1.27" layer="94" rot="R90">+</text>
<text x="2.032" y="-0.762" size="1.27" layer="95">&gt;NAME</text>
<text x="2.032" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.54" x2="1.651" y2="-1.651" layer="94"/>
<pin name="1+" x="0" y="2.54" visible="off" length="point" rot="R270"/>
<pin name="2-" x="0" y="-5.08" visible="off" length="point" rot="R90"/>
</symbol>
<symbol name="ETH-BEZLED">
<wire x1="17.78" y1="27.94" x2="17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="-5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="-5.08" y2="27.94" width="0.254" layer="94"/>
<wire x1="-5.08" y1="27.94" x2="17.78" y2="27.94" width="0.254" layer="94"/>
<text x="-4.826" y="28.702" size="1.27" layer="95">&gt;NAME</text>
<text x="17.78" y="-10.922" size="1.27" layer="95" rot="R180">&gt;VALUE</text>
<pin name="TD-" x="-10.16" y="20.32" length="middle"/>
<pin name="CT_TD" x="-10.16" y="22.86" length="middle"/>
<pin name="TD+" x="-10.16" y="25.4" length="middle"/>
<pin name="RD-" x="-10.16" y="7.62" length="middle"/>
<pin name="CT_RD" x="-10.16" y="10.16" length="middle"/>
<pin name="RD+" x="-10.16" y="12.7" length="middle"/>
<pin name="NC" x="-10.16" y="0" length="middle"/>
<pin name="GND_CHS" x="-10.16" y="-7.62" length="middle"/>
<pin name="SHIELD1" x="22.86" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="SHIELD2" x="22.86" y="-2.54" visible="pin" length="middle" rot="R180"/>
</symbol>
<symbol name="XTAL-4W">
<wire x1="0" y1="5.08" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="5.08" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="4.064" y2="0" width="0.254" layer="94"/>
<wire x1="4.064" y1="0" x2="4.064" y2="5.08" width="0.254" layer="94"/>
<wire x1="4.064" y1="5.08" x2="1.016" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="-2.54" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="7.62" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94" style="shortdash"/>
<text x="-2.54" y="8.382" size="1.27" layer="95">&gt;NAME</text>
<text x="3.556" y="-3.302" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<pin name="IN1" x="-5.08" y="2.54" visible="off" length="middle"/>
<pin name="IN2" x="10.16" y="2.54" visible="off" length="middle" rot="R180"/>
<pin name="GND" x="7.62" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="GND1" x="5.08" y="-5.08" visible="off" length="short" rot="R90"/>
</symbol>
<symbol name="JUMPER">
<wire x1="-2.54" y1="0" x2="-1.778" y2="0" width="0.127" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="2.54" width="0.127" layer="94"/>
<wire x1="2.54" y1="0" x2="1.778" y2="0" width="0.127" layer="94"/>
<wire x1="1.778" y1="0" x2="1.778" y2="2.54" width="0.127" layer="94"/>
<wire x1="-2.286" y1="1.778" x2="2.286" y2="1.778" width="0.127" layer="94"/>
<text x="-1.27" y="-1.524" size="1.27" layer="95">&gt;NAME</text>
<pin name="P$1" x="2.54" y="0" visible="off" length="point" rot="R180"/>
<pin name="P$2" x="-2.54" y="0" visible="off" length="point"/>
</symbol>
<symbol name="NOT">
<wire x1="-2.54" y1="4.064" x2="-2.54" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="-2.54" y2="4.064" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-3.556" size="1.27" layer="95">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="Y" x="7.62" y="0" visible="pad" length="short" function="dot" rot="R180"/>
<pin name="VCC" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="GND" x="0" y="-5.08" visible="pad" length="short" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADE7753" prefix="U">
<description>Analog Devices
ADE7753 - energy measurement</description>
<gates>
<gate name="G$1" symbol="ADE7753" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="20SSOP">
<connects>
<connect gate="G$1" pin="!CS" pad="17"/>
<connect gate="G$1" pin="!IRQ" pad="14"/>
<connect gate="G$1" pin="!RESET" pad="1"/>
<connect gate="G$1" pin="!SAG" pad="13"/>
<connect gate="G$1" pin="AGND" pad="8"/>
<connect gate="G$1" pin="AVDD" pad="3"/>
<connect gate="G$1" pin="CF" pad="11"/>
<connect gate="G$1" pin="CLKIN" pad="15"/>
<connect gate="G$1" pin="CLKOUT" pad="16"/>
<connect gate="G$1" pin="DGND" pad="10"/>
<connect gate="G$1" pin="DIN" pad="20"/>
<connect gate="G$1" pin="DOUT" pad="19"/>
<connect gate="G$1" pin="DVDD" pad="2"/>
<connect gate="G$1" pin="REF_IN/OUT" pad="9"/>
<connect gate="G$1" pin="SCLK" pad="18"/>
<connect gate="G$1" pin="V1N" pad="5"/>
<connect gate="G$1" pin="V1P" pad="4"/>
<connect gate="G$1" pin="V2N" pad="6"/>
<connect gate="G$1" pin="V2P" pad="7"/>
<connect gate="G$1" pin="ZX" pad="12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BD33KA5WF-E2" prefix="I">
<description>Stabilizátor 3.3V/500mA
http://www.farnell.com/datasheets/614208.pdf</description>
<gates>
<gate name="G$1" symbol="STABILIZER" x="5.08" y="5.08"/>
</gates>
<devices>
<device name="" package="SOP08">
<connects>
<connect gate="G$1" pin="EN" pad="5"/>
<connect gate="G$1" pin="GND1" pad="6"/>
<connect gate="G$1" pin="GND2" pad="7"/>
<connect gate="G$1" pin="IN" pad="8"/>
<connect gate="G$1" pin="OUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MEV3SV0505SC" prefix="TRACO">
<gates>
<gate name="G$1" symbol="TRACO-DCDC" x="-2.54" y="-5.08"/>
</gates>
<devices>
<device name="" package="MEV3SV0505SC">
<connects>
<connect gate="G$1" pin="IN+" pad="1"/>
<connect gate="G$1" pin="IN-" pad="2"/>
<connect gate="G$1" pin="OUT+" pad="7"/>
<connect gate="G$1" pin="OUT-" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADUM2402" prefix="U">
<gates>
<gate name="G$1" symbol="ADUM2402" x="5.08" y="15.24"/>
</gates>
<devices>
<device name="NARROW" package="SOIC16">
<connects>
<connect gate="G$1" pin="GND1_1" pad="2"/>
<connect gate="G$1" pin="GND1_2" pad="8"/>
<connect gate="G$1" pin="GND2_1" pad="15"/>
<connect gate="G$1" pin="GND2_2" pad="9"/>
<connect gate="G$1" pin="VDD1" pad="1"/>
<connect gate="G$1" pin="VDD2" pad="16"/>
<connect gate="G$1" pin="VE1" pad="7"/>
<connect gate="G$1" pin="VE2" pad="10"/>
<connect gate="G$1" pin="VIA" pad="3"/>
<connect gate="G$1" pin="VIB" pad="4"/>
<connect gate="G$1" pin="VIC" pad="12"/>
<connect gate="G$1" pin="VID" pad="11"/>
<connect gate="G$1" pin="VOA" pad="14"/>
<connect gate="G$1" pin="VOB" pad="13"/>
<connect gate="G$1" pin="VOC" pad="5"/>
<connect gate="G$1" pin="VOD" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WIDE" package="SOIC16-WIDE">
<connects>
<connect gate="G$1" pin="GND1_1" pad="2"/>
<connect gate="G$1" pin="GND1_2" pad="8"/>
<connect gate="G$1" pin="GND2_1" pad="15"/>
<connect gate="G$1" pin="GND2_2" pad="9"/>
<connect gate="G$1" pin="VDD1" pad="1"/>
<connect gate="G$1" pin="VDD2" pad="16"/>
<connect gate="G$1" pin="VE1" pad="7"/>
<connect gate="G$1" pin="VE2" pad="10"/>
<connect gate="G$1" pin="VIA" pad="3"/>
<connect gate="G$1" pin="VIB" pad="4"/>
<connect gate="G$1" pin="VIC" pad="12"/>
<connect gate="G$1" pin="VID" pad="11"/>
<connect gate="G$1" pin="VOA" pad="14"/>
<connect gate="G$1" pin="VOB" pad="13"/>
<connect gate="G$1" pin="VOC" pad="5"/>
<connect gate="G$1" pin="VOD" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="I-TRAFO" prefix="L">
<description>Proudovy transformator</description>
<gates>
<gate name="G$1" symbol="I-TRAFO" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="2PORT">
<connects>
<connect gate="G$1" pin="L1" pad="P$1"/>
<connect gate="G$1" pin="L2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TRAFO" package="TRAFO23">
<connects>
<connect gate="G$1" pin="L1" pad="P$1"/>
<connect gate="G$1" pin="L2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AKR103/2" prefix="J">
<gates>
<gate name="G$1" symbol="CONECTOR/2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AKR103/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="C0805">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="R0603">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LAN8720" prefix="U" uservalue="yes">
<description>&lt;b&gt;LAN8720A 10/100 Ethernet PHY&lt;/b&gt;
&lt;p&gt;Mouser: 886-LAN8720AI-CP&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LAN8720" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN24_4MM">
<connects>
<connect gate="G$1" pin="CRS_DV" pad="11"/>
<connect gate="G$1" pin="GND" pad="THERM"/>
<connect gate="G$1" pin="LED1/REGOFF" pad="3"/>
<connect gate="G$1" pin="LED2/NINTSEL" pad="2"/>
<connect gate="G$1" pin="MDC" pad="13"/>
<connect gate="G$1" pin="MDIO" pad="12"/>
<connect gate="G$1" pin="NINT/REFCLKO" pad="14"/>
<connect gate="G$1" pin="NRST" pad="15"/>
<connect gate="G$1" pin="RBIAS" pad="24"/>
<connect gate="G$1" pin="RX+" pad="23"/>
<connect gate="G$1" pin="RX-" pad="22"/>
<connect gate="G$1" pin="RXD0" pad="8"/>
<connect gate="G$1" pin="RXD1" pad="7"/>
<connect gate="G$1" pin="RXER" pad="10"/>
<connect gate="G$1" pin="TX+" pad="21"/>
<connect gate="G$1" pin="TX-" pad="20"/>
<connect gate="G$1" pin="TXD0" pad="17"/>
<connect gate="G$1" pin="TXD1" pad="18"/>
<connect gate="G$1" pin="TXEN" pad="16"/>
<connect gate="G$1" pin="VDD1A" pad="19"/>
<connect gate="G$1" pin="VDD2A" pad="1"/>
<connect gate="G$1" pin="VDDCR" pad="6"/>
<connect gate="G$1" pin="VDDIO" pad="9"/>
<connect gate="G$1" pin="XTAL1" pad="5"/>
<connect gate="G$1" pin="XTAL2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TERMISTOR" prefix="R">
<gates>
<gate name="G$1" symbol="TERMISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="MF-R005" package="MF-R005">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSIL" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="TRANSIL" x="0" y="0"/>
</gates>
<devices>
<device name="SMP100LC" package="TRANSIL-SMP100LC">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRACO_TMLM" prefix="TRACO">
<gates>
<gate name="G$1" symbol="TRACO-ACDC" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="TRACO-TMTL04">
<connects>
<connect gate="G$1" pin="+VOUT" pad="2"/>
<connect gate="G$1" pin="-VOUT" pad="3"/>
<connect gate="G$1" pin="L" pad="5"/>
<connect gate="G$1" pin="N" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCH" prefix="J">
<description>http://industrial.panasonic.com/www-data/pdf/ATK0000/ATK0000CE28.pdf</description>
<gates>
<gate name="G$1" symbol="SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SWITCH-EVQP0">
<connects>
<connect gate="G$1" pin="A_1" pad="A"/>
<connect gate="G$1" pin="A_2" pad="A1"/>
<connect gate="G$1" pin="B_1" pad="B"/>
<connect gate="G$1" pin="B_2" pad="B1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICROSD" prefix="J" uservalue="yes">
<description>http://www.dtt8.com/images/micro-sd%20specification.pdf
http://www.sandisk.com/Assets/File/OEM/Manuals/SD_SDIO_specsv1.pdf</description>
<gates>
<gate name="G$1" symbol="SDCARD" x="10.16" y="-2.54"/>
</gates>
<devices>
<device name="" package="SDCON">
<connects>
<connect gate="G$1" pin="CD" pad="CD"/>
<connect gate="G$1" pin="CD_VSS" pad="CD_GND"/>
<connect gate="G$1" pin="CLK/SCLK" pad="PIN5"/>
<connect gate="G$1" pin="CMD/DI" pad="PIN3"/>
<connect gate="G$1" pin="DAT0/DO" pad="PIN7"/>
<connect gate="G$1" pin="DAT1" pad="PIN1"/>
<connect gate="G$1" pin="DAT2" pad="PIN8"/>
<connect gate="G$1" pin="DAT3/CD/!CS" pad="PIN2"/>
<connect gate="G$1" pin="GND1" pad="SHELL1"/>
<connect gate="G$1" pin="GND2" pad="SHELL2"/>
<connect gate="G$1" pin="GND3" pad="SHELL3"/>
<connect gate="G$1" pin="GND4" pad="SHELL4"/>
<connect gate="G$1" pin="VDD" pad="PIN4"/>
<connect gate="G$1" pin="VSS" pad="PIN6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="C0805" package="D0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CPOL" prefix="C">
<description>&lt;b&gt;ELektrolyt Capacitor&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="ELKO" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="C1206">
<connects>
<connect gate="G$1" pin="1+" pad="2+"/>
<connect gate="G$1" pin="2-" pad="1-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2917" package="C2917">
<connects>
<connect gate="G$1" pin="1+" pad="+"/>
<connect gate="G$1" pin="2-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RJ45-BEZLED" prefix="J">
<gates>
<gate name="G$1" symbol="ETH-BEZLED" x="2.54" y="7.62"/>
</gates>
<devices>
<device name="" package="RJ45-JACK-BEZLED">
<connects>
<connect gate="G$1" pin="CT_RD" pad="3"/>
<connect gate="G$1" pin="CT_TD" pad="4"/>
<connect gate="G$1" pin="GND_CHS" pad="8"/>
<connect gate="G$1" pin="NC" pad="7"/>
<connect gate="G$1" pin="RD+" pad="1"/>
<connect gate="G$1" pin="RD-" pad="2"/>
<connect gate="G$1" pin="SHIELD1" pad="SHIELD1"/>
<connect gate="G$1" pin="SHIELD2" pad="SHIELD2"/>
<connect gate="G$1" pin="TD+" pad="5"/>
<connect gate="G$1" pin="TD-" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XTAL" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="XTAL" x="2.54" y="0"/>
</gates>
<devices>
<device name="LEZATY" package="XTAL-LEZATY">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VELKY" package="XTAL-12MHZ">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XTAL4W" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="XTAL-4W" x="0" y="0"/>
</gates>
<devices>
<device name="" package="XTAL-SMAL">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="GND1" pad="4"/>
<connect gate="G$1" pin="IN1" pad="1"/>
<connect gate="G$1" pin="IN2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MINIUSBB" prefix="J" uservalue="yes">
<description>MiniUSB type B
http://pinouts.ru/Slots/USB_pinout.shtml
http://www.hirose.co.jp/cataloge_hp/e24000019.pdf</description>
<gates>
<gate name="G$1" symbol="MINIUSB" x="2.54" y="10.16"/>
</gates>
<devices>
<device name="" package="MINIUSB">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="SHIELD1" pad="SHIELD1"/>
<connect gate="G$1" pin="SHIELD2" pad="SHIELD2"/>
<connect gate="G$1" pin="SHIELD3" pad="SHIELD3"/>
<connect gate="G$1" pin="SHIELD4" pad="SHIELD4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODA-ZENER" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODA-ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD80" package="SOD80">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JUMPER" prefix="J">
<gates>
<gate name="G$1" symbol="JUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JUMPER">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="74AHC_AHCT1G04" prefix="I">
<description>Invertor
http://www.nxp.com/documents/data_sheet/74AHC_AHCT1G04.pdf</description>
<gates>
<gate name="G$1" symbol="NOT" x="2.54" y="2.54"/>
</gates>
<devices>
<device name="" package="SOT353-1">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="Y" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JTAG" prefix="J">
<gates>
<gate name="G$1" symbol="JTAG" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="CONECTOR_JTAG">
<connects>
<connect gate="G$1" pin="!RST" pad="10"/>
<connect gate="G$1" pin="GND1" pad="3"/>
<connect gate="G$1" pin="GND2" pad="5"/>
<connect gate="G$1" pin="GND3" pad="9"/>
<connect gate="G$1" pin="RTCK" pad="7"/>
<connect gate="G$1" pin="TCK" pad="4"/>
<connect gate="G$1" pin="TDI" pad="8"/>
<connect gate="G$1" pin="TDO" pad="6"/>
<connect gate="G$1" pin="TMS" pad="2"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DRILL_HOLE" package="CONNECTOR_JTAG_PCB">
<connects>
<connect gate="G$1" pin="!RST" pad="6"/>
<connect gate="G$1" pin="GND1" pad="2"/>
<connect gate="G$1" pin="GND2" pad="3"/>
<connect gate="G$1" pin="GND3" pad="5"/>
<connect gate="G$1" pin="RTCK" pad="4"/>
<connect gate="G$1" pin="TCK" pad="9"/>
<connect gate="G$1" pin="TDI" pad="7"/>
<connect gate="G$1" pin="TDO" pad="8"/>
<connect gate="G$1" pin="TMS" pad="10"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FUSE-LITTLEFUSE">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BOURNS" package="FUSE-BOURNS">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TH" package="0207/7">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<packages>
</packages>
<symbols>
<symbol name="VDD">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="AGND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-2.667" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="AGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+4.1V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+4.1V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+05V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VDD" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AGND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="AGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+4.1V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="P" symbol="+4.1V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1005">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="21"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="21"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.27" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="21"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="21"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.651" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.159" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.159" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="21"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.286" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.286" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.1524" layer="21"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.667" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.667" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="21"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-2.794" y="1.778" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-3.048" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="21"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="21"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.524" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.524" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="21"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="21"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.397" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9" y1="1.245" x2="0.9" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-0.875" y1="-1.245" x2="0.925" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.159" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.159" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="21"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.286" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.286" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.1524" layer="21"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.794" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="21"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-2.921" y="1.778" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.921" y="-3.048" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.016" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.6858" y1="0.762" x2="-0.6858" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="-0.762" x2="-0.6858" y2="-0.762" width="0.1524" layer="21"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.016" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="1.651" y1="1.1684" x2="-1.6764" y2="1.1684" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.1684" x2="-1.651" y2="-1.1684" width="0.1524" layer="21"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-2.794" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.6858" y1="0.762" x2="-0.6858" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="-0.762" x2="-0.6858" y2="-0.762" width="0.1524" layer="21"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="1.651" y1="1.1684" x2="-1.6764" y2="1.1684" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.1684" x2="-1.651" y2="-1.1684" width="0.1524" layer="21"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-2.794" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.032" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.54" y="-0.635" size="1.27" layer="21">+</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1005" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-small-signal">
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="P-MOS">
<wire x1="0" y1="0" x2="-1.016" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.381" x2="-1.016" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-0.381" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.381" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.27" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-3.048" x2="1.27" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="1.778" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0.762" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="-0.889" y2="-0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-0.127" x2="-0.889" y2="0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="0.127" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.635" x2="1.524" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.635" x2="1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.016" y2="-0.635" width="0.254" layer="94"/>
<circle x="0" y="2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-3.048" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BSS84" prefix="Q">
<description>&lt;b&gt;P-CHANNEL MOS FET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="P-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U5" library="lpc1xxx-v6" deviceset="LPC1768" device=""/>
<part name="U1" library="elektromer" deviceset="LAN8720" device="" value="LAN 8720"/>
<part name="U2" library="elektromer" deviceset="ADE7753" device=""/>
<part name="SUPPLY1" library="supply2" deviceset="VDD" device="" value="AVDD"/>
<part name="TRACO1" library="elektromer" deviceset="TRACO_TMLM" device="" value="TMLM 04105"/>
<part name="L1" library="elektromer" deviceset="I-TRAFO" device="TRAFO"/>
<part name="R1" library="resistor" deviceset="R-EU_" device="R0603" value="10R"/>
<part name="SUPPLY4" library="supply2" deviceset="AGND" device=""/>
<part name="SUPPLY5" library="supply2" deviceset="AGND" device=""/>
<part name="R7" library="resistor" deviceset="R-EU_" device="R0603" value="1K"/>
<part name="R8" library="resistor" deviceset="R-EU_" device="R0603" value="1K"/>
<part name="SUPPLY6" library="supply2" deviceset="AGND" device=""/>
<part name="SUPPLY7" library="supply2" deviceset="AGND" device=""/>
<part name="R5" library="resistor" deviceset="R-EU_" device="R0603" value="1k"/>
<part name="R6" library="resistor" deviceset="R-EU_" device="R0603" value="1k"/>
<part name="C3" library="resistor" deviceset="C-EU" device="C0603K" value="33n"/>
<part name="C4" library="resistor" deviceset="C-EU" device="C0603K" value="33n"/>
<part name="C5" library="resistor" deviceset="C-EU" device="C0603K" value="33n"/>
<part name="C6" library="resistor" deviceset="C-EU" device="C0603K" value="33n"/>
<part name="C7" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C8" library="elektromer" deviceset="CPOL" device="1206" value="10u"/>
<part name="C9" library="elektromer" deviceset="CPOL" device="1206" value="10u"/>
<part name="C10" library="elektromer" deviceset="CPOL" device="1206" value="10u"/>
<part name="C11" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C12" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="SUPPLY8" library="supply2" deviceset="AGND" device=""/>
<part name="SUPPLY10" library="supply2" deviceset="AGND" device=""/>
<part name="SUPPLY11" library="supply2" deviceset="AGND" device=""/>
<part name="SUPPLY13" library="supply2" deviceset="AGND" device=""/>
<part name="SUPPLY14" library="supply2" deviceset="AGND" device=""/>
<part name="Q2" library="elektromer" deviceset="XTAL" device="VELKY" value="25MHz"/>
<part name="R9" library="resistor" deviceset="R-EU_" device="R0603" value="1M 1%"/>
<part name="C13" library="resistor" deviceset="C-EU" device="C0603" value="22p"/>
<part name="C14" library="resistor" deviceset="C-EU" device="C0603" value="22p"/>
<part name="C21" library="resistor" deviceset="C-EU" device="C0603K" value="22p"/>
<part name="C22" library="resistor" deviceset="C-EU" device="C0603K" value="22p"/>
<part name="TRACO2" library="elektromer" deviceset="MEV3SV0505SC" device="" value="MEV3S0505SC"/>
<part name="SUPPLY15" library="supply2" deviceset="AGND" device=""/>
<part name="SUPPLY16" library="supply2" deviceset="VDD" device="" value="AVDD"/>
<part name="T1" library="transistor-small-signal" deviceset="BSS84" device=""/>
<part name="J2" library="elektromer" deviceset="MINIUSBB" device="" value="MiniUSB B"/>
<part name="R10" library="resistor" deviceset="R-EU_" device="R0603" value="33R"/>
<part name="R11" library="resistor" deviceset="R-EU_" device="R0603" value="33R"/>
<part name="SUPPLY17" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY19" library="supply2" deviceset="+5V" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY21" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="R12" library="resistor" deviceset="R-EU_" device="R0603" value="1k5"/>
<part name="R13" library="resistor" deviceset="R-EU_" device="R0603" value="820R"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="D2" library="elektromer" deviceset="LED" device="C0805" value=""/>
<part name="R14" library="resistor" deviceset="R-EU_" device="R0603" value="15k"/>
<part name="Q3" library="elektromer" deviceset="XTAL" device="LEZATY" value="32.768kHz"/>
<part name="SUPPLY23" library="supply2" deviceset="AGND" device=""/>
<part name="FUSE1" library="elektromer" deviceset="FUSE" device="TH" value="1.25A"/>
<part name="C15" library="resistor" deviceset="C-EU" device="C0603K" value="1u"/>
<part name="C16" library="resistor" deviceset="C-EU" device="C0603K" value="12p"/>
<part name="C17" library="resistor" deviceset="C-EU" device="C0603K" value="12p"/>
<part name="C18" library="resistor" deviceset="C-EU" device="C0603K" value="12p"/>
<part name="C19" library="resistor" deviceset="C-EU" device="C0603K" value="12p"/>
<part name="C20" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="R15" library="resistor" deviceset="R-EU_" device="R0603" value="49R 1%"/>
<part name="R16" library="resistor" deviceset="R-EU_" device="R0603" value="49R 1%"/>
<part name="R17" library="resistor" deviceset="R-EU_" device="R0603" value="49R 1%"/>
<part name="R18" library="resistor" deviceset="R-EU_" device="R0603" value="49R 1%"/>
<part name="R19" library="resistor" deviceset="R-EU_" device="R0603" value="12k1 1%"/>
<part name="R21" library="resistor" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="R22" library="resistor" deviceset="R-EU_" device="R0603" value="10k"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY25" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="L2" library="elektromer" deviceset="L" device="0805" value="70R 3.5A"/>
<part name="L3" library="elektromer" deviceset="L" device="0805" value="70R 3.5A"/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY26" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="L4" library="elektromer" deviceset="L" device="0805" value="70R 3.5A"/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="L5" library="elektromer" deviceset="L" device="0805" value="70R 3.5A"/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="C23" library="resistor" deviceset="C-EU" device="C0603" value="22p"/>
<part name="C24" library="resistor" deviceset="C-EU" device="C0603" value="22p"/>
<part name="C25" library="resistor" deviceset="C-EU" device="C0603K" value="33p"/>
<part name="C26" library="resistor" deviceset="C-EU" device="C0603K" value="33p"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="RESET" library="elektromer" deviceset="SWITCH" device=""/>
<part name="SUPPLY22" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="R23" library="resistor" deviceset="R-EU_" device="R0603" value="15k"/>
<part name="C27" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="J4" library="elektromer" deviceset="JTAG" device="DRILL_HOLE" value="JTAG"/>
<part name="SUPPLY27" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="J1" library="elektromer" deviceset="MICROSD" device="" value="SDCARD"/>
<part name="C1" library="resistor" deviceset="C-EU" device="C0603K" value="1u"/>
<part name="C2" library="resistor" deviceset="C-EU" device="C0603K" value="1u"/>
<part name="U4" library="elektromer" deviceset="BD33KA5WF-E2" device=""/>
<part name="SUPPLY9" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="C28" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C29" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C30" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C31" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C32" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C33" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C34" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C35" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C36" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C37" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="SUPPLY12" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="U3" library="elektromer" deviceset="ADUM2402" device="WIDE" value="ADUM2402"/>
<part name="SUPPLY18" library="supply2" deviceset="AGND" device=""/>
<part name="SUPPLY28" library="supply2" deviceset="VDD" device="" value="AVDD"/>
<part name="SUPPLY29" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="C38" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C39" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="SUPPLY30" library="supply2" deviceset="AGND" device=""/>
<part name="J5" library="elektromer" deviceset="AKR103/2" device=""/>
<part name="C40" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="SUPPLY2" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="C41" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="C44" library="elektromer" deviceset="CPOL" device="1206" value="33u"/>
<part name="C43" library="elektromer" deviceset="CPOL" device="2917" value="120u"/>
<part name="R2" library="resistor" deviceset="R-EU_" device="0309/10" value="330k"/>
<part name="R3" library="resistor" deviceset="R-EU_" device="0309/10" value="330k"/>
<part name="R4" library="resistor" deviceset="R-EU_" device="0309/10" value="330k"/>
<part name="L6" library="elektromer" deviceset="L" device="0805" value="70R 3.5A"/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="C42" library="resistor" deviceset="C-EU" device="C0603K" value="100n"/>
<part name="D3" library="elektromer" deviceset="DIODA-ZENER" device="SOD80" value="5V6"/>
<part name="D4" library="elektromer" deviceset="LED" device="C0805" value=""/>
<part name="D5" library="elektromer" deviceset="LED" device="C0805" value=""/>
<part name="D6" library="elektromer" deviceset="LED" device="C0805" value=""/>
<part name="R24" library="resistor" deviceset="R-EU_" device="R0603" value="820R"/>
<part name="R25" library="resistor" deviceset="R-EU_" device="R0603" value="820R"/>
<part name="R26" library="resistor" deviceset="R-EU_" device="R0603" value="820R"/>
<part name="SUPPLY3" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="R20" library="elektromer" deviceset="TERMISTOR" device="MF-R005" value="MF-R005"/>
<part name="D1" library="elektromer" deviceset="TRANSIL" device="SMP100LC" value="SMP100LC-400"/>
<part name="Q1" library="elektromer" deviceset="XTAL" device="VELKY" value="3.58MHz"/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="J3" library="elektromer" deviceset="RJ45-BEZLED" device=""/>
<part name="Q4" library="elektromer" deviceset="XTAL4W" device="" value="12MHz"/>
<part name="J6" library="elektromer" deviceset="JUMPER" device=""/>
<part name="U6" library="elektromer" deviceset="74AHC_AHCT1G04" device="" value="74LVC1G04GW"/>
<part name="SUPPLY20" library="supply2" deviceset="+4.1V" device="" value="3.3V"/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-17.272" y="206.502" size="1.778" layer="95">230 VAC</text>
<text x="58.42" y="205.74" size="1.778" layer="95">5 VDC</text>
<text x="434.34" y="177.8" size="1.778" layer="97" rot="R90">2 mA/1.8 V</text>
</plain>
<instances>
<instance part="U5" gate="G$1" x="294.64" y="289.56"/>
<instance part="U1" gate="G$1" x="441.96" y="317.5"/>
<instance part="U2" gate="G$1" x="83.82" y="266.7"/>
<instance part="SUPPLY1" gate="G$1" x="86.36" y="327.66"/>
<instance part="TRACO1" gate="G$1" x="17.78" y="198.12"/>
<instance part="L1" gate="G$1" x="2.54" y="294.64"/>
<instance part="R1" gate="G$1" x="25.4" y="304.8" rot="R90"/>
<instance part="SUPPLY4" gate="G$1" x="86.36" y="246.38"/>
<instance part="SUPPLY5" gate="G$1" x="7.62" y="198.12"/>
<instance part="R7" gate="G$1" x="22.86" y="269.24" rot="R90"/>
<instance part="R8" gate="G$1" x="38.1" y="269.24" rot="R90"/>
<instance part="SUPPLY6" gate="G$1" x="25.4" y="259.08"/>
<instance part="SUPPLY7" gate="G$1" x="25.4" y="292.1"/>
<instance part="R5" gate="G$1" x="38.1" y="314.96"/>
<instance part="R6" gate="G$1" x="38.1" y="294.64"/>
<instance part="C3" gate="G$1" x="48.26" y="309.88"/>
<instance part="C4" gate="G$1" x="48.26" y="292.1"/>
<instance part="C5" gate="G$1" x="43.18" y="269.24"/>
<instance part="C6" gate="G$1" x="27.94" y="269.24"/>
<instance part="C7" gate="G$1" x="60.96" y="259.08"/>
<instance part="C8" gate="G$1" x="53.34" y="259.08"/>
<instance part="C9" gate="G$1" x="99.06" y="309.88"/>
<instance part="C10" gate="G$1" x="127" y="309.88"/>
<instance part="C11" gate="G$1" x="106.68" y="309.88"/>
<instance part="C12" gate="G$1" x="116.84" y="309.88"/>
<instance part="SUPPLY8" gate="G$1" x="40.64" y="259.08"/>
<instance part="SUPPLY10" gate="G$1" x="48.26" y="284.48"/>
<instance part="SUPPLY11" gate="G$1" x="48.26" y="302.26"/>
<instance part="SUPPLY13" gate="G$1" x="127" y="299.72"/>
<instance part="SUPPLY14" gate="G$1" x="53.34" y="246.38"/>
<instance part="Q2" gate="G$1" x="441.96" y="274.32"/>
<instance part="R9" gate="G$1" x="441.96" y="264.16"/>
<instance part="C13" gate="G$1" x="436.88" y="256.54"/>
<instance part="C14" gate="G$1" x="447.04" y="256.54"/>
<instance part="C21" gate="G$1" x="116.84" y="269.24"/>
<instance part="C22" gate="G$1" x="134.62" y="269.24"/>
<instance part="TRACO2" gate="G$1" x="73.66" y="213.36"/>
<instance part="SUPPLY15" gate="G$1" x="114.3" y="215.9"/>
<instance part="SUPPLY16" gate="G$1" x="114.3" y="231.14"/>
<instance part="T1" gate="G$1" x="454.66" y="195.58" rot="R180"/>
<instance part="J2" gate="G$1" x="497.84" y="185.42"/>
<instance part="R10" gate="G$1" x="467.36" y="175.26" rot="R270"/>
<instance part="R11" gate="G$1" x="472.44" y="185.42" rot="R270"/>
<instance part="SUPPLY17" gate="P" x="462.28" y="205.74"/>
<instance part="GND1" gate="1" x="116.84" y="165.1"/>
<instance part="SUPPLY19" gate="+5V" x="114.3" y="200.66" rot="R270"/>
<instance part="GND2" gate="1" x="63.5" y="193.04"/>
<instance part="GND3" gate="1" x="480.06" y="182.88"/>
<instance part="SUPPLY21" gate="P" x="439.42" y="205.74"/>
<instance part="R12" gate="G$1" x="454.66" y="185.42" rot="R270"/>
<instance part="R13" gate="G$1" x="439.42" y="195.58" rot="R270"/>
<instance part="GND6" gate="1" x="436.88" y="248.92"/>
<instance part="GND7" gate="1" x="447.04" y="248.92"/>
<instance part="D2" gate="G$1" x="439.42" y="185.42"/>
<instance part="R14" gate="G$1" x="462.28" y="198.12" rot="R270"/>
<instance part="Q3" gate="G$1" x="358.14" y="213.36"/>
<instance part="SUPPLY23" gate="G$1" x="129.54" y="259.08"/>
<instance part="FUSE1" gate="G$1" x="-10.16" y="210.82" rot="R90"/>
<instance part="C15" gate="G$1" x="449.58" y="353.06"/>
<instance part="C16" gate="G$1" x="474.98" y="307.34"/>
<instance part="C17" gate="G$1" x="482.6" y="307.34"/>
<instance part="C18" gate="G$1" x="490.22" y="307.34"/>
<instance part="C19" gate="G$1" x="497.84" y="307.34"/>
<instance part="C20" gate="G$1" x="505.46" y="307.34"/>
<instance part="GND8" gate="1" x="505.46" y="294.64"/>
<instance part="R15" gate="G$1" x="474.98" y="340.36" rot="R90"/>
<instance part="R16" gate="G$1" x="482.6" y="340.36" rot="R90"/>
<instance part="R17" gate="G$1" x="490.22" y="340.36" rot="R90"/>
<instance part="R18" gate="G$1" x="497.84" y="340.36" rot="R90"/>
<instance part="R19" gate="G$1" x="454.66" y="274.32" rot="R90"/>
<instance part="R21" gate="G$1" x="411.48" y="289.56" rot="R90"/>
<instance part="R22" gate="G$1" x="403.86" y="289.56" rot="R90"/>
<instance part="GND9" gate="1" x="454.66" y="266.7"/>
<instance part="SUPPLY25" gate="P" x="444.5" y="368.3"/>
<instance part="GND12" gate="1" x="459.74" y="350.52"/>
<instance part="L2" gate="G$1" x="203.2" y="226.06" rot="R90"/>
<instance part="L3" gate="G$1" x="203.2" y="218.44" rot="R90"/>
<instance part="GND13" gate="1" x="193.04" y="208.28"/>
<instance part="SUPPLY26" gate="P" x="193.04" y="256.54"/>
<instance part="L4" gate="G$1" x="203.2" y="233.68" rot="R90"/>
<instance part="GND14" gate="1" x="431.8" y="266.7"/>
<instance part="L5" gate="G$1" x="558.8" y="297.18" rot="R180"/>
<instance part="GND16" gate="1" x="187.96" y="167.64"/>
<instance part="C23" gate="G$1" x="325.12" y="185.42"/>
<instance part="C24" gate="G$1" x="340.36" y="185.42"/>
<instance part="C25" gate="G$1" x="353.06" y="195.58"/>
<instance part="C26" gate="G$1" x="365.76" y="195.58"/>
<instance part="GND17" gate="1" x="325.12" y="177.8"/>
<instance part="GND18" gate="1" x="340.36" y="177.8"/>
<instance part="GND19" gate="1" x="353.06" y="187.96"/>
<instance part="GND20" gate="1" x="365.76" y="187.96"/>
<instance part="RESET" gate="G$1" x="401.32" y="220.98"/>
<instance part="SUPPLY22" gate="P" x="393.7" y="233.68"/>
<instance part="R23" gate="G$1" x="393.7" y="226.06" rot="R90"/>
<instance part="C27" gate="G$1" x="393.7" y="213.36"/>
<instance part="GND15" gate="1" x="393.7" y="205.74"/>
<instance part="GND21" gate="1" x="406.4" y="205.74"/>
<instance part="J4" gate="G$1" x="538.48" y="175.26"/>
<instance part="SUPPLY27" gate="P" x="528.32" y="205.74"/>
<instance part="GND4" gate="1" x="528.32" y="172.72"/>
<instance part="J1" gate="G$1" x="492.76" y="215.9"/>
<instance part="C1" gate="G$1" x="71.12" y="177.8"/>
<instance part="C2" gate="G$1" x="104.14" y="177.8"/>
<instance part="U4" gate="G$1" x="86.36" y="190.5"/>
<instance part="SUPPLY9" gate="P" x="114.3" y="190.5" rot="R270"/>
<instance part="C28" gate="G$1" x="226.06" y="152.4"/>
<instance part="C29" gate="G$1" x="233.68" y="152.4"/>
<instance part="C30" gate="G$1" x="241.3" y="152.4"/>
<instance part="C31" gate="G$1" x="248.92" y="152.4"/>
<instance part="C32" gate="G$1" x="256.54" y="152.4"/>
<instance part="C33" gate="G$1" x="264.16" y="152.4"/>
<instance part="C34" gate="G$1" x="271.78" y="152.4"/>
<instance part="C35" gate="G$1" x="279.4" y="152.4"/>
<instance part="C36" gate="G$1" x="287.02" y="152.4"/>
<instance part="C37" gate="G$1" x="294.64" y="152.4"/>
<instance part="GND5" gate="1" x="226.06" y="144.78"/>
<instance part="SUPPLY12" gate="P" x="226.06" y="157.48"/>
<instance part="U3" gate="G$1" x="157.48" y="289.56"/>
<instance part="SUPPLY18" gate="G$1" x="144.78" y="259.08"/>
<instance part="SUPPLY28" gate="G$1" x="142.24" y="327.66"/>
<instance part="SUPPLY29" gate="P" x="180.34" y="327.66"/>
<instance part="GND22" gate="1" x="180.34" y="256.54"/>
<instance part="GND23" gate="1" x="172.72" y="312.42"/>
<instance part="C38" gate="G$1" x="172.72" y="320.04"/>
<instance part="C39" gate="G$1" x="147.32" y="320.04"/>
<instance part="SUPPLY30" gate="G$1" x="147.32" y="312.42"/>
<instance part="J5" gate="G$1" x="-20.32" y="205.74"/>
<instance part="C40" gate="G$1" x="467.36" y="218.44"/>
<instance part="SUPPLY2" gate="P" x="467.36" y="243.84"/>
<instance part="GND11" gate="1" x="472.44" y="208.28"/>
<instance part="C41" gate="G$1" x="302.26" y="152.4"/>
<instance part="C44" gate="G$1" x="320.04" y="152.4"/>
<instance part="C43" gate="G$1" x="50.8" y="208.28"/>
<instance part="R2" gate="G$1" x="10.16" y="271.78" rot="R90"/>
<instance part="R3" gate="G$1" x="10.16" y="259.08" rot="R90"/>
<instance part="R4" gate="G$1" x="10.16" y="246.38" rot="R90"/>
<instance part="L6" gate="G$1" x="182.88" y="182.88" rot="R180"/>
<instance part="GND24" gate="1" x="558.8" y="287.02"/>
<instance part="C42" gate="G$1" x="309.88" y="152.4"/>
<instance part="D3" gate="G$1" x="114.3" y="226.06" rot="R180"/>
<instance part="D4" gate="G$1" x="365.76" y="256.54"/>
<instance part="D5" gate="G$1" x="373.38" y="256.54"/>
<instance part="D6" gate="G$1" x="381" y="256.54"/>
<instance part="R24" gate="G$1" x="365.76" y="264.16" rot="R270"/>
<instance part="R25" gate="G$1" x="373.38" y="264.16" rot="R270"/>
<instance part="R26" gate="G$1" x="381" y="264.16" rot="R270"/>
<instance part="SUPPLY3" gate="P" x="365.76" y="271.78"/>
<instance part="R20" gate="G$1" x="2.54" y="210.82" rot="R270"/>
<instance part="D1" gate="G$1" x="7.62" y="205.74" smashed="yes">
<attribute name="NAME" x="8.382" y="211.582" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.16" y="211.582" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="Q1" gate="G$1" x="124.46" y="279.4"/>
<instance part="GND25" gate="1" x="403.86" y="276.86"/>
<instance part="J3" gate="G$1" x="523.24" y="307.34"/>
<instance part="Q4" gate="G$1" x="330.2" y="198.12"/>
<instance part="J6" gate="G$1" x="109.22" y="190.5"/>
<instance part="U6" gate="G$1" x="383.54" y="360.68" rot="MR0"/>
<instance part="SUPPLY20" gate="P" x="383.54" y="368.3"/>
<instance part="GND26" gate="1" x="383.54" y="353.06"/>
</instances>
<busses>
<bus name="ETH:MDIO,MDC,REFCLKO,TXD0,TXD1,TXEN,RXD0,RXD1,CRS_DV,RXER">
<segment>
<wire x1="347.98" y1="312.42" x2="347.98" y2="337.82" width="0.762" layer="92"/>
<wire x1="347.98" y1="337.82" x2="408.94" y2="337.82" width="0.762" layer="92"/>
<wire x1="408.94" y1="337.82" x2="408.94" y2="312.42" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="USB_DPLUS,USB_DMINUS,USB_LED,USB_CONNECT,MOSI0,MISO0,SCK0,SSEL0,!RSTOUT,!RESET,JTAG_RTCK,JTAG_TMS,JTAG_TCK,JTAG_TDO,JTAG_TDI,JTAG_TRST,MISO1,MOSI1,SCK1">
<segment>
<wire x1="213.36" y1="312.42" x2="213.36" y2="170.18" width="0.762" layer="92"/>
<wire x1="213.36" y1="170.18" x2="218.44" y2="165.1" width="0.762" layer="92"/>
<wire x1="218.44" y1="165.1" x2="347.98" y2="165.1" width="0.762" layer="92"/>
<wire x1="347.98" y1="165.1" x2="347.98" y2="304.8" width="0.762" layer="92"/>
<wire x1="347.98" y1="165.1" x2="426.72" y2="165.1" width="0.762" layer="92"/>
<wire x1="426.72" y1="165.1" x2="426.72" y2="233.68" width="0.762" layer="92"/>
<wire x1="426.72" y1="165.1" x2="576.58" y2="165.1" width="0.762" layer="92"/>
<wire x1="576.58" y1="165.1" x2="576.58" y2="185.42" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="TXD0" class="0">
<segment>
<wire x1="320.04" y1="332.74" x2="345.44" y2="332.74" width="0.1524" layer="91"/>
<wire x1="345.44" y1="332.74" x2="347.98" y2="335.28" width="0.1524" layer="91"/>
<label x="345.44" y="335.28" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[0]/ENET_TXD0"/>
</segment>
<segment>
<wire x1="419.1" y1="325.12" x2="411.48" y2="325.12" width="0.1524" layer="91"/>
<wire x1="411.48" y1="325.12" x2="408.94" y2="327.66" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="TXD0"/>
</segment>
</net>
<net name="TXD1" class="0">
<segment>
<wire x1="320.04" y1="330.2" x2="345.44" y2="330.2" width="0.1524" layer="91"/>
<wire x1="345.44" y1="330.2" x2="347.98" y2="332.74" width="0.1524" layer="91"/>
<label x="345.44" y="332.74" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[1]/ENET_TXD1"/>
</segment>
<segment>
<wire x1="419.1" y1="322.58" x2="411.48" y2="322.58" width="0.1524" layer="91"/>
<wire x1="411.48" y1="322.58" x2="408.94" y2="325.12" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="TXD1"/>
</segment>
</net>
<net name="TXEN" class="0">
<segment>
<wire x1="320.04" y1="327.66" x2="345.44" y2="327.66" width="0.1524" layer="91"/>
<wire x1="345.44" y1="327.66" x2="347.98" y2="330.2" width="0.1524" layer="91"/>
<label x="345.44" y="330.2" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[4]/ENET_TX_EN"/>
</segment>
<segment>
<wire x1="419.1" y1="320.04" x2="411.48" y2="320.04" width="0.1524" layer="91"/>
<wire x1="411.48" y1="320.04" x2="408.94" y2="322.58" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="TXEN"/>
</segment>
</net>
<net name="CRS_DV" class="0">
<segment>
<wire x1="320.04" y1="325.12" x2="345.44" y2="325.12" width="0.1524" layer="91"/>
<wire x1="345.44" y1="325.12" x2="347.98" y2="327.66" width="0.1524" layer="91"/>
<label x="345.44" y="327.66" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[8]/ENET_CRS"/>
</segment>
<segment>
<wire x1="419.1" y1="312.42" x2="411.48" y2="312.42" width="0.1524" layer="91"/>
<wire x1="411.48" y1="312.42" x2="408.94" y2="314.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="CRS_DV"/>
</segment>
</net>
<net name="RXD0" class="0">
<segment>
<wire x1="320.04" y1="322.58" x2="345.44" y2="322.58" width="0.1524" layer="91"/>
<wire x1="345.44" y1="322.58" x2="347.98" y2="325.12" width="0.1524" layer="91"/>
<label x="345.44" y="325.12" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[9]/ENET_RXD0"/>
</segment>
<segment>
<wire x1="419.1" y1="317.5" x2="411.48" y2="317.5" width="0.1524" layer="91"/>
<wire x1="411.48" y1="317.5" x2="408.94" y2="320.04" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RXD0"/>
</segment>
</net>
<net name="RXD1" class="0">
<segment>
<wire x1="320.04" y1="320.04" x2="345.44" y2="320.04" width="0.1524" layer="91"/>
<wire x1="345.44" y1="320.04" x2="347.98" y2="322.58" width="0.1524" layer="91"/>
<label x="345.44" y="322.58" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[10]/ENET_RXD1"/>
</segment>
<segment>
<wire x1="419.1" y1="314.96" x2="411.48" y2="314.96" width="0.1524" layer="91"/>
<wire x1="411.48" y1="314.96" x2="408.94" y2="317.5" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RXD1"/>
</segment>
</net>
<net name="RXER" class="0">
<segment>
<wire x1="320.04" y1="317.5" x2="345.44" y2="317.5" width="0.1524" layer="91"/>
<wire x1="345.44" y1="317.5" x2="347.98" y2="320.04" width="0.1524" layer="91"/>
<label x="345.44" y="320.04" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[14]/ENET_RX_ER"/>
</segment>
<segment>
<wire x1="419.1" y1="309.88" x2="411.48" y2="309.88" width="0.1524" layer="91"/>
<wire x1="411.48" y1="309.88" x2="408.94" y2="312.42" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RXER"/>
</segment>
</net>
<net name="REFCLKO" class="0">
<segment>
<wire x1="320.04" y1="314.96" x2="345.44" y2="314.96" width="0.1524" layer="91"/>
<wire x1="345.44" y1="314.96" x2="347.98" y2="317.5" width="0.1524" layer="91"/>
<label x="345.44" y="317.5" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[15]/ENET_REF_CLK"/>
</segment>
<segment>
<wire x1="375.92" y1="360.68" x2="368.3" y2="360.68" width="0.1524" layer="91"/>
<wire x1="368.3" y1="360.68" x2="368.3" y2="340.36" width="0.1524" layer="91"/>
<wire x1="368.3" y1="340.36" x2="365.76" y2="337.82" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="Y"/>
<label x="367.538" y="341.63" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="MDC" class="0">
<segment>
<wire x1="320.04" y1="312.42" x2="345.44" y2="312.42" width="0.1524" layer="91"/>
<wire x1="345.44" y1="312.42" x2="347.98" y2="314.96" width="0.1524" layer="91"/>
<label x="345.44" y="314.96" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[16]/ENET_MDC"/>
</segment>
<segment>
<wire x1="419.1" y1="332.74" x2="411.48" y2="332.74" width="0.1524" layer="91"/>
<wire x1="411.48" y1="332.74" x2="408.94" y2="335.28" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="MDC"/>
</segment>
</net>
<net name="MDIO" class="0">
<segment>
<wire x1="320.04" y1="309.88" x2="345.44" y2="309.88" width="0.1524" layer="91"/>
<wire x1="345.44" y1="309.88" x2="347.98" y2="312.42" width="0.1524" layer="91"/>
<label x="345.44" y="312.42" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[17]/ENET_MDIO"/>
</segment>
<segment>
<wire x1="419.1" y1="335.28" x2="411.48" y2="335.28" width="0.1524" layer="91"/>
<wire x1="411.48" y1="335.28" x2="408.94" y2="337.82" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="MDIO"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<wire x1="81.28" y1="302.26" x2="81.28" y2="314.96" width="0.1524" layer="91"/>
<wire x1="81.28" y1="314.96" x2="86.36" y2="314.96" width="0.1524" layer="91"/>
<wire x1="91.44" y1="302.26" x2="91.44" y2="314.96" width="0.1524" layer="91"/>
<wire x1="91.44" y1="314.96" x2="86.36" y2="314.96" width="0.1524" layer="91"/>
<wire x1="86.36" y1="302.26" x2="86.36" y2="314.96" width="0.1524" layer="91"/>
<wire x1="86.36" y1="314.96" x2="86.36" y2="325.12" width="0.1524" layer="91"/>
<wire x1="91.44" y1="314.96" x2="99.06" y2="314.96" width="0.1524" layer="91"/>
<wire x1="99.06" y1="314.96" x2="106.68" y2="314.96" width="0.1524" layer="91"/>
<wire x1="106.68" y1="314.96" x2="116.84" y2="314.96" width="0.1524" layer="91"/>
<wire x1="116.84" y1="314.96" x2="127" y2="314.96" width="0.1524" layer="91"/>
<wire x1="127" y1="314.96" x2="127" y2="312.42" width="0.1524" layer="91"/>
<wire x1="99.06" y1="314.96" x2="99.06" y2="312.42" width="0.1524" layer="91"/>
<wire x1="106.68" y1="314.96" x2="106.68" y2="312.42" width="0.1524" layer="91"/>
<wire x1="116.84" y1="314.96" x2="116.84" y2="312.42" width="0.1524" layer="91"/>
<junction x="86.36" y="314.96"/>
<junction x="106.68" y="314.96"/>
<junction x="99.06" y="314.96"/>
<junction x="91.44" y="314.96"/>
<junction x="116.84" y="314.96"/>
<pinref part="U2" gate="G$1" pin="AVDD"/>
<pinref part="U2" gate="G$1" pin="!RESET"/>
<pinref part="U2" gate="G$1" pin="DVDD"/>
<pinref part="SUPPLY1" gate="G$1" pin="VDD"/>
<pinref part="C10" gate="G$1" pin="1+"/>
<pinref part="C9" gate="G$1" pin="1+"/>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="C12" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="144.78" y1="279.4" x2="142.24" y2="279.4" width="0.1524" layer="91"/>
<wire x1="142.24" y1="279.4" x2="142.24" y2="294.64" width="0.1524" layer="91"/>
<wire x1="142.24" y1="294.64" x2="142.24" y2="299.72" width="0.1524" layer="91"/>
<wire x1="142.24" y1="299.72" x2="142.24" y2="322.58" width="0.1524" layer="91"/>
<wire x1="144.78" y1="294.64" x2="142.24" y2="294.64" width="0.1524" layer="91"/>
<wire x1="144.78" y1="299.72" x2="142.24" y2="299.72" width="0.1524" layer="91"/>
<wire x1="147.32" y1="322.58" x2="142.24" y2="322.58" width="0.1524" layer="91"/>
<wire x1="142.24" y1="325.12" x2="142.24" y2="322.58" width="0.1524" layer="91"/>
<junction x="142.24" y="294.64"/>
<junction x="142.24" y="299.72"/>
<junction x="142.24" y="322.58"/>
<pinref part="U3" gate="G$1" pin="VE1"/>
<pinref part="U3" gate="G$1" pin="VIA"/>
<pinref part="U3" gate="G$1" pin="VDD1"/>
<pinref part="C39" gate="G$1" pin="1"/>
<pinref part="SUPPLY28" gate="G$1" pin="VDD"/>
</segment>
<segment>
<wire x1="104.14" y1="228.6" x2="114.3" y2="228.6" width="0.1524" layer="91"/>
<junction x="114.3" y="228.6"/>
<pinref part="TRACO2" gate="G$1" pin="OUT+"/>
<pinref part="SUPPLY16" gate="G$1" pin="VDD"/>
<pinref part="D3" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="7.62" y1="210.82" x2="12.7" y2="210.82" width="0.1524" layer="91"/>
<junction x="7.62" y="210.82"/>
<pinref part="TRACO1" gate="G$1" pin="L"/>
<pinref part="R20" gate="G$1" pin="P$1"/>
<pinref part="D1" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<wire x1="86.36" y1="248.92" x2="86.36" y2="251.46" width="0.1524" layer="91"/>
<wire x1="86.36" y1="251.46" x2="81.28" y2="251.46" width="0.1524" layer="91"/>
<wire x1="81.28" y1="254" x2="81.28" y2="251.46" width="0.1524" layer="91"/>
<wire x1="86.36" y1="251.46" x2="91.44" y2="251.46" width="0.1524" layer="91"/>
<wire x1="91.44" y1="251.46" x2="91.44" y2="254" width="0.1524" layer="91"/>
<junction x="86.36" y="251.46"/>
<pinref part="SUPPLY4" gate="G$1" pin="AGND"/>
<pinref part="U2" gate="G$1" pin="AGND"/>
<pinref part="U2" gate="G$1" pin="DGND"/>
</segment>
<segment>
<wire x1="-17.78" y1="203.2" x2="-12.7" y2="203.2" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="203.2" x2="-12.7" y2="200.66" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="200.66" x2="7.62" y2="200.66" width="0.1524" layer="91"/>
<wire x1="7.62" y1="200.66" x2="12.7" y2="200.66" width="0.1524" layer="91"/>
<wire x1="12.7" y1="200.66" x2="12.7" y2="203.2" width="0.1524" layer="91"/>
<junction x="7.62" y="200.66"/>
<junction x="7.62" y="200.66"/>
<pinref part="TRACO1" gate="G$1" pin="N"/>
<pinref part="SUPPLY5" gate="G$1" pin="AGND"/>
<pinref part="J5" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="P$2"/>
</segment>
<segment>
<wire x1="22.86" y1="294.64" x2="25.4" y2="294.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="299.72" x2="25.4" y2="294.64" width="0.1524" layer="91"/>
<wire x1="33.02" y1="294.64" x2="25.4" y2="294.64" width="0.1524" layer="91"/>
<junction x="25.4" y="294.64"/>
<pinref part="L1" gate="G$1" pin="L2"/>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="SUPPLY7" gate="G$1" pin="AGND"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="27.94" y1="264.16" x2="27.94" y2="261.62" width="0.1524" layer="91"/>
<wire x1="25.4" y1="261.62" x2="27.94" y2="261.62" width="0.1524" layer="91"/>
<wire x1="22.86" y1="264.16" x2="22.86" y2="261.62" width="0.1524" layer="91"/>
<wire x1="22.86" y1="261.62" x2="25.4" y2="261.62" width="0.1524" layer="91"/>
<junction x="25.4" y="261.62"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="G$1" pin="AGND"/>
<pinref part="R7" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="38.1" y1="261.62" x2="38.1" y2="264.16" width="0.1524" layer="91"/>
<wire x1="43.18" y1="264.16" x2="43.18" y2="261.62" width="0.1524" layer="91"/>
<wire x1="43.18" y1="261.62" x2="40.64" y2="261.62" width="0.1524" layer="91"/>
<wire x1="40.64" y1="261.62" x2="38.1" y2="261.62" width="0.1524" layer="91"/>
<junction x="40.64" y="261.62"/>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="SUPPLY8" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="SUPPLY11" gate="G$1" pin="AGND"/>
</segment>
<segment>
<wire x1="99.06" y1="304.8" x2="99.06" y2="302.26" width="0.1524" layer="91"/>
<wire x1="99.06" y1="302.26" x2="106.68" y2="302.26" width="0.1524" layer="91"/>
<wire x1="106.68" y1="302.26" x2="116.84" y2="302.26" width="0.1524" layer="91"/>
<wire x1="116.84" y1="302.26" x2="127" y2="302.26" width="0.1524" layer="91"/>
<wire x1="127" y1="302.26" x2="127" y2="304.8" width="0.1524" layer="91"/>
<wire x1="106.68" y1="304.8" x2="106.68" y2="302.26" width="0.1524" layer="91"/>
<wire x1="116.84" y1="304.8" x2="116.84" y2="302.26" width="0.1524" layer="91"/>
<junction x="127" y="302.26"/>
<junction x="106.68" y="302.26"/>
<junction x="116.84" y="302.26"/>
<pinref part="C9" gate="G$1" pin="2-"/>
<pinref part="C10" gate="G$1" pin="2-"/>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="SUPPLY13" gate="G$1" pin="AGND"/>
</segment>
<segment>
<wire x1="104.14" y1="218.44" x2="114.3" y2="218.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="218.44" x2="114.3" y2="220.98" width="0.1524" layer="91"/>
<junction x="114.3" y="218.44"/>
<pinref part="TRACO2" gate="G$1" pin="OUT-"/>
<pinref part="SUPPLY15" gate="G$1" pin="AGND"/>
<pinref part="D3" gate="G$1" pin="C"/>
</segment>
<segment>
<wire x1="116.84" y1="264.16" x2="116.84" y2="261.62" width="0.1524" layer="91"/>
<wire x1="116.84" y1="261.62" x2="129.54" y2="261.62" width="0.1524" layer="91"/>
<wire x1="129.54" y1="261.62" x2="134.62" y2="261.62" width="0.1524" layer="91"/>
<wire x1="134.62" y1="261.62" x2="134.62" y2="264.16" width="0.1524" layer="91"/>
<junction x="129.54" y="261.62"/>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="SUPPLY23" gate="G$1" pin="AGND"/>
</segment>
<segment>
<wire x1="53.34" y1="254" x2="53.34" y2="251.46" width="0.1524" layer="91"/>
<wire x1="53.34" y1="251.46" x2="53.34" y2="248.92" width="0.1524" layer="91"/>
<wire x1="60.96" y1="251.46" x2="60.96" y2="254" width="0.1524" layer="91"/>
<wire x1="60.96" y1="251.46" x2="53.34" y2="251.46" width="0.1524" layer="91"/>
<junction x="53.34" y="251.46"/>
<pinref part="C8" gate="G$1" pin="2-"/>
<pinref part="SUPPLY14" gate="G$1" pin="AGND"/>
<pinref part="C7" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="109.22" y1="287.02" x2="137.16" y2="287.02" width="0.1524" layer="91"/>
<wire x1="137.16" y1="287.02" x2="137.16" y2="274.32" width="0.1524" layer="91"/>
<wire x1="137.16" y1="274.32" x2="144.78" y2="274.32" width="0.1524" layer="91"/>
<wire x1="144.78" y1="274.32" x2="144.78" y2="271.78" width="0.1524" layer="91"/>
<wire x1="144.78" y1="271.78" x2="144.78" y2="261.62" width="0.1524" layer="91"/>
<junction x="144.78" y="274.32"/>
<junction x="144.78" y="271.78"/>
<pinref part="U2" gate="G$1" pin="!CS"/>
<pinref part="U3" gate="G$1" pin="GND1_1"/>
<pinref part="U3" gate="G$1" pin="GND1_2"/>
<pinref part="SUPPLY18" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="SUPPLY30" gate="G$1" pin="AGND"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="22.86" y1="314.96" x2="25.4" y2="314.96" width="0.1524" layer="91"/>
<wire x1="25.4" y1="314.96" x2="25.4" y2="309.88" width="0.1524" layer="91"/>
<wire x1="25.4" y1="314.96" x2="33.02" y2="314.96" width="0.1524" layer="91"/>
<junction x="25.4" y="314.96"/>
<pinref part="L1" gate="G$1" pin="L1"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="22.86" y1="276.86" x2="22.86" y2="274.32" width="0.1524" layer="91"/>
<wire x1="10.16" y1="276.86" x2="22.86" y2="276.86" width="0.1524" layer="91"/>
<wire x1="22.86" y1="276.86" x2="27.94" y2="276.86" width="0.1524" layer="91"/>
<wire x1="27.94" y1="276.86" x2="27.94" y2="271.78" width="0.1524" layer="91"/>
<wire x1="63.5" y1="276.86" x2="27.94" y2="276.86" width="0.1524" layer="91"/>
<junction x="22.86" y="276.86"/>
<junction x="27.94" y="276.86"/>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="V2N"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="38.1" y1="274.32" x2="38.1" y2="279.4" width="0.1524" layer="91"/>
<wire x1="38.1" y1="279.4" x2="43.18" y2="279.4" width="0.1524" layer="91"/>
<wire x1="63.5" y1="279.4" x2="43.18" y2="279.4" width="0.1524" layer="91"/>
<wire x1="43.18" y1="279.4" x2="43.18" y2="271.78" width="0.1524" layer="91"/>
<junction x="43.18" y="279.4"/>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="V2P"/>
<pinref part="C5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="43.18" y1="294.64" x2="48.26" y2="294.64" width="0.1524" layer="91"/>
<wire x1="48.26" y1="294.64" x2="60.96" y2="294.64" width="0.1524" layer="91"/>
<wire x1="60.96" y1="294.64" x2="60.96" y2="284.48" width="0.1524" layer="91"/>
<wire x1="60.96" y1="284.48" x2="63.5" y2="284.48" width="0.1524" layer="91"/>
<junction x="48.26" y="294.64"/>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="V1N"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="43.18" y1="314.96" x2="48.26" y2="314.96" width="0.1524" layer="91"/>
<wire x1="48.26" y1="314.96" x2="48.26" y2="312.42" width="0.1524" layer="91"/>
<wire x1="63.5" y1="289.56" x2="63.5" y2="314.96" width="0.1524" layer="91"/>
<wire x1="63.5" y1="314.96" x2="48.26" y2="314.96" width="0.1524" layer="91"/>
<junction x="48.26" y="314.96"/>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="V1P"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="53.34" y1="271.78" x2="53.34" y2="261.62" width="0.1524" layer="91"/>
<wire x1="53.34" y1="271.78" x2="60.96" y2="271.78" width="0.1524" layer="91"/>
<wire x1="63.5" y1="271.78" x2="60.96" y2="271.78" width="0.1524" layer="91"/>
<wire x1="60.96" y1="271.78" x2="60.96" y2="261.62" width="0.1524" layer="91"/>
<junction x="60.96" y="271.78"/>
<pinref part="C8" gate="G$1" pin="1+"/>
<pinref part="U2" gate="G$1" pin="REF_IN/OUT"/>
<pinref part="C7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="353.06" y1="213.36" x2="320.04" y2="213.36" width="0.1524" layer="91"/>
<wire x1="353.06" y1="213.36" x2="353.06" y2="198.12" width="0.1524" layer="91"/>
<junction x="353.06" y="213.36"/>
<pinref part="U5" gate="G$1" pin="RTCX2"/>
<pinref part="Q3" gate="G$1" pin="P$1"/>
<pinref part="C25" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="436.88" y1="287.02" x2="436.88" y2="274.32" width="0.1524" layer="91"/>
<wire x1="436.88" y1="274.32" x2="436.88" y2="264.16" width="0.1524" layer="91"/>
<wire x1="436.88" y1="264.16" x2="436.88" y2="259.08" width="0.1524" layer="91"/>
<junction x="436.88" y="274.32"/>
<junction x="436.88" y="264.16"/>
<pinref part="U1" gate="G$1" pin="XTAL1"/>
<pinref part="Q2" gate="G$1" pin="P$1"/>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="C13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="447.04" y1="287.02" x2="447.04" y2="274.32" width="0.1524" layer="91"/>
<wire x1="447.04" y1="274.32" x2="447.04" y2="264.16" width="0.1524" layer="91"/>
<wire x1="447.04" y1="264.16" x2="447.04" y2="259.08" width="0.1524" layer="91"/>
<junction x="447.04" y="274.32"/>
<junction x="447.04" y="264.16"/>
<pinref part="U1" gate="G$1" pin="XTAL2"/>
<pinref part="Q2" gate="G$1" pin="P$2"/>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="C14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="USB_LED" class="0">
<segment>
<wire x1="320.04" y1="307.34" x2="345.44" y2="307.34" width="0.1524" layer="91"/>
<wire x1="345.44" y1="307.34" x2="347.98" y2="304.8" width="0.1524" layer="91"/>
<label x="345.44" y="309.88" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P1[18]/USB_UP_LED/PWM1[1]/CAP1[0]"/>
</segment>
<segment>
<wire x1="436.88" y1="165.1" x2="439.42" y2="167.64" width="0.1524" layer="91"/>
<wire x1="439.42" y1="180.34" x2="439.42" y2="167.64" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
</segment>
</net>
<net name="USB_CONNECT" class="0">
<segment>
<wire x1="320.04" y1="246.38" x2="345.44" y2="246.38" width="0.1524" layer="91"/>
<wire x1="345.44" y1="246.38" x2="347.98" y2="243.84" width="0.1524" layer="91"/>
<label x="345.44" y="248.92" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P2[9]/USB_CONNECT/RXD2/ENET_MDIO"/>
</segment>
<segment>
<wire x1="459.74" y1="193.04" x2="462.28" y2="193.04" width="0.1524" layer="91"/>
<wire x1="462.28" y1="167.64" x2="462.28" y2="193.04" width="0.1524" layer="91"/>
<wire x1="462.28" y1="167.64" x2="459.74" y2="165.1" width="0.1524" layer="91"/>
<junction x="462.28" y="193.04"/>
<pinref part="T1" gate="G$1" pin="G"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="USB_DPLUS" class="0">
<segment>
<wire x1="231.14" y1="266.7" x2="215.9" y2="266.7" width="0.1524" layer="91"/>
<wire x1="215.9" y1="266.7" x2="213.36" y2="264.16" width="0.1524" layer="91"/>
<label x="231.14" y="269.24" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P0[29]/USB_DPLUS"/>
</segment>
<segment>
<wire x1="467.36" y1="170.18" x2="467.36" y2="167.64" width="0.1524" layer="91"/>
<wire x1="467.36" y1="167.64" x2="464.82" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="USB_DMINUS" class="0">
<segment>
<wire x1="215.9" y1="264.16" x2="213.36" y2="261.62" width="0.1524" layer="91"/>
<wire x1="215.9" y1="264.16" x2="231.14" y2="264.16" width="0.1524" layer="91"/>
<label x="231.14" y="266.7" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P0[30]/USB_DMINUS"/>
</segment>
<segment>
<wire x1="469.9" y1="165.1" x2="472.44" y2="167.64" width="0.1524" layer="91"/>
<wire x1="472.44" y1="167.64" x2="472.44" y2="180.34" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="MOSI0" class="0">
<segment>
<wire x1="213.36" y1="292.1" x2="215.9" y2="294.64" width="0.1524" layer="91"/>
<wire x1="215.9" y1="294.64" x2="231.14" y2="294.64" width="0.1524" layer="91"/>
<label x="220.98" y="294.64" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="P0[18]/DCD1/MOSI0/MOSI"/>
</segment>
<segment>
<wire x1="426.72" y1="231.14" x2="429.26" y2="233.68" width="0.1524" layer="91"/>
<wire x1="429.26" y1="233.68" x2="477.52" y2="233.68" width="0.1524" layer="91"/>
<label x="429.26" y="233.68" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="CMD/DI"/>
</segment>
</net>
<net name="SSEL0" class="0">
<segment>
<wire x1="231.14" y1="299.72" x2="215.9" y2="299.72" width="0.1524" layer="91"/>
<wire x1="215.9" y1="299.72" x2="213.36" y2="297.18" width="0.1524" layer="91"/>
<label x="228.6" y="302.26" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P0[16]/RXD1/SSEL0/SSEL"/>
</segment>
<segment>
<wire x1="429.26" y1="236.22" x2="426.72" y2="233.68" width="0.1524" layer="91"/>
<wire x1="477.52" y1="236.22" x2="429.26" y2="236.22" width="0.1524" layer="91"/>
<label x="429.26" y="236.22" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="DAT3/CD/!CS"/>
</segment>
</net>
<net name="SCK0" class="0">
<segment>
<wire x1="231.14" y1="302.26" x2="215.9" y2="302.26" width="0.1524" layer="91"/>
<wire x1="215.9" y1="302.26" x2="213.36" y2="299.72" width="0.1524" layer="91"/>
<label x="228.6" y="304.8" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="P0[15]/TXD1/SCK0/SCK"/>
</segment>
<segment>
<wire x1="477.52" y1="228.6" x2="429.26" y2="228.6" width="0.1524" layer="91"/>
<wire x1="429.26" y1="228.6" x2="426.72" y2="226.06" width="0.1524" layer="91"/>
<label x="429.26" y="229.108" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="CLK/SCLK"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="60.96" y1="228.6" x2="68.58" y2="228.6" width="0.1524" layer="91"/>
<wire x1="60.96" y1="210.82" x2="60.96" y2="228.6" width="0.1524" layer="91"/>
<wire x1="60.96" y1="210.82" x2="71.12" y2="210.82" width="0.1524" layer="91"/>
<wire x1="50.8" y1="210.82" x2="60.96" y2="210.82" width="0.1524" layer="91"/>
<wire x1="73.66" y1="190.5" x2="71.12" y2="190.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="190.5" x2="71.12" y2="185.42" width="0.1524" layer="91"/>
<wire x1="71.12" y1="185.42" x2="73.66" y2="185.42" width="0.1524" layer="91"/>
<wire x1="71.12" y1="180.34" x2="71.12" y2="185.42" width="0.1524" layer="91"/>
<wire x1="71.12" y1="210.82" x2="71.12" y2="200.66" width="0.1524" layer="91"/>
<wire x1="71.12" y1="200.66" x2="71.12" y2="190.5" width="0.1524" layer="91"/>
<wire x1="111.76" y1="200.66" x2="71.12" y2="200.66" width="0.1524" layer="91"/>
<junction x="60.96" y="210.82"/>
<junction x="71.12" y="185.42"/>
<junction x="71.12" y="190.5"/>
<junction x="71.12" y="200.66"/>
<junction x="50.8" y="210.82"/>
<pinref part="TRACO2" gate="G$1" pin="IN+"/>
<pinref part="TRACO1" gate="G$1" pin="+VOUT"/>
<pinref part="U4" gate="G$1" pin="EN"/>
<pinref part="U4" gate="G$1" pin="IN"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="SUPPLY19" gate="+5V" pin="+5V"/>
<pinref part="C43" gate="G$1" pin="1+"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="50.8" y1="203.2" x2="63.5" y2="203.2" width="0.1524" layer="91"/>
<wire x1="63.5" y1="203.2" x2="63.5" y2="218.44" width="0.1524" layer="91"/>
<wire x1="63.5" y1="218.44" x2="68.58" y2="218.44" width="0.1524" layer="91"/>
<wire x1="63.5" y1="203.2" x2="63.5" y2="195.58" width="0.1524" layer="91"/>
<junction x="63.5" y="203.2"/>
<junction x="50.8" y="203.2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="TRACO1" gate="G$1" pin="-VOUT"/>
<pinref part="TRACO2" gate="G$1" pin="IN-"/>
<pinref part="C43" gate="G$1" pin="2-"/>
</segment>
<segment>
<wire x1="487.68" y1="190.5" x2="480.06" y2="190.5" width="0.1524" layer="91"/>
<wire x1="480.06" y1="190.5" x2="480.06" y2="185.42" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="J2" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="231.14" y1="213.36" x2="220.98" y2="213.36" width="0.1524" layer="91"/>
<wire x1="220.98" y1="213.36" x2="220.98" y2="210.82" width="0.1524" layer="91"/>
<wire x1="231.14" y1="210.82" x2="220.98" y2="210.82" width="0.1524" layer="91"/>
<wire x1="220.98" y1="210.82" x2="220.98" y2="208.28" width="0.1524" layer="91"/>
<wire x1="231.14" y1="208.28" x2="220.98" y2="208.28" width="0.1524" layer="91"/>
<wire x1="220.98" y1="208.28" x2="220.98" y2="205.74" width="0.1524" layer="91"/>
<wire x1="231.14" y1="205.74" x2="220.98" y2="205.74" width="0.1524" layer="91"/>
<wire x1="220.98" y1="205.74" x2="220.98" y2="203.2" width="0.1524" layer="91"/>
<wire x1="231.14" y1="203.2" x2="220.98" y2="203.2" width="0.1524" layer="91"/>
<wire x1="220.98" y1="203.2" x2="220.98" y2="200.66" width="0.1524" layer="91"/>
<wire x1="220.98" y1="200.66" x2="231.14" y2="200.66" width="0.1524" layer="91"/>
<wire x1="195.58" y1="218.44" x2="193.04" y2="218.44" width="0.1524" layer="91"/>
<wire x1="193.04" y1="218.44" x2="193.04" y2="213.36" width="0.1524" layer="91"/>
<wire x1="193.04" y1="213.36" x2="193.04" y2="210.82" width="0.1524" layer="91"/>
<wire x1="220.98" y1="213.36" x2="193.04" y2="213.36" width="0.1524" layer="91"/>
<junction x="220.98" y="210.82"/>
<junction x="220.98" y="208.28"/>
<junction x="220.98" y="205.74"/>
<junction x="220.98" y="203.2"/>
<junction x="220.98" y="213.36"/>
<junction x="193.04" y="213.36"/>
<pinref part="U5" gate="G$1" pin="VSS1"/>
<pinref part="U5" gate="G$1" pin="VSS2"/>
<pinref part="U5" gate="G$1" pin="VSS3"/>
<pinref part="U5" gate="G$1" pin="VSS4"/>
<pinref part="U5" gate="G$1" pin="VSS5"/>
<pinref part="U5" gate="G$1" pin="VSS6"/>
<pinref part="L3" gate="G$1" pin="P$2"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="513.08" y1="299.72" x2="505.46" y2="299.72" width="0.1524" layer="91"/>
<wire x1="505.46" y1="299.72" x2="497.84" y2="299.72" width="0.1524" layer="91"/>
<wire x1="497.84" y1="299.72" x2="490.22" y2="299.72" width="0.1524" layer="91"/>
<wire x1="490.22" y1="299.72" x2="482.6" y2="299.72" width="0.1524" layer="91"/>
<wire x1="482.6" y1="299.72" x2="474.98" y2="299.72" width="0.1524" layer="91"/>
<wire x1="474.98" y1="299.72" x2="474.98" y2="302.26" width="0.1524" layer="91"/>
<wire x1="505.46" y1="302.26" x2="505.46" y2="299.72" width="0.1524" layer="91"/>
<wire x1="505.46" y1="299.72" x2="505.46" y2="297.18" width="0.1524" layer="91"/>
<wire x1="497.84" y1="302.26" x2="497.84" y2="299.72" width="0.1524" layer="91"/>
<wire x1="490.22" y1="302.26" x2="490.22" y2="299.72" width="0.1524" layer="91"/>
<wire x1="482.6" y1="302.26" x2="482.6" y2="299.72" width="0.1524" layer="91"/>
<junction x="505.46" y="299.72"/>
<junction x="497.84" y="299.72"/>
<junction x="490.22" y="299.72"/>
<junction x="482.6" y="299.72"/>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="J3" gate="G$1" pin="GND_CHS"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="449.58" y1="355.6" x2="459.74" y2="355.6" width="0.1524" layer="91"/>
<wire x1="459.74" y1="355.6" x2="459.74" y2="353.06" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="431.8" y1="287.02" x2="431.8" y2="269.24" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="182.88" y1="175.26" x2="182.88" y2="170.18" width="0.1524" layer="91"/>
<wire x1="182.88" y1="170.18" x2="187.96" y2="170.18" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="L6" gate="G$1" pin="P$2"/>
</segment>
<segment>
<wire x1="340.36" y1="180.34" x2="335.28" y2="180.34" width="0.1524" layer="91"/>
<wire x1="335.28" y1="180.34" x2="335.28" y2="190.5" width="0.1524" layer="91"/>
<wire x1="335.28" y1="193.04" x2="335.28" y2="190.5" width="0.1524" layer="91"/>
<wire x1="335.28" y1="190.5" x2="337.82" y2="190.5" width="0.1524" layer="91"/>
<wire x1="337.82" y1="190.5" x2="337.82" y2="193.04" width="0.1524" layer="91"/>
<junction x="335.28" y="190.5"/>
<junction x="340.36" y="180.34"/>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="Q4" gate="G$1" pin="GND1"/>
<pinref part="Q4" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="C26" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="C23" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="406.4" y1="220.98" x2="406.4" y2="218.44" width="0.1524" layer="91"/>
<wire x1="406.4" y1="218.44" x2="406.4" y2="208.28" width="0.1524" layer="91"/>
<junction x="406.4" y="218.44"/>
<pinref part="RESET" gate="G$1" pin="B_1"/>
<pinref part="GND21" gate="1" pin="GND"/>
<pinref part="RESET" gate="G$1" pin="B_2"/>
</segment>
<segment>
<wire x1="530.86" y1="177.8" x2="528.32" y2="177.8" width="0.1524" layer="91"/>
<wire x1="528.32" y1="175.26" x2="528.32" y2="177.8" width="0.1524" layer="91"/>
<wire x1="528.32" y1="177.8" x2="528.32" y2="182.88" width="0.1524" layer="91"/>
<wire x1="530.86" y1="182.88" x2="528.32" y2="182.88" width="0.1524" layer="91"/>
<wire x1="528.32" y1="182.88" x2="528.32" y2="185.42" width="0.1524" layer="91"/>
<wire x1="528.32" y1="185.42" x2="530.86" y2="185.42" width="0.1524" layer="91"/>
<junction x="528.32" y="177.8"/>
<junction x="528.32" y="182.88"/>
<pinref part="J4" gate="G$1" pin="GND3"/>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="J4" gate="G$1" pin="GND2"/>
<pinref part="J4" gate="G$1" pin="GND1"/>
</segment>
<segment>
<wire x1="104.14" y1="170.18" x2="104.14" y2="172.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="170.18" x2="91.44" y2="175.26" width="0.1524" layer="91"/>
<wire x1="88.9" y1="175.26" x2="88.9" y2="170.18" width="0.1524" layer="91"/>
<wire x1="88.9" y1="170.18" x2="91.44" y2="170.18" width="0.1524" layer="91"/>
<wire x1="71.12" y1="172.72" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<wire x1="88.9" y1="170.18" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<wire x1="91.44" y1="170.18" x2="104.14" y2="170.18" width="0.1524" layer="91"/>
<wire x1="104.14" y1="170.18" x2="116.84" y2="170.18" width="0.1524" layer="91"/>
<wire x1="116.84" y1="170.18" x2="116.84" y2="167.64" width="0.1524" layer="91"/>
<junction x="88.9" y="170.18"/>
<junction x="91.44" y="170.18"/>
<junction x="104.14" y="170.18"/>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="U4" gate="G$1" pin="GND2"/>
<pinref part="U4" gate="G$1" pin="GND1"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="226.06" y1="147.32" x2="233.68" y2="147.32" width="0.1524" layer="91"/>
<wire x1="233.68" y1="147.32" x2="241.3" y2="147.32" width="0.1524" layer="91"/>
<wire x1="241.3" y1="147.32" x2="248.92" y2="147.32" width="0.1524" layer="91"/>
<wire x1="248.92" y1="147.32" x2="256.54" y2="147.32" width="0.1524" layer="91"/>
<wire x1="256.54" y1="147.32" x2="264.16" y2="147.32" width="0.1524" layer="91"/>
<wire x1="264.16" y1="147.32" x2="271.78" y2="147.32" width="0.1524" layer="91"/>
<wire x1="271.78" y1="147.32" x2="279.4" y2="147.32" width="0.1524" layer="91"/>
<wire x1="279.4" y1="147.32" x2="287.02" y2="147.32" width="0.1524" layer="91"/>
<wire x1="287.02" y1="147.32" x2="294.64" y2="147.32" width="0.1524" layer="91"/>
<wire x1="294.64" y1="147.32" x2="302.26" y2="147.32" width="0.1524" layer="91"/>
<wire x1="302.26" y1="147.32" x2="309.88" y2="147.32" width="0.1524" layer="91"/>
<wire x1="320.04" y1="147.32" x2="309.88" y2="147.32" width="0.1524" layer="91"/>
<junction x="233.68" y="147.32"/>
<junction x="241.3" y="147.32"/>
<junction x="248.92" y="147.32"/>
<junction x="256.54" y="147.32"/>
<junction x="264.16" y="147.32"/>
<junction x="271.78" y="147.32"/>
<junction x="279.4" y="147.32"/>
<junction x="287.02" y="147.32"/>
<junction x="226.06" y="147.32"/>
<junction x="294.64" y="147.32"/>
<junction x="302.26" y="147.32"/>
<junction x="309.88" y="147.32"/>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="C36" gate="G$1" pin="2"/>
<pinref part="C37" gate="G$1" pin="2"/>
<pinref part="C41" gate="G$1" pin="2"/>
<pinref part="C44" gate="G$1" pin="2-"/>
<pinref part="C42" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<pinref part="C38" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="177.8" y1="274.32" x2="180.34" y2="274.32" width="0.1524" layer="91"/>
<wire x1="180.34" y1="274.32" x2="180.34" y2="271.78" width="0.1524" layer="91"/>
<wire x1="180.34" y1="271.78" x2="180.34" y2="259.08" width="0.1524" layer="91"/>
<wire x1="177.8" y1="271.78" x2="180.34" y2="271.78" width="0.1524" layer="91"/>
<junction x="180.34" y="271.78"/>
<pinref part="U3" gate="G$1" pin="GND2_1"/>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="GND2_2"/>
</segment>
<segment>
<wire x1="467.36" y1="213.36" x2="467.36" y2="210.82" width="0.1524" layer="91"/>
<wire x1="467.36" y1="210.82" x2="472.44" y2="210.82" width="0.1524" layer="91"/>
<wire x1="472.44" y1="210.82" x2="474.98" y2="210.82" width="0.1524" layer="91"/>
<wire x1="474.98" y1="210.82" x2="474.98" y2="226.06" width="0.1524" layer="91"/>
<wire x1="474.98" y1="226.06" x2="477.52" y2="226.06" width="0.1524" layer="91"/>
<junction x="472.44" y="210.82"/>
<pinref part="C40" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="J1" gate="G$1" pin="VSS"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="L5" gate="G$1" pin="P$2"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="403.86" y1="284.48" x2="403.86" y2="281.94" width="0.1524" layer="91"/>
<wire x1="403.86" y1="281.94" x2="403.86" y2="279.4" width="0.1524" layer="91"/>
<wire x1="411.48" y1="284.48" x2="411.48" y2="281.94" width="0.1524" layer="91"/>
<wire x1="411.48" y1="281.94" x2="403.86" y2="281.94" width="0.1524" layer="91"/>
<junction x="403.86" y="281.94"/>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="R21" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="GND"/>
<pinref part="GND26" gate="1" pin="GND"/>
</segment>
</net>
<net name="+4.1V" class="0">
<segment>
<wire x1="462.28" y1="203.2" x2="454.66" y2="203.2" width="0.1524" layer="91"/>
<wire x1="454.66" y1="203.2" x2="454.66" y2="200.66" width="0.1524" layer="91"/>
<junction x="462.28" y="203.2"/>
<pinref part="SUPPLY17" gate="P" pin="+4.1V"/>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="T1" gate="G$1" pin="D"/>
</segment>
<segment>
<wire x1="193.04" y1="254" x2="193.04" y2="251.46" width="0.1524" layer="91"/>
<wire x1="193.04" y1="251.46" x2="193.04" y2="233.68" width="0.1524" layer="91"/>
<wire x1="193.04" y1="233.68" x2="193.04" y2="226.06" width="0.1524" layer="91"/>
<wire x1="193.04" y1="226.06" x2="195.58" y2="226.06" width="0.1524" layer="91"/>
<wire x1="193.04" y1="233.68" x2="195.58" y2="233.68" width="0.1524" layer="91"/>
<wire x1="231.14" y1="238.76" x2="228.6" y2="238.76" width="0.1524" layer="91"/>
<wire x1="228.6" y1="238.76" x2="228.6" y2="241.3" width="0.1524" layer="91"/>
<wire x1="228.6" y1="241.3" x2="228.6" y2="243.84" width="0.1524" layer="91"/>
<wire x1="228.6" y1="243.84" x2="228.6" y2="246.38" width="0.1524" layer="91"/>
<wire x1="231.14" y1="251.46" x2="228.6" y2="251.46" width="0.1524" layer="91"/>
<wire x1="228.6" y1="251.46" x2="228.6" y2="248.92" width="0.1524" layer="91"/>
<wire x1="231.14" y1="248.92" x2="228.6" y2="248.92" width="0.1524" layer="91"/>
<wire x1="228.6" y1="248.92" x2="228.6" y2="246.38" width="0.1524" layer="91"/>
<wire x1="231.14" y1="246.38" x2="228.6" y2="246.38" width="0.1524" layer="91"/>
<wire x1="228.6" y1="243.84" x2="231.14" y2="243.84" width="0.1524" layer="91"/>
<wire x1="231.14" y1="241.3" x2="228.6" y2="241.3" width="0.1524" layer="91"/>
<wire x1="228.6" y1="251.46" x2="193.04" y2="251.46" width="0.1524" layer="91"/>
<wire x1="231.14" y1="233.68" x2="228.6" y2="233.68" width="0.1524" layer="91"/>
<wire x1="228.6" y1="233.68" x2="228.6" y2="238.76" width="0.1524" layer="91"/>
<junction x="193.04" y="233.68"/>
<junction x="228.6" y="251.46"/>
<junction x="228.6" y="248.92"/>
<junction x="228.6" y="246.38"/>
<junction x="228.6" y="243.84"/>
<junction x="228.6" y="241.3"/>
<junction x="193.04" y="251.46"/>
<junction x="228.6" y="238.76"/>
<pinref part="SUPPLY26" gate="P" pin="+4.1V"/>
<pinref part="L2" gate="G$1" pin="P$2"/>
<pinref part="L4" gate="G$1" pin="P$2"/>
<pinref part="U5" gate="G$1" pin="VDD(REG)(3V3)2"/>
<pinref part="U5" gate="G$1" pin="VDD(3V3)1"/>
<pinref part="U5" gate="G$1" pin="VDD(3V3)2"/>
<pinref part="U5" gate="G$1" pin="VDD(3V3)3"/>
<pinref part="U5" gate="G$1" pin="VDD(3V3)4"/>
<pinref part="U5" gate="G$1" pin="VDD(REG)(3V3)1"/>
<pinref part="U5" gate="G$1" pin="VBAT"/>
</segment>
<segment>
<pinref part="SUPPLY22" gate="P" pin="+4.1V"/>
<pinref part="R23" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="530.86" y1="187.96" x2="528.32" y2="187.96" width="0.1524" layer="91"/>
<wire x1="528.32" y1="187.96" x2="528.32" y2="203.2" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="VDD"/>
<pinref part="SUPPLY27" gate="P" pin="+4.1V"/>
</segment>
<segment>
<wire x1="226.06" y1="154.94" x2="233.68" y2="154.94" width="0.1524" layer="91"/>
<wire x1="233.68" y1="154.94" x2="241.3" y2="154.94" width="0.1524" layer="91"/>
<wire x1="241.3" y1="154.94" x2="248.92" y2="154.94" width="0.1524" layer="91"/>
<wire x1="248.92" y1="154.94" x2="256.54" y2="154.94" width="0.1524" layer="91"/>
<wire x1="256.54" y1="154.94" x2="264.16" y2="154.94" width="0.1524" layer="91"/>
<wire x1="264.16" y1="154.94" x2="271.78" y2="154.94" width="0.1524" layer="91"/>
<wire x1="271.78" y1="154.94" x2="279.4" y2="154.94" width="0.1524" layer="91"/>
<wire x1="279.4" y1="154.94" x2="287.02" y2="154.94" width="0.1524" layer="91"/>
<wire x1="287.02" y1="154.94" x2="294.64" y2="154.94" width="0.1524" layer="91"/>
<wire x1="294.64" y1="154.94" x2="302.26" y2="154.94" width="0.1524" layer="91"/>
<wire x1="302.26" y1="154.94" x2="309.88" y2="154.94" width="0.1524" layer="91"/>
<wire x1="320.04" y1="154.94" x2="309.88" y2="154.94" width="0.1524" layer="91"/>
<junction x="287.02" y="154.94"/>
<junction x="279.4" y="154.94"/>
<junction x="271.78" y="154.94"/>
<junction x="264.16" y="154.94"/>
<junction x="256.54" y="154.94"/>
<junction x="248.92" y="154.94"/>
<junction x="241.3" y="154.94"/>
<junction x="233.68" y="154.94"/>
<junction x="226.06" y="154.94"/>
<junction x="302.26" y="154.94"/>
<junction x="294.64" y="154.94"/>
<junction x="309.88" y="154.94"/>
<pinref part="C28" gate="G$1" pin="1"/>
<pinref part="SUPPLY12" gate="P" pin="+4.1V"/>
<pinref part="C29" gate="G$1" pin="1"/>
<pinref part="C30" gate="G$1" pin="1"/>
<pinref part="C31" gate="G$1" pin="1"/>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="C34" gate="G$1" pin="1"/>
<pinref part="C35" gate="G$1" pin="1"/>
<pinref part="C36" gate="G$1" pin="1"/>
<pinref part="C37" gate="G$1" pin="1"/>
<pinref part="C41" gate="G$1" pin="1"/>
<pinref part="C44" gate="G$1" pin="1+"/>
<pinref part="C42" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="177.8" y1="279.4" x2="180.34" y2="279.4" width="0.1524" layer="91"/>
<wire x1="180.34" y1="279.4" x2="180.34" y2="299.72" width="0.1524" layer="91"/>
<wire x1="180.34" y1="299.72" x2="180.34" y2="322.58" width="0.1524" layer="91"/>
<wire x1="180.34" y1="322.58" x2="180.34" y2="325.12" width="0.1524" layer="91"/>
<wire x1="177.8" y1="299.72" x2="180.34" y2="299.72" width="0.1524" layer="91"/>
<wire x1="172.72" y1="322.58" x2="180.34" y2="322.58" width="0.1524" layer="91"/>
<junction x="180.34" y="299.72"/>
<junction x="180.34" y="322.58"/>
<pinref part="U3" gate="G$1" pin="VE2"/>
<pinref part="SUPPLY29" gate="P" pin="+4.1V"/>
<pinref part="U3" gate="G$1" pin="VDD2"/>
<pinref part="C38" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="477.52" y1="231.14" x2="467.36" y2="231.14" width="0.1524" layer="91"/>
<wire x1="467.36" y1="220.98" x2="467.36" y2="231.14" width="0.1524" layer="91"/>
<wire x1="467.36" y1="231.14" x2="467.36" y2="241.3" width="0.1524" layer="91"/>
<junction x="467.36" y="231.14"/>
<junction x="467.36" y="220.98"/>
<pinref part="J1" gate="G$1" pin="VDD"/>
<pinref part="C40" gate="G$1" pin="1"/>
<pinref part="SUPPLY2" gate="P" pin="+4.1V"/>
</segment>
<segment>
<wire x1="365.76" y1="269.24" x2="373.38" y2="269.24" width="0.1524" layer="91"/>
<wire x1="373.38" y1="269.24" x2="381" y2="269.24" width="0.1524" layer="91"/>
<junction x="365.76" y="269.24"/>
<junction x="373.38" y="269.24"/>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="SUPPLY3" gate="P" pin="+4.1V"/>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="R26" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="439.42" y1="203.2" x2="439.42" y2="200.66" width="0.1524" layer="91"/>
<pinref part="SUPPLY21" gate="P" pin="+4.1V"/>
<pinref part="R13" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="444.5" y1="365.76" x2="444.5" y2="358.14" width="0.1524" layer="91"/>
<wire x1="444.5" y1="358.14" x2="444.5" y2="345.44" width="0.1524" layer="91"/>
<wire x1="444.5" y1="358.14" x2="439.42" y2="358.14" width="0.1524" layer="91"/>
<wire x1="439.42" y1="358.14" x2="434.34" y2="358.14" width="0.1524" layer="91"/>
<wire x1="434.34" y1="358.14" x2="434.34" y2="345.44" width="0.1524" layer="91"/>
<wire x1="439.42" y1="345.44" x2="439.42" y2="358.14" width="0.1524" layer="91"/>
<wire x1="444.5" y1="358.14" x2="474.98" y2="358.14" width="0.1524" layer="91"/>
<wire x1="505.46" y1="309.88" x2="505.46" y2="317.5" width="0.1524" layer="91"/>
<wire x1="505.46" y1="317.5" x2="505.46" y2="330.2" width="0.1524" layer="91"/>
<wire x1="505.46" y1="330.2" x2="505.46" y2="347.98" width="0.1524" layer="91"/>
<wire x1="513.08" y1="330.2" x2="505.46" y2="330.2" width="0.1524" layer="91"/>
<wire x1="513.08" y1="317.5" x2="505.46" y2="317.5" width="0.1524" layer="91"/>
<wire x1="505.46" y1="347.98" x2="497.84" y2="347.98" width="0.1524" layer="91"/>
<wire x1="474.98" y1="345.44" x2="474.98" y2="347.98" width="0.1524" layer="91"/>
<wire x1="474.98" y1="347.98" x2="482.6" y2="347.98" width="0.1524" layer="91"/>
<wire x1="482.6" y1="345.44" x2="482.6" y2="347.98" width="0.1524" layer="91"/>
<wire x1="482.6" y1="347.98" x2="490.22" y2="347.98" width="0.1524" layer="91"/>
<wire x1="490.22" y1="345.44" x2="490.22" y2="347.98" width="0.1524" layer="91"/>
<wire x1="490.22" y1="347.98" x2="497.84" y2="347.98" width="0.1524" layer="91"/>
<wire x1="497.84" y1="347.98" x2="497.84" y2="345.44" width="0.1524" layer="91"/>
<wire x1="474.98" y1="358.14" x2="474.98" y2="347.98" width="0.1524" layer="91"/>
<junction x="505.46" y="317.5"/>
<junction x="505.46" y="330.2"/>
<junction x="482.6" y="347.98"/>
<junction x="490.22" y="347.98"/>
<junction x="497.84" y="347.98"/>
<junction x="474.98" y="347.98"/>
<junction x="439.42" y="358.14"/>
<junction x="444.5" y="358.14"/>
<pinref part="SUPPLY25" gate="P" pin="+4.1V"/>
<pinref part="U1" gate="G$1" pin="VDDIO"/>
<pinref part="U1" gate="G$1" pin="VDD1A"/>
<pinref part="U1" gate="G$1" pin="VDD2A"/>
<pinref part="C20" gate="G$1" pin="1"/>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="R18" gate="G$1" pin="2"/>
<pinref part="J3" gate="G$1" pin="CT_TD"/>
<pinref part="J3" gate="G$1" pin="CT_RD"/>
</segment>
<segment>
<pinref part="SUPPLY9" gate="P" pin="+4.1V"/>
<pinref part="J6" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="P" pin="+4.1V"/>
<pinref part="U6" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="467.36" y1="193.04" x2="467.36" y2="180.34" width="0.1524" layer="91"/>
<wire x1="454.66" y1="180.34" x2="467.36" y2="180.34" width="0.1524" layer="91"/>
<wire x1="467.36" y1="193.04" x2="487.68" y2="193.04" width="0.1524" layer="91"/>
<junction x="467.36" y="180.34"/>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="J2" gate="G$1" pin="D+"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="T1" gate="G$1" pin="S"/>
<pinref part="R12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="487.68" y1="195.58" x2="472.44" y2="195.58" width="0.1524" layer="91"/>
<wire x1="472.44" y1="190.5" x2="472.44" y2="195.58" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="D-"/>
<pinref part="R11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="116.84" y1="279.4" x2="116.84" y2="271.78" width="0.1524" layer="91"/>
<wire x1="116.84" y1="279.4" x2="109.22" y2="279.4" width="0.1524" layer="91"/>
<wire x1="119.38" y1="279.4" x2="116.84" y2="279.4" width="0.1524" layer="91"/>
<junction x="116.84" y="279.4"/>
<pinref part="U2" gate="G$1" pin="CLKOUT"/>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="Q1" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="109.22" y1="276.86" x2="114.3" y2="276.86" width="0.1524" layer="91"/>
<wire x1="114.3" y1="276.86" x2="114.3" y2="284.48" width="0.1524" layer="91"/>
<wire x1="114.3" y1="284.48" x2="134.62" y2="284.48" width="0.1524" layer="91"/>
<wire x1="134.62" y1="284.48" x2="134.62" y2="279.4" width="0.1524" layer="91"/>
<wire x1="134.62" y1="271.78" x2="134.62" y2="279.4" width="0.1524" layer="91"/>
<wire x1="129.54" y1="279.4" x2="134.62" y2="279.4" width="0.1524" layer="91"/>
<junction x="134.62" y="279.4"/>
<pinref part="U2" gate="G$1" pin="CLKIN"/>
<pinref part="C22" gate="G$1" pin="1"/>
<pinref part="Q1" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="-17.78" y1="210.82" x2="-15.24" y2="210.82" width="0.1524" layer="91"/>
<pinref part="FUSE1" gate="G$1" pin="P$1"/>
<pinref part="J5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="474.98" y1="309.88" x2="474.98" y2="332.74" width="0.1524" layer="91"/>
<wire x1="474.98" y1="332.74" x2="474.98" y2="335.28" width="0.1524" layer="91"/>
<wire x1="464.82" y1="335.28" x2="469.9" y2="335.28" width="0.1524" layer="91"/>
<wire x1="469.9" y1="335.28" x2="469.9" y2="332.74" width="0.1524" layer="91"/>
<wire x1="469.9" y1="332.74" x2="474.98" y2="332.74" width="0.1524" layer="91"/>
<wire x1="474.98" y1="332.74" x2="513.08" y2="332.74" width="0.1524" layer="91"/>
<junction x="474.98" y="332.74"/>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="TX+"/>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="J3" gate="G$1" pin="TD+"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="482.6" y1="309.88" x2="482.6" y2="327.66" width="0.1524" layer="91"/>
<wire x1="482.6" y1="327.66" x2="482.6" y2="335.28" width="0.1524" layer="91"/>
<wire x1="513.08" y1="327.66" x2="482.6" y2="327.66" width="0.1524" layer="91"/>
<wire x1="482.6" y1="327.66" x2="467.36" y2="327.66" width="0.1524" layer="91"/>
<wire x1="467.36" y1="327.66" x2="467.36" y2="330.2" width="0.1524" layer="91"/>
<wire x1="467.36" y1="330.2" x2="464.82" y2="330.2" width="0.1524" layer="91"/>
<junction x="482.6" y="327.66"/>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="TX-"/>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="J3" gate="G$1" pin="TD-"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="490.22" y1="309.88" x2="490.22" y2="320.04" width="0.1524" layer="91"/>
<wire x1="490.22" y1="320.04" x2="490.22" y2="335.28" width="0.1524" layer="91"/>
<wire x1="464.82" y1="307.34" x2="467.36" y2="307.34" width="0.1524" layer="91"/>
<wire x1="467.36" y1="307.34" x2="467.36" y2="320.04" width="0.1524" layer="91"/>
<wire x1="467.36" y1="320.04" x2="490.22" y2="320.04" width="0.1524" layer="91"/>
<wire x1="490.22" y1="320.04" x2="513.08" y2="320.04" width="0.1524" layer="91"/>
<junction x="490.22" y="320.04"/>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="RX+"/>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="J3" gate="G$1" pin="RD+"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="497.84" y1="309.88" x2="497.84" y2="314.96" width="0.1524" layer="91"/>
<wire x1="497.84" y1="314.96" x2="497.84" y2="335.28" width="0.1524" layer="91"/>
<wire x1="469.9" y1="314.96" x2="497.84" y2="314.96" width="0.1524" layer="91"/>
<wire x1="497.84" y1="314.96" x2="513.08" y2="314.96" width="0.1524" layer="91"/>
<wire x1="469.9" y1="302.26" x2="469.9" y2="314.96" width="0.1524" layer="91"/>
<wire x1="464.82" y1="302.26" x2="469.9" y2="302.26" width="0.1524" layer="91"/>
<junction x="497.84" y="314.96"/>
<pinref part="C19" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="RX-"/>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="J3" gate="G$1" pin="RD-"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="454.66" y1="279.4" x2="454.66" y2="287.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RBIAS"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<wire x1="449.58" y1="345.44" x2="449.58" y2="347.98" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDCR"/>
<pinref part="C15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="210.82" y1="226.06" x2="215.9" y2="226.06" width="0.1524" layer="91"/>
<wire x1="215.9" y1="226.06" x2="215.9" y2="220.98" width="0.1524" layer="91"/>
<wire x1="215.9" y1="220.98" x2="231.14" y2="220.98" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="P$1"/>
<pinref part="U5" gate="G$1" pin="VREFP"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="231.14" y1="218.44" x2="220.98" y2="218.44" width="0.1524" layer="91"/>
<wire x1="220.98" y1="218.44" x2="210.82" y2="218.44" width="0.1524" layer="91"/>
<wire x1="231.14" y1="226.06" x2="220.98" y2="226.06" width="0.1524" layer="91"/>
<wire x1="220.98" y1="226.06" x2="220.98" y2="218.44" width="0.1524" layer="91"/>
<junction x="220.98" y="218.44"/>
<pinref part="U5" gate="G$1" pin="VREFN"/>
<pinref part="L3" gate="G$1" pin="P$1"/>
<pinref part="U5" gate="G$1" pin="VSSA"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="210.82" y1="233.68" x2="220.98" y2="233.68" width="0.1524" layer="91"/>
<wire x1="220.98" y1="233.68" x2="220.98" y2="228.6" width="0.1524" layer="91"/>
<wire x1="220.98" y1="228.6" x2="231.14" y2="228.6" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="P$1"/>
<pinref part="U5" gate="G$1" pin="VDDA"/>
</segment>
</net>
<net name="SHIELD" class="0">
<segment>
<wire x1="487.68" y1="182.88" x2="485.14" y2="182.88" width="0.1524" layer="91"/>
<wire x1="485.14" y1="182.88" x2="485.14" y2="180.34" width="0.1524" layer="91"/>
<wire x1="485.14" y1="180.34" x2="485.14" y2="177.8" width="0.1524" layer="91"/>
<wire x1="485.14" y1="175.26" x2="485.14" y2="170.18" width="0.1524" layer="91"/>
<wire x1="487.68" y1="180.34" x2="485.14" y2="180.34" width="0.1524" layer="91"/>
<wire x1="487.68" y1="177.8" x2="485.14" y2="177.8" width="0.1524" layer="91"/>
<wire x1="485.14" y1="177.8" x2="485.14" y2="175.26" width="0.1524" layer="91"/>
<wire x1="485.14" y1="175.26" x2="487.68" y2="175.26" width="0.1524" layer="91"/>
<junction x="485.14" y="175.26"/>
<junction x="485.14" y="177.8"/>
<junction x="485.14" y="180.34"/>
<label x="484.632" y="170.434" size="1.778" layer="95" rot="R90"/>
<pinref part="J2" gate="G$1" pin="SHIELD1"/>
<pinref part="J2" gate="G$1" pin="SHIELD2"/>
<pinref part="J2" gate="G$1" pin="SHIELD3"/>
<pinref part="J2" gate="G$1" pin="SHIELD4"/>
</segment>
<segment>
<wire x1="515.62" y1="228.6" x2="518.16" y2="228.6" width="0.1524" layer="91"/>
<wire x1="518.16" y1="228.6" x2="535.94" y2="228.6" width="0.1524" layer="91"/>
<wire x1="515.62" y1="226.06" x2="518.16" y2="226.06" width="0.1524" layer="91"/>
<wire x1="518.16" y1="228.6" x2="518.16" y2="226.06" width="0.1524" layer="91"/>
<wire x1="518.16" y1="226.06" x2="518.16" y2="223.52" width="0.1524" layer="91"/>
<wire x1="515.62" y1="220.98" x2="518.16" y2="220.98" width="0.1524" layer="91"/>
<wire x1="518.16" y1="220.98" x2="518.16" y2="223.52" width="0.1524" layer="91"/>
<wire x1="518.16" y1="223.52" x2="515.62" y2="223.52" width="0.1524" layer="91"/>
<junction x="518.16" y="228.6"/>
<junction x="518.16" y="223.52"/>
<junction x="518.16" y="226.06"/>
<label x="535.94" y="231.14" size="1.778" layer="95" rot="R180"/>
<pinref part="J1" gate="G$1" pin="GND1"/>
<pinref part="J1" gate="G$1" pin="GND2"/>
<pinref part="J1" gate="G$1" pin="GND4"/>
<pinref part="J1" gate="G$1" pin="GND3"/>
</segment>
<segment>
<wire x1="182.88" y1="190.5" x2="182.88" y2="193.04" width="0.1524" layer="91"/>
<wire x1="182.88" y1="193.04" x2="198.12" y2="193.04" width="0.1524" layer="91"/>
<label x="198.12" y="195.58" size="1.778" layer="95" rot="R180"/>
<pinref part="L6" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<wire x1="320.04" y1="208.28" x2="340.36" y2="208.28" width="0.1524" layer="91"/>
<wire x1="340.36" y1="208.28" x2="340.36" y2="200.66" width="0.1524" layer="91"/>
<wire x1="340.36" y1="187.96" x2="340.36" y2="200.66" width="0.1524" layer="91"/>
<junction x="340.36" y="200.66"/>
<pinref part="U5" gate="G$1" pin="XTAL1"/>
<pinref part="C24" gate="G$1" pin="1"/>
<pinref part="Q4" gate="G$1" pin="IN2"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="325.12" y1="187.96" x2="325.12" y2="200.66" width="0.1524" layer="91"/>
<wire x1="325.12" y1="200.66" x2="325.12" y2="205.74" width="0.1524" layer="91"/>
<wire x1="325.12" y1="205.74" x2="320.04" y2="205.74" width="0.1524" layer="91"/>
<junction x="325.12" y="200.66"/>
<pinref part="C23" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="XTAL2"/>
<pinref part="Q4" gate="G$1" pin="IN1"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<wire x1="365.76" y1="198.12" x2="365.76" y2="213.36" width="0.1524" layer="91"/>
<wire x1="365.76" y1="213.36" x2="363.22" y2="213.36" width="0.1524" layer="91"/>
<wire x1="320.04" y1="215.9" x2="340.36" y2="215.9" width="0.1524" layer="91"/>
<wire x1="340.36" y1="215.9" x2="340.36" y2="220.98" width="0.1524" layer="91"/>
<wire x1="340.36" y1="220.98" x2="365.76" y2="220.98" width="0.1524" layer="91"/>
<wire x1="365.76" y1="220.98" x2="365.76" y2="213.36" width="0.1524" layer="91"/>
<junction x="365.76" y="213.36"/>
<pinref part="C26" gate="G$1" pin="1"/>
<pinref part="Q3" gate="G$1" pin="P$2"/>
<pinref part="U5" gate="G$1" pin="RTCX1"/>
</segment>
</net>
<net name="!RESET" class="0">
<segment>
<wire x1="231.14" y1="259.08" x2="215.9" y2="259.08" width="0.1524" layer="91"/>
<wire x1="215.9" y1="259.08" x2="213.36" y2="256.54" width="0.1524" layer="91"/>
<label x="228.6" y="261.62" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="!RESET"/>
</segment>
<segment>
<wire x1="347.98" y1="223.52" x2="350.52" y2="226.06" width="0.1524" layer="91"/>
<wire x1="396.24" y1="220.98" x2="393.7" y2="220.98" width="0.1524" layer="91"/>
<wire x1="393.7" y1="215.9" x2="393.7" y2="218.44" width="0.1524" layer="91"/>
<wire x1="393.7" y1="218.44" x2="393.7" y2="220.98" width="0.1524" layer="91"/>
<wire x1="393.7" y1="220.98" x2="375.92" y2="220.98" width="0.1524" layer="91"/>
<wire x1="375.92" y1="220.98" x2="375.92" y2="226.06" width="0.1524" layer="91"/>
<wire x1="375.92" y1="226.06" x2="350.52" y2="226.06" width="0.1524" layer="91"/>
<wire x1="396.24" y1="218.44" x2="393.7" y2="218.44" width="0.1524" layer="91"/>
<junction x="393.7" y="220.98"/>
<junction x="393.7" y="218.44"/>
<label x="358.14" y="228.6" size="1.778" layer="95" rot="R180"/>
<pinref part="RESET" gate="G$1" pin="A_1"/>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="RESET" gate="G$1" pin="A_2"/>
</segment>
<segment>
<wire x1="574.04" y1="177.8" x2="576.58" y2="175.26" width="0.1524" layer="91"/>
<wire x1="558.8" y1="177.8" x2="574.04" y2="177.8" width="0.1524" layer="91"/>
<label x="558.8" y="177.8" size="1.778" layer="95"/>
<pinref part="J4" gate="G$1" pin="!RST"/>
</segment>
</net>
<net name="JTAG_TDI" class="0">
<segment>
<wire x1="558.8" y1="180.34" x2="574.04" y2="180.34" width="0.1524" layer="91"/>
<wire x1="574.04" y1="180.34" x2="576.58" y2="177.8" width="0.1524" layer="91"/>
<label x="558.8" y="180.34" size="1.778" layer="95"/>
<pinref part="J4" gate="G$1" pin="TDI"/>
</segment>
<segment>
<wire x1="231.14" y1="193.04" x2="215.9" y2="193.04" width="0.1524" layer="91"/>
<wire x1="215.9" y1="193.04" x2="213.36" y2="190.5" width="0.1524" layer="91"/>
<label x="215.9" y="193.548" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="TDI"/>
</segment>
</net>
<net name="JTAG_TDO" class="0">
<segment>
<wire x1="558.8" y1="182.88" x2="574.04" y2="182.88" width="0.1524" layer="91"/>
<wire x1="574.04" y1="182.88" x2="576.58" y2="180.34" width="0.1524" layer="91"/>
<label x="558.8" y="182.88" size="1.778" layer="95"/>
<pinref part="J4" gate="G$1" pin="TDO"/>
</segment>
<segment>
<wire x1="231.14" y1="195.58" x2="215.9" y2="195.58" width="0.1524" layer="91"/>
<wire x1="215.9" y1="195.58" x2="213.36" y2="193.04" width="0.1524" layer="91"/>
<label x="216.154" y="196.596" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="TDO/SWO"/>
</segment>
</net>
<net name="JTAG_TCK" class="0">
<segment>
<wire x1="558.8" y1="185.42" x2="574.04" y2="185.42" width="0.1524" layer="91"/>
<wire x1="574.04" y1="185.42" x2="576.58" y2="182.88" width="0.1524" layer="91"/>
<label x="558.8" y="185.42" size="1.778" layer="95"/>
<pinref part="J4" gate="G$1" pin="TCK"/>
</segment>
<segment>
<wire x1="231.14" y1="185.42" x2="215.9" y2="185.42" width="0.1524" layer="91"/>
<wire x1="215.9" y1="185.42" x2="213.36" y2="182.88" width="0.1524" layer="91"/>
<label x="216.154" y="186.182" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="TCK/SWDCLK"/>
</segment>
</net>
<net name="JTAG_TMS" class="0">
<segment>
<wire x1="558.8" y1="187.96" x2="574.04" y2="187.96" width="0.1524" layer="91"/>
<wire x1="574.04" y1="187.96" x2="576.58" y2="185.42" width="0.1524" layer="91"/>
<label x="558.8" y="187.96" size="1.778" layer="95"/>
<pinref part="J4" gate="G$1" pin="TMS"/>
</segment>
<segment>
<wire x1="231.14" y1="190.5" x2="215.9" y2="190.5" width="0.1524" layer="91"/>
<wire x1="215.9" y1="190.5" x2="213.36" y2="187.96" width="0.1524" layer="91"/>
<label x="216.408" y="191.008" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="TMS/SWDIO"/>
</segment>
</net>
<net name="JTAG_RTCK" class="0">
<segment>
<wire x1="520.7" y1="165.1" x2="523.24" y2="167.64" width="0.1524" layer="91"/>
<wire x1="523.24" y1="167.64" x2="523.24" y2="180.34" width="0.1524" layer="91"/>
<wire x1="523.24" y1="180.34" x2="530.86" y2="180.34" width="0.1524" layer="91"/>
<label x="522.478" y="168.91" size="1.778" layer="95" rot="R90"/>
<pinref part="J4" gate="G$1" pin="RTCK"/>
</segment>
<segment>
<wire x1="231.14" y1="182.88" x2="215.9" y2="182.88" width="0.1524" layer="91"/>
<wire x1="215.9" y1="182.88" x2="213.36" y2="180.34" width="0.1524" layer="91"/>
<label x="216.154" y="183.388" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="RTCK"/>
</segment>
</net>
<net name="MISO0" class="0">
<segment>
<wire x1="213.36" y1="294.64" x2="215.9" y2="297.18" width="0.1524" layer="91"/>
<wire x1="215.9" y1="297.18" x2="231.14" y2="297.18" width="0.1524" layer="91"/>
<label x="220.98" y="297.18" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="P0[17]/CTS1/MISO0/MISO"/>
</segment>
<segment>
<wire x1="426.72" y1="220.98" x2="429.26" y2="223.52" width="0.1524" layer="91"/>
<wire x1="429.26" y1="223.52" x2="477.52" y2="223.52" width="0.1524" layer="91"/>
<label x="429.26" y="223.52" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="DAT0/DO"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="109.22" y1="292.1" x2="144.78" y2="292.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="DOUT"/>
<pinref part="U3" gate="G$1" pin="VIB"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="144.78" y1="289.56" x2="109.22" y2="289.56" width="0.1524" layer="91"/>
<wire x1="144.78" y1="289.56" x2="144.78" y2="287.02" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SCLK"/>
<pinref part="U3" gate="G$1" pin="VOC"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="144.78" y1="284.48" x2="139.7" y2="284.48" width="0.1524" layer="91"/>
<wire x1="139.7" y1="284.48" x2="139.7" y2="294.64" width="0.1524" layer="91"/>
<wire x1="139.7" y1="294.64" x2="109.22" y2="294.64" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VOD"/>
<pinref part="U2" gate="G$1" pin="DIN"/>
</segment>
</net>
<net name="MISO1" class="0">
<segment>
<wire x1="177.8" y1="292.1" x2="210.82" y2="292.1" width="0.1524" layer="91"/>
<wire x1="210.82" y1="292.1" x2="213.36" y2="289.56" width="0.1524" layer="91"/>
<label x="208.28" y="294.64" size="1.778" layer="95" rot="R180"/>
<pinref part="U3" gate="G$1" pin="VOB"/>
</segment>
<segment>
<wire x1="231.14" y1="312.42" x2="215.9" y2="312.42" width="0.1524" layer="91"/>
<wire x1="215.9" y1="312.42" x2="213.36" y2="309.88" width="0.1524" layer="91"/>
<label x="220.98" y="312.42" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="P0[8]/I2STX_WS/MISO1/MAT2[2]"/>
</segment>
</net>
<net name="SCK1" class="0">
<segment>
<wire x1="177.8" y1="287.02" x2="210.82" y2="287.02" width="0.1524" layer="91"/>
<wire x1="210.82" y1="287.02" x2="213.36" y2="284.48" width="0.1524" layer="91"/>
<label x="208.28" y="289.56" size="1.778" layer="95" rot="R180"/>
<pinref part="U3" gate="G$1" pin="VIC"/>
</segment>
<segment>
<wire x1="231.14" y1="314.96" x2="215.9" y2="314.96" width="0.1524" layer="91"/>
<wire x1="215.9" y1="314.96" x2="213.36" y2="312.42" width="0.1524" layer="91"/>
<label x="220.98" y="314.96" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="P0[7]/I2STX_CLK/SCK1/MAT2[1]"/>
</segment>
</net>
<net name="MOSI1" class="0">
<segment>
<wire x1="177.8" y1="284.48" x2="210.82" y2="284.48" width="0.1524" layer="91"/>
<wire x1="210.82" y1="284.48" x2="213.36" y2="281.94" width="0.1524" layer="91"/>
<label x="208.28" y="287.02" size="1.778" layer="95" rot="R180"/>
<pinref part="U3" gate="G$1" pin="VID"/>
</segment>
<segment>
<wire x1="231.14" y1="309.88" x2="215.9" y2="309.88" width="0.1524" layer="91"/>
<wire x1="215.9" y1="309.88" x2="213.36" y2="307.34" width="0.1524" layer="91"/>
<label x="220.98" y="309.88" size="1.778" layer="95"/>
<pinref part="U5" gate="G$1" pin="P0[9]/I2STX_SDA/MOSI1/MAT2[3]"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<wire x1="10.16" y1="251.46" x2="10.16" y2="254" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<wire x1="10.16" y1="264.16" x2="10.16" y2="266.7" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="-5.08" y1="210.82" x2="-2.54" y2="210.82" width="0.1524" layer="91"/>
<wire x1="10.16" y1="241.3" x2="10.16" y2="226.06" width="0.1524" layer="91"/>
<wire x1="10.16" y1="226.06" x2="-2.54" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="226.06" x2="-2.54" y2="210.82" width="0.1524" layer="91"/>
<junction x="-2.54" y="210.82"/>
<pinref part="FUSE1" gate="G$1" pin="P$2"/>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="R20" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="SHIELD2" class="0">
<segment>
<wire x1="546.1" y1="304.8" x2="548.64" y2="304.8" width="0.1524" layer="91"/>
<wire x1="548.64" y1="304.8" x2="558.8" y2="304.8" width="0.1524" layer="91"/>
<wire x1="546.1" y1="302.26" x2="548.64" y2="302.26" width="0.1524" layer="91"/>
<wire x1="548.64" y1="302.26" x2="548.64" y2="304.8" width="0.1524" layer="91"/>
<junction x="548.64" y="304.8"/>
<pinref part="L5" gate="G$1" pin="P$1"/>
<pinref part="J3" gate="G$1" pin="SHIELD1"/>
<pinref part="J3" gate="G$1" pin="SHIELD2"/>
</segment>
</net>
<net name="!RSTOUT" class="0">
<segment>
<wire x1="231.14" y1="256.54" x2="215.9" y2="256.54" width="0.1524" layer="91"/>
<wire x1="215.9" y1="256.54" x2="213.36" y2="254" width="0.1524" layer="91"/>
<label x="228.6" y="259.08" size="1.778" layer="95" rot="R180"/>
<pinref part="U5" gate="G$1" pin="!RSTOUT"/>
</segment>
<segment>
<wire x1="347.98" y1="294.64" x2="350.52" y2="297.18" width="0.1524" layer="91"/>
<wire x1="350.52" y1="297.18" x2="419.1" y2="297.18" width="0.1524" layer="91"/>
<label x="360.68" y="299.72" size="1.778" layer="95" rot="R180"/>
<pinref part="U1" gate="G$1" pin="NRST"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<wire x1="320.04" y1="236.22" x2="381" y2="236.22" width="0.1524" layer="91"/>
<wire x1="381" y1="236.22" x2="381" y2="251.46" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="P2[13]/EINT3/I2STX_SDA"/>
<pinref part="D6" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<wire x1="320.04" y1="241.3" x2="365.76" y2="241.3" width="0.1524" layer="91"/>
<wire x1="365.76" y1="241.3" x2="365.76" y2="251.46" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="P2[11]/EINT1/I2STX_CLK"/>
<pinref part="D4" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="A"/>
<pinref part="R26" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<pinref part="R25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<pinref part="R24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<wire x1="320.04" y1="238.76" x2="373.38" y2="238.76" width="0.1524" layer="91"/>
<wire x1="373.38" y1="238.76" x2="373.38" y2="251.46" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="P2[12]/EINT2/I2STX_WS"/>
<pinref part="D5" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="439.42" y1="190.5" x2="439.42" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<wire x1="411.48" y1="302.26" x2="419.1" y2="302.26" width="0.1524" layer="91"/>
<wire x1="411.48" y1="294.64" x2="411.48" y2="302.26" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="LED2/NINTSEL"/>
<pinref part="R21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<wire x1="403.86" y1="304.8" x2="403.86" y2="294.64" width="0.1524" layer="91"/>
<wire x1="419.1" y1="304.8" x2="403.86" y2="304.8" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="LED1/REGOFF"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<wire x1="101.6" y1="190.5" x2="104.14" y2="190.5" width="0.1524" layer="91"/>
<wire x1="104.14" y1="190.5" x2="106.68" y2="190.5" width="0.1524" layer="91"/>
<wire x1="104.14" y1="180.34" x2="104.14" y2="190.5" width="0.1524" layer="91"/>
<junction x="104.14" y="190.5"/>
<pinref part="J6" gate="G$1" pin="P$2"/>
<pinref part="U4" gate="G$1" pin="OUT"/>
<pinref part="C2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="419.1" y1="330.2" x2="393.7" y2="330.2" width="0.1524" layer="91"/>
<wire x1="393.7" y1="330.2" x2="393.7" y2="360.68" width="0.1524" layer="91"/>
<wire x1="393.7" y1="360.68" x2="388.62" y2="360.68" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="NINT/REFCLKO"/>
<pinref part="U6" gate="G$1" pin="A"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
