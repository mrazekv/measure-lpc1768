﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace LPCunlock
{
    public partial class frmPortSelect : Form
    {
        public string PortName = "COM1";
        public frmPortSelect(string port="")
        {
            PortName = port;
            InitializeComponent();
            lbItems.Items.Clear();
            int i = 0, selected=-1;
            foreach (string p in SerialPort.GetPortNames())
            {
                if (p == PortName) selected = i;
                i++;
                lbItems.Items.Add(p);
            }

            if (selected >= 0) lbItems.SelectedIndex = selected;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void lbItems_DoubleClick(object sender, EventArgs e)
        {
            if (lbItems.SelectedItems.Count == 1)
            {
                PortName = lbItems.SelectedItems[0].ToString();
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
