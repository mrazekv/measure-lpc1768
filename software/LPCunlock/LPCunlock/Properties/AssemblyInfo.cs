﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LPCunlock")]
[assembly: AssemblyDescription("Unlock CRP protection on NXP LPC 1768")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("FIT VUT v Brne; Vojtech Mrazek")]
[assembly: AssemblyProduct("LPCunlock")]
[assembly: AssemblyCopyright("Copyright © FIT VUT v Brne a Vojtech Mrazek 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0a97c3fa-b7f9-4177-833c-6d7633b4aabb")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
