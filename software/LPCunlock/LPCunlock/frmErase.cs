﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace LPCunlock
{
    public partial class frmErase : Form
    {
        #region WinAPI
        /// <summary>
        /// The CreateFile function creates or opens a file, file stream, directory, physical disk, volume, console buffer, tape drive,
        /// communications resource, mailslot, or named pipe. The function returns a handle that can be used to access an object.
        /// </summary>
        /// <param name="lpFileName"></param>
        /// <param name="dwDesiredAccess"> access to the object, which can be read, write, or both</param>
        /// <param name="dwShareMode">The sharing mode of an object, which can be read, write, both, or none</param>
        /// <param name="SecurityAttributes">A pointer to a SECURITY_ATTRIBUTES structure that determines whether or not the returned handle can 
        /// be inherited by child processes. Can be null</param>
        /// <param name="dwCreationDisposition">An action to take on files that exist and do not exist</param>
        /// <param name="dwFlagsAndAttributes">The file attributes and flags. </param>
        /// <param name="hTemplateFile">A handle to a template file with the GENERIC_READ access right. The template file supplies file attributes 
        /// and extended attributes for the file that is being created. This parameter can be null</param>
        /// <returns>If the function succeeds, the return value is an open handle to a specified file. If a specified file exists before the function 
        /// all and dwCreationDisposition is CREATE_ALWAYS or OPEN_ALWAYS, a call to GetLastError returns ERROR_ALREADY_EXISTS, even when the function 
        /// succeeds. If a file does not exist before the call, GetLastError returns 0 (zero).
        /// If the function fails, the return value is INVALID_HANDLE_VALUE. To get extended error information, call GetLastError.
        /// </returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern IntPtr CreateFile(
           string lpFileName,
           EFileAccess dwDesiredAccess,
           EFileShare dwShareMode,
           IntPtr lpSecurityAttributes,
           ECreationDisposition dwCreationDisposition,
           EFileAttributes dwFlagsAndAttributes,
           IntPtr hTemplateFile);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll")]
        static extern bool SetCommMask(IntPtr hFile, uint dwEvtMask);


        [DllImport("kernel32.dll")]
        static extern bool GetCommModemStatus(IntPtr hFile, out uint lpModemStat);

        [DllImport("kernel32.dll")]
        static extern bool EscapeCommFunction(IntPtr hFile, ExtendedFunctions dwFunc);

        [Flags]
        public enum ExtendedFunctions : uint
        {
            /// <summary>
            /// Restores character transmission and places the transmission line in a nonbreak state
            /// </summary>
            CLRBREAK = 9, 
            /// <summary>
            /// Clears the DTR (data-terminal-ready) signal.
            /// </summary>
            CLRDTR = 6, 
            CLRRTS = 4, //Clears the RTS (request-to-send) signal.
            SETBREAK = 8, //Suspends character transmission and places the transmission line in a break state until the ClearCommBreak function is called
            SETDTR = 5, //Sends the DTR (data-terminal-ready) signal.
            SETRTS = 3, //Sends the RTS (request-to-send) signal.
            SETXOFF = 1, //Causes transmission to act as if an XOFF character has been received.
            SETXON = 2 //Causes transmission to act as if an XON character has been received.
        }

        [Flags]
        public enum EFileAccess : uint
        {
            Delete = 0x10000,
            ReadControl = 0x20000,
            WriteDAC = 0x40000,
            WriteOwner = 0x80000,
            Synchronize = 0x100000,

            StandardRightsRequired = 0xF0000,
            StandardRightsRead = ReadControl,
            StandardRightsWrite = ReadControl,
            StandardRightsExecute = ReadControl,
            StandardRightsAll = 0x1F0000,
            SpecificRightsAll = 0xFFFF,

            AccessSystemSecurity = 0x1000000,       // AccessSystemAcl access type

            MaximumAllowed = 0x2000000,         // MaximumAllowed access type

            GenericRead = 0x80000000,
            GenericWrite = 0x40000000,
            GenericExecute = 0x20000000,
            GenericAll = 0x10000000
        }

        [Flags]
        public enum EFileShare : uint
        {
            /// <summary>
            /// 
            /// </summary>
            None = 0x00000000,
            /// <summary>
            /// Enables subsequent open operations on an object to request read access. 
            /// Otherwise, other processes cannot open the object if they request read access. 
            /// If this flag is not specified, but the object has been opened for read access, the function fails.
            /// </summary>
            Read = 0x00000001,
            /// <summary>
            /// Enables subsequent open operations on an object to request write access. 
            /// Otherwise, other processes cannot open the object if they request write access. 
            /// If this flag is not specified, but the object has been opened for write access, the function fails.
            /// </summary>
            Write = 0x00000002,
            /// <summary>
            /// Enables subsequent open operations on an object to request delete access. 
            /// Otherwise, other processes cannot open the object if they request delete access.
            /// If this flag is not specified, but the object has been opened for delete access, the function fails.
            /// </summary>
            Delete = 0x00000004
        }

        public enum ECreationDisposition : uint
        {
            /// <summary>
            /// Creates a new file. The function fails if a specified file exists.
            /// </summary>
            New = 1,
            /// <summary>
            /// Creates a new file, always. 
            /// If a file exists, the function overwrites the file, clears the existing attributes, combines the specified file attributes, 
            /// and flags with FILE_ATTRIBUTE_ARCHIVE, but does not set the security descriptor that the SECURITY_ATTRIBUTES structure specifies.
            /// </summary>
            CreateAlways = 2,
            /// <summary>
            /// Opens a file. The function fails if the file does not exist. 
            /// </summary>
            OpenExisting = 3,
            /// <summary>
            /// Opens a file, always. 
            /// If a file does not exist, the function creates a file as if dwCreationDisposition is CREATE_NEW.
            /// </summary>
            OpenAlways = 4,
            /// <summary>
            /// Opens a file and truncates it so that its size is 0 (zero) bytes. The function fails if the file does not exist.
            /// The calling process must open the file with the GENERIC_WRITE access right. 
            /// </summary>
            TruncateExisting = 5
        }

        [Flags]
        public enum EFileAttributes : uint
        {
            Readonly = 0x00000001,
            Hidden = 0x00000002,
            System = 0x00000004,
            Directory = 0x00000010,
            Archive = 0x00000020,
            Device = 0x00000040,
            Normal = 0x00000080,
            Temporary = 0x00000100,
            SparseFile = 0x00000200,
            ReparsePoint = 0x00000400,
            Compressed = 0x00000800,
            Offline = 0x00001000,
            NotContentIndexed = 0x00002000,
            Encrypted = 0x00004000,
            Write_Through = 0x80000000,
            Overlapped = 0x40000000,
            NoBuffering = 0x20000000,
            RandomAccess = 0x10000000,
            SequentialScan = 0x08000000,
            DeleteOnClose = 0x04000000,
            BackupSemantics = 0x02000000,
            PosixSemantics = 0x01000000,
            OpenReparsePoint = 0x00200000,
            OpenNoRecall = 0x00100000,
            FirstPipeInstance = 0x00080000
        }
        #endregion


        public frmErase()
        {
            InitializeComponent();
            serial.NewLine = "\r\n";
            serial.ReadTimeout = 1000;
            serial.PortName = "COM11";
            btnPort.Text = serial.PortName;
        }

        private void Log(string line)
        {
            txtLog.Text += line.Replace("\r", "\r\n") + "\r\n";
            txtLog.SelectionStart = txtLog.Text.Length - 1;
            txtLog.ScrollToCaret();
        }

        private void btnErase_Click(object sender, EventArgs e)
        {
            // RTS = P2.10
            // DTR = RESET

            txtLog.Text = "";
            try
            {
                serial.Open();

                serial.WriteLine("?");
                Log(serial.ReadLine());
                serial.WriteLine("Synchronized");
                Log(serial.ReadLine());
                serial.WriteLine("OK");
                Log(serial.ReadLine());
                serial.WriteLine("U 23130");
                Log(serial.ReadLine());
                serial.WriteLine("P 0 26");
                Log(serial.ReadLine());
                serial.WriteLine("E 0 26");
                Log(serial.ReadLine());
                serial.Close();

                MessageBox.Show("Done", "CRP unblock", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (TimeoutException)
            {
                MessageBox.Show("Timeout exception", "CRP unblock", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (serial.IsOpen) serial.Close();
            }
            catch (System.IO.IOException ex)
            {
                MessageBox.Show("IO exception\r\n" + ex.Message, "CRP unblock", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (serial.IsOpen) serial.Close();
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Access exception\r\n" + ex.Message, "CRP unblock", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (serial.IsOpen) serial.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString() + "\r\n" + ex.Message, "CRP unblock", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (serial.IsOpen) serial.Close();
            }
        }

        private void frmErase_FormClosing(object sender, FormClosingEventArgs e)
        {
            global::LPCunlock.Properties.Settings.Default.PortName = serial.PortName;
            global::LPCunlock.Properties.Settings.Default.Save();
        }

        private void btnPort_Click(object sender, EventArgs e)
        {
            frmPortSelect p = new frmPortSelect(serial.PortName);
            if (p.ShowDialog() == DialogResult.OK)
            {
                serial.PortName = p.PortName;
                btnPort.Text = p.PortName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            // RTS = P2.10
            // DTR = RESET
            IntPtr hComm = CreateFile("\\\\.\\" + serial.PortName, EFileAccess.GenericRead | EFileAccess.GenericWrite, EFileShare.None, IntPtr.Zero, ECreationDisposition.OpenExisting, EFileAttributes.Overlapped, IntPtr.Zero);
            if (hComm.ToInt32() == -1) // INVALID_HANDLE
            {
                MessageBox.Show("Invalid openning port");
            }

            EscapeCommFunction(hComm, ExtendedFunctions.SETDTR);
            System.Threading.Thread.Sleep(1000);
            EscapeCommFunction(hComm, ExtendedFunctions.SETDTR);


            CloseHandle(hComm);
        }
    }
}
